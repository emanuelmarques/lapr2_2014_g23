# README #

Este repositório tem o código de base do projeto de LAPR2 2013/2014, correspondendo ao fim da iteração 2 do trabalho de ESOFT. Foram incluídos alguns testes unitários para demonstrar a sua utilização.

### Instruções para criar o repositório do grupo em LAPR2 ###

* Um elemento do grupo faz fork deste projeto (Actions->More->Fork)
* Escolher as opções "Private repository" e "Issues"
* Nome do repositório: LAPR2_2014_GXX em que XX representa o número do grupo (ex: LAPR2_2014_G03)
* Convidar os restantes elementos do grupo, o Orientador (ver tabela dos grupos- email formado pelas siglas e "@isep.ipp.pt") e o responsável de LAPR2 (AMM@isep.ipp.pt). Atribuir-lhes permissões de escrita, ficando apenas o criador com permissões de administração.

### Contacto ###

* Angelo Martins (amm@isep.ipp.pt)

(c) ISEP, 2014