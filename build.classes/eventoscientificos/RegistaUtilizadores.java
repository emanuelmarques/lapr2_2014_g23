/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.util.List;

/**
 *
 * @author vitor_000
 */
public class RegistaUtilizadores {

    private List<Utilizador> listaUtilizadores;
    private Empresa empresa;

    public RegistaUtilizadores() {
    }

    public List<Utilizador> getListaUtilizadores() {
        return this.listaUtilizadores;
    }

    public void setListaUtilizadores(List<Utilizador> listaUtilizadores) {
        this.listaUtilizadores = listaUtilizadores;
    }

    public Utilizador getUtilizador(String id) {
        for (Utilizador utilizador : listaUtilizadores) {
            if (utilizador.getUsername().equals(id)) {
                return utilizador;
            }
        }
        return null;
    }

}
