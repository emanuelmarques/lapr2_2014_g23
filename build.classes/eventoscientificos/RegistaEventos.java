package eventoscientificos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vitor_000
 */
public class RegistaEventos {

    private List<Evento> listaEventos;
    private Empresa empresa;

    public RegistaEventos() {
    }

    public List<Evento> getLe() {
        return this.listaEventos;
    }

    public void setLe(List<Evento> le) {
        this.listaEventos = le;
    }

    public Evento novoEvento() {
        return new Evento();
    }

    public List<Evento> getListaEventosOrganizador(String id) {
        RegistaUtilizadores ru = this.empresa.getRegistaUtilizadores();
        Utilizador u = ru.getUtilizador(id);
        List<Evento> le = new ArrayList();
        for (Evento evento : this.listaEventos) {
            Evento e = evento;
            List<Organizador> lo = e.getListaOrganizadores();
            if (lo.contains(u)) {
                le.add(e);

            }
        }
        return le;
    }

}
