/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class FormulaDePagamento1Test {
    
    public FormulaDePagamento1Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of resultado method, of class FormulaDePagamento1.
     */
    @Test
    public void testResultado() {
        System.out.println("resultado");
        Evento evento = new Evento();
        evento.setPrecoFullPaper(10);
        FormulaDePagamento1 instance = new FormulaDePagamento1(evento);
        instance.setQntFullPapers(1);
        float expResult = 10F;
        float result = instance.resultado();
        assertEquals(expResult, result, 0.0);

    }
    
}
