/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import eventoscientificos.submissaostates.SubmissaoState;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class SubmissaoTest {
    
    public SubmissaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    

  
    /**
     * Test of valida method, of class Submissao.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Evento e = new Evento();
        Submissao instance = new Submissao(e);
        Artigo a = new Artigo();
        instance.setArtigo(a);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);

    }

   
    /**
     * Test of equals method, of class Submissao.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Evento e = new Evento();
        Submissao sub = new Submissao(e);
         Artigo a = new Artigo();
       sub.setArtigo(a);
        Submissao instance = new Submissao(e);
        instance.setArtigo(a);
        
        boolean expResult = true;
        boolean result = instance.equals(sub);
        assertEquals(expResult, result);
     
    }

  
    

    

   

    /**
     * Test of getRevisoesDoRevisor method, of class Submissao.
     */
    @Test
    public void testGetRevisoesDoRevisor() {
        System.out.println("getRevisoesDoRevisor");
        Evento e  = new Evento();
        Utilizador u = new Utilizador();
        Revisor revisor = new Revisor(u);
        Revisao rev = new Revisao();
        rev.setRevisor(revisor);
        Submissao instance = new Submissao(e);
        instance.getListaRevisoes().getListaRevisoes().add(rev);
        List<Revisao> expResult = instance.getListaRevisoes().getListaRevisoes();
        List<Revisao> result = instance.getRevisoesDoRevisor(revisor);
        assertEquals(expResult, result);
        
    }

    
}
