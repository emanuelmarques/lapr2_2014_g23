/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author emanuelmarques
 */
public class RegistaUtilizadoresTest {

    public RegistaUtilizadoresTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testGetUtilizador() {
        System.out.println("getUtilizador");
        Empresa teste = new Empresa();
        Utilizador user = new Utilizador("username", "pwd", "nome", "email@email.pt");
        RegistaUtilizadores ru = new RegistaUtilizadores(teste);
        teste.setRu(ru);
        ru.registaUtilizador(user);
        Utilizador user2 = ru.getUtilizador("username");

        assertEquals(user, user2);

    }

    @Test
    public void testGetUtilizadorEmail() {
        System.out.println("getUtilizadorEmail");
        Empresa teste = new Empresa();
        Utilizador user = new Utilizador("username", "pwd", "nome", "email@email.pt");
        RegistaUtilizadores ru = new RegistaUtilizadores(teste);
        teste.setRu(ru);
        ru.registaUtilizador(user);
        Utilizador user2 = ru.getUtilizadorEmail("email@email.pt");

        assertEquals(user, user2);

    }

    @Test
    public void testRegistaUtilizador() {
        System.out.println("RegistaUtilizador");
        Empresa teste = new Empresa();
        Utilizador user = new Utilizador("username", "pwd", "nome", "email@email.pt");
        RegistaUtilizadores ru = new RegistaUtilizadores(teste);
        teste.setRu(ru);
        ru.registaUtilizador(user);
        int expectedSize = 1;

        assertEquals(expectedSize, teste.getRegistaUtilizadores().getListaUtilizadores().size());

    }
    @Test
    public void testGetListaVazia() {
        System.out.println("Lista Vazia");
        Empresa teste = new Empresa();
        List<Utilizador> test= teste.getRegistaUtilizadores().getListaUtilizadores();
        boolean expected = true;
        boolean real = test.isEmpty();

        assertEquals(expected,real);

    }

    @Test
    public void testValidaUtilizador() {
        System.out.println("validaUtilizador");
        Empresa teste = new Empresa();
        Utilizador user = new Utilizador("username", "pwd", "nome", "email@email.pt");
        RegistaUtilizadores ru = new RegistaUtilizadores(teste);
        teste.setRu(ru);
        ru.registaUtilizador(user);

        boolean expected = false;

        assertEquals(expected, ru.validaUtilizador(user));

    }

}
