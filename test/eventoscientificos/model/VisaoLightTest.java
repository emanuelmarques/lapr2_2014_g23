/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import eventoscientificos.utils.Data;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import pt.ipp.isep.dei.eapli.visaolight.VisaoLight;

/**
 *
 * @author vitor_000
 */
public class VisaoLightTest {

    public VisaoLightTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getAutorizacaoDCC method, of class VisaoLightPagamento.
     */
    @Test
    public void getAutorizacaoDCC() {
        assertNotNull(VisaoLight.getAutorizacaoDCC("1234", "2020-12-31", 500.0f, "2020-12-30"));
        // numero CC vazio
        assertNull(VisaoLight.getAutorizacaoDCC("", "2020-12-31", 500.0f, "2020-12-30"));
        // numero CC null
        assertNull(VisaoLight.getAutorizacaoDCC(null, "2020-12-31", 500.0f, "2020-12-30"));
        // valor menor que zero
        assertNull(VisaoLight.getAutorizacaoDCC("1234", "2020-12-31", -500.0f, "2020-12-30"));
        // valor igual zero
        assertNull(VisaoLight.getAutorizacaoDCC("1234", "2020-12-31", 0.0f, "2020-12-30"));
        // data limite anterior a atual
        assertNull(VisaoLight.getAutorizacaoDCC("1234", "2020-12-31", 500.0f, "2012-01-01"));
        // data validade anterior a data atual
        assertNull(VisaoLight.getAutorizacaoDCC("1234", "2012-01-31", 500.0f, "2021-12-31"));
        // data limite e data de validade anterior a data atual
        assertNull(VisaoLight.getAutorizacaoDCC("1234", "2012-01-01", 500.0f, "2012-01-31"));

    }

    @Test
    public void test() {
        assertNotNull(VisaoLight.getAutorizacaoDCC("1234", "2020-12-31", 500.0f, "2020-12-30"));
        System.out.println(VisaoLight.getAutorizacaoDCC("1234", "2020-12-31", 500.0f, "2020-12-30"));
       
    }
}
