package eventoscientificos.model;

import java.util.*;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Rita
 */
public class ArtigoTest {

    public ArtigoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setListaTopicos method, of class Artigo.
     */
    @Test
    public void testSetListaTopicos() {
        System.out.println("setListaTopicos");
        List<Topico> listaTopicos = new ArrayList<>();
        Artigo instance = new Artigo();
        instance.setListaTopicos(listaTopicos);
        List<Topico> result =instance.getM_listaTopicos().getListaTopicos();
        
        assertEquals(listaTopicos, result);
    }

     /**
     * Test of getInfo method, of class Artigo.
     */
    @Test
    public void testGetInfo() {
        System.out.println("getInfo");
        Artigo instance = new Artigo();
        String expResult = instance.getInfo().toString();
        String result = instance.getInfo();
        assertEquals(expResult, result);
    }

   
    /**
     * Test of valida method, of class Artigo.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Artigo instance = new Artigo();
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    
    /**
     * Test of equals method, of class Artigo.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Utilizador myUt = new Utilizador();
        myUt.setUsername("xxx@isep.ipp.pt");
        myUt.setEmail("xxx@isep.ipp.pt");
        Autor act = new Autor();
        act.setUtilizador(myUt);

        Artigo obj = new Artigo();
        obj.setTitulo("");
        obj.setAutorCorrespondente(act);
        Artigo instance = new Artigo();
        instance.setTitulo("");
        instance.setAutorCorrespondente(act);

        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    
    
}