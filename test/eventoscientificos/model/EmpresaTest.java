/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class EmpresaTest {
    
    public EmpresaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   
    

   
    /**
     * Test of getRevisoesDoRevisor method, of class Empresa.
     */
    @Test
    public void testGetRevisoesDoRevisor() {
        System.out.println("getRevisoesDoRevisor");
        Empresa instance = new Empresa();
        
        Evento evento=new Evento();
        instance.getRegistaEventos().getLe().add(evento);
        Utilizador u = new Utilizador();
        instance.getRegistaUtilizadores().getListaUtilizadores().add(u);
        Submissao sub = new Submissao(evento);
        evento.getSubmissoes().getListaSubmissoes().add(sub);
        u.setEmail("asd");
        
        Revisor revisor = new Revisor(u);
        evento.getCP().getListaRevisores().getListaRevisores().add(revisor);
        Revisao rev = new Revisao();
        rev.setRevisor(revisor);
        sub.getListaRevisoes().getListaRevisoes().add(rev);
       
        
        List<Revisao> expResult = new ArrayList();
        expResult.add(rev);
        List<Revisao> result = instance.getRevisoesDoRevisor(revisor);
        assertEquals(expResult, result);
        
    }

   
    
}
