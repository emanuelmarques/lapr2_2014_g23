/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class TopicoTest {
    
    public TopicoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    

    /**
     * Test of valida method, of class Topico.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Topico instance = new Topico();
        instance.setDescricao("asd");
        instance.setCodigoACM("fgh");
        
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
      
    }

   

    /**
     * Test of equals method, of class Topico.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Topico instance = new Topico();
        Topico instance1 = new Topico();
        instance.setDescricao("asd");
        instance1.setDescricao("asd");
        instance.setCodigoACM("fgh");
        instance1.setCodigoACM("fgh");
        
        boolean expResult = true;
        boolean result = instance.equals(instance1);
        assertEquals(expResult, result);
        
    }
    
}
