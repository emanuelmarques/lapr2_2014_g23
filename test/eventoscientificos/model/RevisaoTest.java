/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author vitor_000
 */
public class RevisaoTest {

    public RevisaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testValidaRevisao() {
        System.out.println("validaRevisao");
        Revisor r = new Revisor(new Utilizador());
        Revisao instance = new Revisao(1, 2, 3, 4, "Aceite", r , "teste");
        boolean expResult = true;
        boolean result = instance.validaRevisao();
        assertEquals(expResult, result);
    }

   
    


    /**
     * Test of equals method, of class Revisao.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Revisao rev = new Revisao();
        rev.setAdequacao(3);
        Revisao instance = new Revisao();
        instance.setAdequacao(3);
        boolean expResult = true;
        boolean result = instance.equals(rev);
        assertEquals(expResult, result);

    }

  

    /**
     * Test of getMediClassificaçoes method, of class Revisao.
     */
    @Test
    public void testGetMediClassificaçoes() {
        System.out.println("getMediClassifica\u00e7oes");
        Revisao instance = new Revisao();
        instance.setAdequacao(2);
        instance.setConfianca(2);
        instance.setOriginalidade(4);
        instance.setQualidade(4);
        int expResult = 3;
        int result = instance.getMediClassificaçoes();
        assertEquals(expResult, result);
      
    }
}
