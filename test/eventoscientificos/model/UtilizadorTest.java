/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author amartins
 */
public class UtilizadorTest {

    public UtilizadorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    

    /**
     * Test of valida method, of class Utilizador.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Utilizador instance = new Utilizador();
        String strEmail = "buckr@nasa.gov";
        instance.setEmail(strEmail);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class Utilizador.
     */
    @Test
    public void testValidaNot() {
        System.out.println("valida - false");
        Utilizador instance = new Utilizador();
        String strEmail = " BucKR@NASA.gov ";
        instance.setEmail(strEmail);
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of mesmoQueUtilizador method, of class Utilizador.
     */
    @Test
    public void testMesmoQueUtilizador() {
        System.out.println("mesmoQueUtilizador");
        Utilizador u = null;
        Utilizador instance = new Utilizador();
        boolean expResult = false;
        boolean result = instance.mesmoQueUtilizador(u);
        assertEquals(expResult, result);

    }

 
    /**
     * Test of equals method, of class Utilizador.
     */
    @Test
    public void testEquals() {
        System.out.println("equals - same username");
        Object obj = new Utilizador("buckr", "", "Buck Rogers", "buckr@gmail.com");;
        Utilizador instance = new Utilizador("buckr", "1234", "Buck Rogers", "buckr@nasa.gov");
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Utilizador.
     */
    @Test
    public void testEqualsSameEmail() {
        System.out.println("equals - same email");
        Object obj = new Utilizador("bucky", "", "Buck", "buckr@nasa.gov");;
        Utilizador instance = new Utilizador("buckr", "1234", "Buck Rogers", "buckr@nasa.gov");
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Utilizador.
     */
    @Test
    public void testEqualsNot() {
        System.out.println("equals not");
        Object obj = new Utilizador("bucky", "", "Buck Rogers", "buckr@gmail.com");;
        Utilizador instance = new Utilizador("buckr", "1234", "Buck Rogers", "buckr@nasa.gov");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    
}
