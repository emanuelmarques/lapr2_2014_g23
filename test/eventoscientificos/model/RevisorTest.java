package eventoscientificos.model;

import java.util.*;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Rita
 */
public class RevisorTest {

    public RevisorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    
    /**
     * Test of valida method, of class Revisor.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Utilizador u = new Utilizador();
        Revisor instance = new Revisor(u);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }


  

    /**
     * Test of equals method, of class Revisor.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
    Utilizador u1 = new Utilizador();
        Revisor instance1 = new Revisor(u1);
        Utilizador u = new Utilizador();
        Revisor instance = new Revisor(u);
        boolean expResult = true;
        boolean result = instance.equals(instance1);
        assertEquals(expResult, result);
    }

    

}
