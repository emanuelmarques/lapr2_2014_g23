/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class ListaAutoresTest {
    
    public ListaAutoresTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addAutor method, of class ListaAutores.
     */
    @Test
    public void testAddAutor() {
        System.out.println("addAutor");
        Autor autor = new Autor();
        ListaAutores instance = new ListaAutores();
        autor.setAfiliacao("sf");
        autor.setEMail("asd@ad.a");
        autor.setNome("safda");
        autor.setUtilizador(new Utilizador());
        
        boolean expResult = true;
        boolean result = instance.addAutor(autor);
        assertEquals(expResult, result);
      
    }

    

    /**
     * Test of getPossiveisAutoresCorrespondentes method, of class ListaAutores.
     */
    @Test
    public void testGetPossiveisAutoresCorrespondentes() {
        System.out.println("getPossiveisAutoresCorrespondentes");
        ListaAutores instance = new ListaAutores();
        Autor autor = new Autor();
        instance.m_listaAutores.add(autor);
        List<Autor> expResult =new ArrayList();
        
        List<Autor> result = instance.getPossiveisAutoresCorrespondentes();
        assertEquals(expResult, result);
        
    }
    
}
