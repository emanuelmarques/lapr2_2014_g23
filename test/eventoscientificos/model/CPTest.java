package eventoscientificos.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rita
 */
public class CPTest {

    public CPTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addMembroCP method, of class CP.
     */
    @Test
    public void testAddMembroCP() {
        System.out.println("addMembroCP");
        String strId = "";
        Utilizador u = new Utilizador("", "", "", "");
        Revisor r = new Revisor(u);
        CP instance = new CP();
        Revisor expResult = r;
        Revisor result = instance.addMembroCP(strId, u);
        assertEquals(expResult, result);
    }

    /**
     * Test of registaMembroCP method, of class CP.
     */
    @Test
    public void testRegistaMembroCP() {
        System.out.println("registaMembroCP");
        Utilizador u = new Utilizador();
        Revisor r = new Revisor(u);
        CP instance = new CP();
        boolean expResult = true;
        boolean result = instance.registaMembroCP(r);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class CP.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        CP instance = new CP();
        String expResult = "Membros de CP: ";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of cpString method, of class CP.
     */
    @Test
    public void testCpString() {
        System.out.println("cpString");
        CP instance = new CP();
        String expResult = "CP:"
                + "\nRevisores/Membros da CP: " + instance.getListaRevisores();
        String result = instance.cpString();
        assertEquals(expResult, result);
    }
}
