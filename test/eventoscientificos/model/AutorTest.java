/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author amartins
 */
public class AutorTest {

    public AutorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setNome method, of class Autor.
     */
    @Test
    public void testSetNome() {
        System.out.println("setNome");
        String strNome = "teste";
        Autor instance = new Autor();
        instance.setNome(strNome);
        assertEquals(strNome, instance.getNome());

    }

    /**
     * Test of setAfiliacao method, of class Autor.
     */
    @Test
    public void testSetAfiliacao() {
        System.out.println("setAfiliacao");
        String strAfiliacao = "teste";
        Autor instance = new Autor();
        instance.setAfiliacao(strAfiliacao);
        assertEquals(strAfiliacao, instance.getAfiliacao());

    }

    /**
     * Test of setEMail method, of class Autor.
     */
    @Test
    public void testSetEMail() {
        System.out.println("setEMail");
        String strEMail = "teste";
        Autor instance = new Autor();
        instance.setEMail(strEMail);
        assertEquals(strEMail, instance.getEMail());

    }

    /**
     * Test of setUtilizador method, of class Autor.
     */
    @Test
    public void testSetUtilizador() {
        System.out.println("setUtilizador");
        Utilizador utilizador = null;
        Autor instance = new Autor();
        instance.setUtilizador(utilizador);
        assertEquals(utilizador, instance.getUtilizador());;
    }

    /**
     * Test of valida method, of class Autor.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Autor instance = new Autor();
        boolean expResult = false;
        boolean result = instance.valida();

       
        assertEquals(expResult, result);
    }

    /**
     * Test of podeSerCorrespondente method, of class Autor.
     */
    @Test
    public void testPodeSerCorrespondente() {
        System.out.println("podeSerCorrespondente");
        Autor instance = new Autor();
        boolean expResult = false;
        boolean result = instance.podeSerCorrespondente();
        assertEquals(expResult, result);

    }

    /**
     * Test of toString method, of class Autor.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Autor instance = new Autor();
        String expResult = instance.getNome() + " - " + instance.getAfiliacao() + " - " + instance.getEMail();
        String result = instance.toString();
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class Autor.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        Autor instance = new Autor();
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);

    }

    /**
     * Test of hashCode method, of class Autor.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Autor instance = new Autor();
        int expResult = 11767;
        int result = instance.hashCode();
        assertEquals(expResult, result);

    }

}
