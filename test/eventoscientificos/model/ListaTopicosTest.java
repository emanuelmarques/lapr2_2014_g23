package eventoscientificos.model;

import java.util.*;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Rita
 */
public class ListaTopicosTest {
    
    public ListaTopicosTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   
    /**
     * Test of addTopico method, of class ListaTopicos.
     */
    @Test
    public void testAddTopico() {
        System.out.println("addTopico");
        Topico t = null;
        ListaTopicos instance = new ListaTopicos();
        boolean expResult = true;
        boolean result = instance.addTopico(t);
        assertEquals(expResult, result);
    }

    
   

    /**
     * Test of valida method, of class ListaTopicos.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Topico t = new Topico();
        ListaTopicos instance = new ListaTopicos();
        boolean expResult = true;
        boolean result = instance.valida(t);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of addAll method, of class ListaTopicos.
     */
    @Test
    public void testAddAll() {
        System.out.println("addAll");
        List<Topico> listaTopicos = new ArrayList<>();
        ListaTopicos instance = new ListaTopicos();
        instance.addAll(listaTopicos);
        boolean expResult = false;
        boolean result = instance.getListaTopicos().addAll(listaTopicos);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class ListaTopicos.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        ListaTopicos instance = new ListaTopicos();
        int expResult = 5;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class ListaTopicos.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        ListaTopicos instance = new ListaTopicos();
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
}
