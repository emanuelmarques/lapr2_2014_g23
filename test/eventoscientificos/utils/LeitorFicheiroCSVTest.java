/*
 * To change this license header; choose License Headers in Project Properties.
 * To change this template file; choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.utils;

import eventoscientificos.model.Artigo;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author vitor_000
 */
public class LeitorFicheiroCSVTest {

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of lerFicheiro method; of class LeitorFicheiroCSV.
     */
    @Test
    public void testLerFicheiro() throws FileNotFoundException {
        System.out.println("lerFicheiro");
        String test1 = "2016;12th International CDIO Conference, Turku, Finland;Turku University of Applied Sciences;Turku;Finland;Sunday, June 12, 2016";
        String test2 = "2015;11th International CDIO Conference, Chengdu, China;Chengdu University of Information Technology;Chengdu, Sichuan;China;Monday, June 8, 2015";
        String test3 = "2014;2014 Worldwide CDIO Fall Meeting;University of Chile;Santiago;Chile;Wednesday, November 12, 2014";
        List<String> test = new ArrayList<>();
        test.add(test1);
        test.add(test2);
        test.add(test3);

        //assertEquals(test, LeitorFicheiroCSV.lerFicheiro("EventList_CDIOTESTE.csv"));
    }
}
