/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.utils;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Autor;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisao;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class LeitorFicheiroRevisoesCSVTest {
    
    public LeitorFicheiroRevisoesCSVTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of lerFicheiro method, of class LeitorFicheiroRevisoesCSV.
     */
    @Test
    public void testLerFicheiro() {
         System.out.println("lerFicheiro");
        Empresa empresa = new Empresa();
        List<Revisao> expResult = new ArrayList<>();

        Artigo a = new Artigo();
        a.setTipoArtigo("Full Paper");
        a.setTitulo("EDUCATING ENGINEERING PRACTICE IN SIX DESIGN PROJECTS IN A ROW");
        Autor autor = a.getListaAutores().novoAutor("Aldert Kamp", "Delft University of Technology, Faculty of Aerospace Engineering");
        autor.setEMail("akamp@tudelft.nl");
        a.getListaAutores().addAutor(autor);
      
        Evento evento = empresa.getRegistaEventos().novoEvento();
        empresa.getRegistaEventos().getLe().add(evento);
        empresa.getRegistaEventos().getMapaEventos().put("CDIO09", evento);
        Submissao s = new Submissao(evento);
        s.setArtigo(a);
        evento.getSubmissoes().addSubmissao(s);
        s.setArtigo(a);
        evento.getSubmissoes().addSubmissao(s);
        evento.getSubmissoes().getMapaDeSubmissoes().put("CDIO06ARM", s);
        Revisao r = new Revisao();
        Utilizador u = new Utilizador();
        u.setEmail("hulk@marvel.com");
        r.setRevisor(new Revisor(u));
        r.setConfianca(3);
        r.setAdequacao(4);
        r.setOriginalidade(4);
        r.setQualidade(4);
        r.setRecomendacao("Accept");
        
        expResult.add(r);

        LeitorFicheiroRevisoesCSV instance = new LeitorFicheiroRevisoesCSV("/home/emanuelmarques/Documentos/lapr2/EventHist_CDIO_Reviews.csv", empresa);

        List<Revisao> result = instance.lerFicheiro();
        assertEquals( expResult,result);

    }
    @Test
     public void testAddFicheiro() throws Exception {
        System.out.println("addFicheiro");
       
        Empresa empresa = new Empresa();
        int expResult = 1;

        Artigo a = new Artigo();
        a.setTipoArtigo("Full Paper");
        a.setTitulo("EDUCATING ENGINEERING PRACTICE IN SIX DESIGN PROJECTS IN A ROW");
        Autor autor = a.getListaAutores().novoAutor("Aldert Kamp", "Delft University of Technology, Faculty of Aerospace Engineering");
        autor.setEMail("akamp@tudelft.nl");
        a.getListaAutores().addAutor(autor);
      
        Evento evento = empresa.getRegistaEventos().novoEvento();
        empresa.getRegistaEventos().getLe().add(evento);
        empresa.getRegistaEventos().getMapaEventos().put("CDIO09", evento);
        Submissao s = new Submissao(evento);
        s.setArtigo(a);
        evento.getSubmissoes().addSubmissao(s);
        s.setArtigo(a);
        evento.getSubmissoes().addSubmissao(s);
        evento.getSubmissoes().getMapaDeSubmissoes().put("CDIO06ARM", s);
        
        Revisao r = new Revisao();
        Utilizador u = new Utilizador();
        u.setEmail("hulk@marvel.com");
        r.setRevisor(new Revisor(u));
        r.setConfianca(3);
        r.setAdequacao(4);
        r.setOriginalidade(4);
        r.setQualidade(4);
        r.setRecomendacao("Accept");
        LeitorFicheiroRevisoesCSV instance = new LeitorFicheiroRevisoesCSV("/home/emanuelmarques/Documentos/lapr2/EventHist_CDIO_Reviews.csv", empresa);
       
        
         instance.lerFicheiro();
         


        int result = s.getListaRevisoes().getListaRevisoes().size();
        assertEquals(expResult, result);

    }
    
}
