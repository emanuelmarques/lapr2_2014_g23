/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.utils;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Autor;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class LeitorFicheiroArtigoCSVTest {

    public LeitorFicheiroArtigoCSVTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of lerFicheiro method, of class LeitorFicheiroArtigoCSV.
     */
    @Test
    public void testLerFicheiro() throws Exception {
        System.out.println("lerFicheiro");
        Empresa empresa = new Empresa();
        List<Submissao> expResult = new ArrayList<>();

        Artigo a = new Artigo();
        a.setTipoArtigo("Full Paper");
        a.setTitulo("EDUCATING ENGINEERING PRACTICE IN SIX DESIGN PROJECTS IN A ROW");
        Autor autor = a.getListaAutores().novoAutor("Aldert Kamp", "Delft University of Technology, Faculty of Aerospace Engineering");
        autor.setEMail("akamp@tudelft.nl");
        a.getListaAutores().addAutor(autor);

        Evento evento = empresa.getRegistaEventos().novoEvento();
        empresa.getRegistaEventos().getLe().add(evento);
        empresa.getRegistaEventos().getMapaEventos().put("CDIO09", evento);

        Submissao s = evento.getSubmissoes().novaSubmissao(evento);
        s.setArtigo(a);
        evento.getSubmissoes().addSubmissao(s);

        expResult.add(s);

        LeitorFicheiroArtigoCSV instance = new LeitorFicheiroArtigoCSV("/home/emanuelmarques/Documentos/lapr2/teste_papers.csv", empresa);

        List<Submissao> result = instance.lerFicheiro();
        assertEquals(expResult, result);
        
    }
    @Test
     public void testAddFicheiro() throws Exception {
        System.out.println("addFicheiro");
        Empresa empresa = new Empresa();
        int expResult = 1;

        Artigo a = new Artigo();
        a.setTipoArtigo("Full Paper");
        a.setTitulo("EDUCATING ENGINEERING PRACTICE IN SIX DESIGN PROJECTS IN A ROW");
        Autor autor = a.getListaAutores().novoAutor("Aldert Kamp", "Delft University of Technology, Faculty of Aerospace Engineering");
        autor.setEMail("akamp@tudelft.nl");
        a.getListaAutores().addAutor(autor);

        Evento evento = empresa.getRegistaEventos().novoEvento();
        empresa.getRegistaEventos().getLe().add(evento);
        empresa.getRegistaEventos().getMapaEventos().put("CDIO09", evento);

        Submissao s = evento.getSubmissoes().novaSubmissao(evento);
        s.setArtigo(a);
        evento.getSubmissoes().addSubmissao(s);

        


        int result = evento.getSubmissoes().getListaSubmissoes().size();
        assertEquals(expResult, result);

    }

}
