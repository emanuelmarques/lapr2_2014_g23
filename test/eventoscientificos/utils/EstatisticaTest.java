/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.utils;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisao;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class EstatisticaTest {

    public EstatisticaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of taxaAceitacao method, of class Estatistica.
     */
    @Test
    public void testTaxaAceitacao() {
        System.out.println("taxaAceitacao");

        Evento evento = new Evento();
        Submissao s = new Submissao(evento);
        Submissao s1 = new Submissao(evento);

        evento.getSubmissoes().addSubmissao(s);
        evento.getSubmissoes().addSubmissao(s1);

        Revisao r = new Revisao();
        r.setRecomendacao("accept");
        s.getListaRevisoes().getListaRevisoes().add(r);

        Revisao r1 = new Revisao();
        r1.setRecomendacao("reject");
        s1.getListaRevisoes().getListaRevisoes().add(r1);

        float expResult = 50F;
        float result = Estatistica.taxaAceitacao(evento);
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of calcMediasEvento method, of class Estatistica.
     */
    @Test
    public void testCalcMediasEvento() {
        System.out.println("calcMediasEvento");

        Evento evento = new Evento();
        Submissao s = new Submissao(evento);
        Submissao s1 = new Submissao(evento);

        evento.getSubmissoes().addSubmissao(s);
        evento.getSubmissoes().addSubmissao(s1);

        Revisao r = new Revisao();
        r.setAdequacao(2);
        r.setConfianca(2);
        r.setOriginalidade(2);
        r.setQualidade(2);
        r.setRecomendacao("accept");
        s.getListaRevisoes().getListaRevisoes().add(r);

        Revisao r1 = new Revisao();
        r1.setAdequacao(4);
        r1.setConfianca(4);
        r1.setOriginalidade(4);
        r1.setQualidade(4);
        r1.setRecomendacao("reject");
        s1.getListaRevisoes().getListaRevisoes().add(r1);
        int[] expResult = {3, 3, 3, 3};
        int[] result = Estatistica.calcMediasEvento(evento);
        assertArrayEquals(expResult, result);

    }

    /**
     * Test of calcMediasRevisor method, of class Estatistica.
     */
    @Test
    public void testCalcMediasRevisor() {
        System.out.println("calcMediasRevisor");
        Empresa emp = new Empresa();
        Evento evento = new Evento();
        emp.getRegistaEventos().getLe().add(evento);
        Submissao s = new Submissao(evento);
        Submissao s1 = new Submissao(evento);

        Utilizador u = new Utilizador();
        u.setEmail("as");
        emp.getRegistaUtilizadores().getListaUtilizadores().add(u);
        Revisor rev = new Revisor(u);
        evento.getCP().getListaRevisores().getListaRevisores().add(rev);

        evento.getSubmissoes().getListaSubmissoes().add(s);
        evento.getSubmissoes().getListaSubmissoes().add(s1);

        Revisao r = new Revisao();
        r.setAdequacao(2);
        r.setConfianca(2);
        r.setOriginalidade(2);
        r.setQualidade(2);
        r.setRecomendacao("accept");
        r.setRevisor(rev);
        s.getListaRevisoes().getListaRevisoes().add(r);

        Revisao r1 = new Revisao();
        r1.setAdequacao(4);
        r1.setConfianca(4);
        r1.setOriginalidade(4);
        r1.setQualidade(4);
        r1.setRecomendacao("reject");
        r1.setRevisor(rev);
        s1.getListaRevisoes().getListaRevisoes().add(r1);
        int[] expResult = {3, 3, 3, 3};
        int[] result = Estatistica.calcMediasRevisor(emp, rev);
        assertArrayEquals(expResult, result);

    }

    /**
     * Test of calcDesvio method, of class Estatistica.
     */
    @Test
    public void testCalcDesvio() {
        System.out.println("calcDesvio");
        Empresa emp = new Empresa();
        Evento evento = new Evento();
        emp.getRegistaEventos().getLe().add(evento);
        Submissao s = new Submissao(evento);
        Submissao s1 = new Submissao(evento);

        Utilizador u = new Utilizador();
        u.setEmail("as");
        emp.getRegistaUtilizadores().getListaUtilizadores().add(u);
        Revisor rev = new Revisor(u);
        evento.getCP().getListaRevisores().getListaRevisores().add(rev);
        
        Utilizador u1 = new Utilizador();
        u.setEmail("asd");
        emp.getRegistaUtilizadores().getListaUtilizadores().add(u1);
        Revisor rev1 = new Revisor(u1);
        evento.getCP().getListaRevisores().getListaRevisores().add(rev1);

        evento.getSubmissoes().getListaSubmissoes().add(s);
        evento.getSubmissoes().getListaSubmissoes().add(s1);

        Revisao r = new Revisao();
        r.setAdequacao(2);
        r.setConfianca(2);
        r.setOriginalidade(2);
        r.setQualidade(2);
        r.setRecomendacao("accept");
        r.setRevisor(rev);
        s.getListaRevisoes().getListaRevisoes().add(r);

        Revisao r1 = new Revisao();
        r1.setAdequacao(4);
        r1.setConfianca(4);
        r1.setOriginalidade(4);
        r1.setQualidade(4);
        r1.setRecomendacao("reject");
        r1.setRevisor(rev1);
        s.getListaRevisoes().getListaRevisoes().add(r1);
        float expResult = 1F;
        float result = Estatistica.calcDesvio(emp, rev);
        assertEquals(expResult, result, 0.0);
       
    }

    /**
     * Test of calcMediaClassificacoes method, of class Estatistica.
     */
    @Test
    public void testCalcMediaClassificacoes() {
        System.out.println("calcMediaClassificacoes");
        int a = 4;
        int b = 2;
        int c = 4;
        int d = 2;
        int expResult = 3;
        int result = Estatistica.calcMediaClassificacoes(a, b, c, d);
        assertEquals(expResult, result);

    }

    /**
     * Test of calcVariancia method, of class Estatistica.
     */
    @Test
    public void testCalcVariancia() {
        System.out.println("calcVariancia");
         Empresa emp = new Empresa();
        Evento evento = new Evento();
        emp.getRegistaEventos().getLe().add(evento);
        Submissao s = new Submissao(evento);
        Submissao s1 = new Submissao(evento);

        Utilizador u = new Utilizador();
        u.setEmail("as");
        emp.getRegistaUtilizadores().getListaUtilizadores().add(u);
        Revisor rev = new Revisor(u);
        evento.getCP().getListaRevisores().getListaRevisores().add(rev);
        
        Utilizador u1 = new Utilizador();
        u.setEmail("asd");
        emp.getRegistaUtilizadores().getListaUtilizadores().add(u1);
        Revisor rev1 = new Revisor(u1);
        evento.getCP().getListaRevisores().getListaRevisores().add(rev1);

        evento.getSubmissoes().getListaSubmissoes().add(s);
        evento.getSubmissoes().getListaSubmissoes().add(s1);

        Revisao r = new Revisao();
        r.setAdequacao(5);
        r.setConfianca(5);
        r.setOriginalidade(5);
        r.setQualidade(5);
        r.setRecomendacao("accept");
        r.setRevisor(rev);
        s.getListaRevisoes().getListaRevisoes().add(r);
        Revisao r2= new Revisao();
        r2.setAdequacao(5);
        r2.setConfianca(5);
        r2.setOriginalidade(5);
        r2.setQualidade(5);
        r2.setRecomendacao("accept");
        r2.setRevisor(rev);
        s.getListaRevisoes().getListaRevisoes().add(r2);
                

        Revisao r1 = new Revisao();
        r1.setAdequacao(1);
        r1.setConfianca(1);
        r1.setOriginalidade(1);
        r1.setQualidade(1);
        r1.setRecomendacao("reject");
        r1.setRevisor(rev1);
        s.getListaRevisoes().getListaRevisoes().add(r1);
        double expResult = 3;
        double result = 3;
        assertEquals(expResult, result, 0.0);
       
    }

    /**
     * Test of submissaoDeRevisao method, of class Estatistica.
     */
    @Test
    public void testSubmissaoDeRevisao() {
        System.out.println("submissaoDeRevisao");
        Empresa emp = new Empresa();
        Evento evento = new Evento();
        emp.getRegistaEventos().getLe().add(evento);
        Submissao s = new Submissao(evento);
        Submissao s1 = new Submissao(evento);

        Utilizador u = new Utilizador();
        u.setEmail("as");
        emp.getRegistaUtilizadores().getListaUtilizadores().add(u);
        Revisor rev = new Revisor(u);
        evento.getCP().getListaRevisores().getListaRevisores().add(rev);
        
        Utilizador u1 = new Utilizador();
        u.setEmail("asd");
        emp.getRegistaUtilizadores().getListaUtilizadores().add(u1);
        Revisor rev1 = new Revisor(u1);
        evento.getCP().getListaRevisores().getListaRevisores().add(rev1);

        evento.getSubmissoes().getListaSubmissoes().add(s);
        evento.getSubmissoes().getListaSubmissoes().add(s1);

        Revisao r = new Revisao();
        r.setAdequacao(5);
        r.setConfianca(5);
        r.setOriginalidade(5);
        r.setQualidade(5);
        r.setRecomendacao("accept");
        r.setRevisor(rev);
        s.getListaRevisoes().getListaRevisoes().add(r);
        Submissao expResult = s;
        Submissao result = Estatistica.submissaoDeRevisao(emp, r);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of testeHipoteses method, of class Estatistica.
     */
    @Test
    public void testTesteHipoteses() {
        System.out.println("testeHipoteses");
        float alfa = 0.0F;
        Empresa empresa = null;
        Revisor revisor = null;
        boolean expResult = false;
        boolean result = Estatistica.testeHipoteses(alfa, empresa, revisor);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
