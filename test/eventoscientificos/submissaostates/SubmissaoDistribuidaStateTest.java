/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.submissaostates;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Distribuicao;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisao;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class SubmissaoDistribuidaStateTest {
    
    public SubmissaoDistribuidaStateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class SubmissaoDistribuidaState.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        
        Empresa emp= new Empresa();
        Evento e = new Evento();
        emp.getRegistaEventos().getLe().add(e);
        Submissao sub = new Submissao(e);
        e.getSubmissoes().getListaSubmissoes().add(sub);
        Distribuicao d = new Distribuicao();
        Artigo a = new Artigo();
        d.setArtigo(a);
        d.getRevisores().add(new Revisor (new Utilizador()));
        e.getProcessoDistribuicao().getListaDistribuicoes().getListaDitr().add(d);
        sub.setArtigo(a);
        sub.getListaRevisoes().getListaRevisoes().add(new Revisao ());
        
        SubmissaoDistribuidaState instance = new SubmissaoDistribuidaState(sub, e);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        
    }

   
    
}
