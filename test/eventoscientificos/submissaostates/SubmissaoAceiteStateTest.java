/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.submissaostates;

import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class SubmissaoAceiteStateTest {
    
    public SubmissaoAceiteStateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class SubmissaoAceiteState.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Evento e =  new Evento();
        Submissao sub = new Submissao(e);
        sub.setPagamentoEfetuado();
        SubmissaoAceiteState instance = new SubmissaoAceiteState(sub);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        
    }

    
    
}
