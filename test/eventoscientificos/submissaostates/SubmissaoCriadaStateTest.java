/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.submissaostates;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Autor;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Topico;
import eventoscientificos.model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class SubmissaoCriadaStateTest {
    
    public SubmissaoCriadaStateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class SubmissaoCriadaState.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Empresa emp =  new Empresa();
        Evento e = new Evento();
        emp.getRegistaEventos().getLe().add(e);
        
        Submissao sub = new Submissao(e);
        e.getSubmissoes().getListaSubmissoes().add(sub);
        Artigo a = new Artigo();
        sub.setArtigo(a);
        a.setTitulo("asdfa");
        a.setFicheiro("dasd");
        a.setResumo("asdasd");
        a.getM_listaTopicos().getListaTopicos().add(new Topico("asd","asd"));
        Autor au= new Autor ();
        au.setUtilizador(new Utilizador());
        a.getListaAutores().getListaAutores().add(au);
        SubmissaoCriadaState instance = new SubmissaoCriadaState(sub, e);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
   
    }

    
    
}
