/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.eventstates;

import eventoscientificos.model.Evento;
import eventoscientificos.model.FormulaDePagamento1;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class EventoRegistadoStateTest {

    public EventoRegistadoStateTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class EventoRegistadoState.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Evento e = new Evento();
        e.setFormulaDePagamento(new FormulaDePagamento1(e));
        e.setPrecoFullPaper(10);
        e.setPrecoPosterPaper(10);
        e.setPrecoShortPaper(10);
        EventoRegistadoState instance = new EventoRegistadoState(e);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);

    }

}
