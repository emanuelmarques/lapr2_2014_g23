/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.eventstates;

import eventoscientificos.model.CP;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Organizador;
import eventoscientificos.model.Utilizador;
import eventoscientificos.utils.Data;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class EventoCriadoPorFicheiroCSVStateTest {

    public EventoCriadoPorFicheiroCSVStateTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class EventoCriadoPorFicheiroCSVState.
     */
    @Test
    public void testValida() {
        System.out.println("valida");

        Empresa emp = new Empresa();
        Utilizador u = new Utilizador();
        Evento e = new Evento();
        emp.getRegistaEventos().getLe().add(e);
        emp.getRegistaUtilizadores().getListaUtilizadores().add(u);
        u.setUsername("teste");
        e.setCP(new CP());
        e.setTitulo("sfd");
        e.setDataFim(new Data());
        e.setDataInicio(new Data());
        e.setDataLimiteDeRegistoDeAutores(new Data());
        e.setDataLimiteDeRevisao(new Data());
        e.setDataLimiteDeSubmissaoFinal(new Data());
        e.setDescricao("tesfe");
        e.setDataLimiteSubmissao(new Data());
        e.setLocal("fd");
        Organizador o = new Organizador(u);
        e.getListaOrganizadores().addOrganizador(o);

        EventoCriadoPorFicheiroCSVState instance = new EventoCriadoPorFicheiroCSVState(e);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);

    }

}
