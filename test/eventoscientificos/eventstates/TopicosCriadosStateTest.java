/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.eventstates;

import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class TopicosCriadosStateTest {

    public TopicosCriadosStateTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class TopicosCriadosState.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Evento e = new Evento();
        e.getCP().getListaRevisores().getListaRevisores().add(new Revisor(new Utilizador()));
        TopicosCriadosState instance = new TopicosCriadosState(e);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);

    }

}
