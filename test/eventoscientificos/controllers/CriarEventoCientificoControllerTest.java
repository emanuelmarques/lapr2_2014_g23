/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.model.CP;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.ListaOrganizadores;
import eventoscientificos.model.Organizador;
import eventoscientificos.model.Utilizador;
import eventoscientificos.utils.Data;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class CriarEventoCientificoControllerTest {

    public CriarEventoCientificoControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addOrganizador method, of class CriarEventoCientificoController.
     */
    @Test
    public void testAddOrganizador() {
        System.out.println("addOrganizador");
        Empresa emp = new Empresa();
        Utilizador u = new Utilizador();
        Evento e = new Evento();
        emp.getRegistaEventos().getLe().add(e);
        emp.getRegistaUtilizadores().getListaUtilizadores().add(u);
        u.setUsername("teste");
        CriarEventoCientificoController instance = new CriarEventoCientificoController(emp);
        instance.setEvento(e);
        boolean expResult = true;
        boolean result = instance.addOrganizador("teste");
        assertEquals(expResult, result);

    }

    /**
     * Test of registaEvento method, of class CriarEventoCientificoController.
     */
    @Test
    public void testRegistaEvento() {
        System.out.println("registaEvento");
        Empresa emp = new Empresa();
        Utilizador u = new Utilizador();
        Evento e = new Evento();
        emp.getRegistaEventos().getLe().add(e);
        emp.getRegistaUtilizadores().getListaUtilizadores().add(u);
        u.setUsername("teste");
        e.setCP(new CP());
        e.setTitulo("sfd");
        e.setDataFim(new Data());
        e.setDataInicio(new Data());
        e.setDataLimiteDeRegistoDeAutores(new Data());
        e.setDataLimiteDeRevisao(new Data());
        e.setDataLimiteDeSubmissaoFinal(new Data());
        e.setDescricao("tesfe");
        e.setDataLimiteSubmissao(new Data());
        e.setLocal("fd");
        Organizador o = new Organizador(u);
        e.getListaOrganizadores().addOrganizador(o);
        
        CriarEventoCientificoController instance = new CriarEventoCientificoController(emp);
        instance.setEvento(e);
        boolean expResult = true;
        boolean result = instance.registaEvento();
        assertEquals(expResult, result);

    }

  

}
