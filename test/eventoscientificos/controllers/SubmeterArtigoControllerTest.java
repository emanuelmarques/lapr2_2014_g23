/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controllers;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Autor;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Topico;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class SubmeterArtigoControllerTest {
    
    public SubmeterArtigoControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   

    

   

    /**
     * Test of getPossiveisAutoresCorrespondentes method, of class SubmeterArtigoController.
     */
    @Test
    public void testGetPossiveisAutoresCorrespondentes() {
        System.out.println("getPossiveisAutoresCorrespondentes");
        Empresa emp = new Empresa();
        SubmeterArtigoController instance = new SubmeterArtigoController(emp);
        Artigo a = new Artigo();
        
        Utilizador u = new Utilizador();
        instance.setArtigo(a);
        Autor aut = new Autor();
        aut.setUtilizador(u);
        a.getListaAutores().getListaAutores().add(aut);
        List<Autor> expResult = new ArrayList<>();
        expResult.add(aut);
        List<Autor> result = instance.getPossiveisAutoresCorrespondentes();
        assertEquals(expResult, result);
  
    }

    


    
}
