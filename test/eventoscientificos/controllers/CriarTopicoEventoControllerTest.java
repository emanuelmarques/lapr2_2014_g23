/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controllers;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Topico;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class CriarTopicoEventoControllerTest {
    
    public CriarTopicoEventoControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getEventosOrganizador method, of class CriarTopicoEventoController.
     */
    @Test
    public void testGetEventosOrganizador() {
        System.out.println("getEventosOrganizador");
        
        Empresa instanceEmp = new Empresa();
        CriarTopicoEventoController instance = new CriarTopicoEventoController(instanceEmp);

        Evento instanceE = new Evento();
        instanceEmp.getRegistaEventos().getLe().add(instanceE);

        Utilizador instanceU = new Utilizador();
        instanceU.setUsername("teste");
        instanceE.getListaOrganizadores().addOrganizador(instanceU);
        instanceEmp.getRegistaUtilizadores().getListaUtilizadores().add(instanceU);

        List<Evento> expResult = new ArrayList<>();
        expResult.add(instanceE);

        
        List<Evento> result = instance.getEventosOrganizador("teste");
        assertEquals(expResult, result);
    
    }

   
    /**
     * Test of addTopico method, of class CriarTopicoEventoController.
     */
    @Test
    public void testAddTopico() {
        System.out.println("addTopico");
        
        Empresa emp = new Empresa();
        
        CriarTopicoEventoController instance = new CriarTopicoEventoController(emp);
        instance.setEvento(new Evento());
        Topico expResult = new Topico("teste", "teste");
        Topico result = instance.addTopico("teste", "teste");
        assertEquals(expResult, result);
 
    }

    /**
     * Test of registaTopico method, of class CriarTopicoEventoController.
     */
    @Test
    public void testRegistaTopico() {
        System.out.println("registaTopico");
        Topico t = new Topico("s", "s");
        Empresa emp = new Empresa();
        CriarTopicoEventoController instance = new CriarTopicoEventoController(emp);
        instance.setEvento(new Evento());
        boolean expResult = true;
        boolean result = instance.registaTopico(t);
        assertEquals(expResult, result);
       
        
    }

    /**
     * Test of iniciarCricao method, of class CriarTopicoEventoController.
     */
    @Test
    public void testIniciarCricao() {
        System.out.println("iniciarCricao");
        
        
        Empresa instanceEmp = new Empresa();
        CriarTopicoEventoController instance = new CriarTopicoEventoController(instanceEmp);

        Evento instanceE = new Evento();
        instanceEmp.getRegistaEventos().getLe().add(instanceE);

        Utilizador instanceU = new Utilizador();
        instanceU.setUsername("teste");
        instanceE.getListaOrganizadores().addOrganizador(instanceU);
        instanceEmp.getRegistaUtilizadores().getListaUtilizadores().add(instanceU);

        List<Evento> expResult = new ArrayList<>();
        expResult.add(instanceE);


        List<Evento> result = instance.iniciarCricao("teste");
        assertEquals(expResult, result);

    }

    /**
     * Test of novoTopico method, of class CriarTopicoEventoController.
     */
    @Test
    public void testNovoTopico() {
        System.out.println("novoTopico");
       Empresa emp = new Empresa();
        CriarTopicoEventoController instance = new CriarTopicoEventoController(emp);
        Evento e = new Evento();
        
        instance.setEvento(e);
        Topico expResult = new Topico("s","s");
        Topico result = instance.novoTopico("s", "s");
        assertEquals(expResult, result);
     
    }

   
    /**
     * Test of equals method, of class CriarTopicoEventoController.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Empresa emp = new Empresa();
        Evento evento=new Evento();
        CriarTopicoEventoController instance = new CriarTopicoEventoController(emp);
        instance.setEvento(evento);
        instance.addTopico("s", "s");
        CriarTopicoEventoController instance1 = new CriarTopicoEventoController(emp);
        instance1.setEvento(evento);
        instance1.addTopico("s", "s");
        boolean expResult = true;
        boolean result = instance.equals(instance1);
        assertEquals(expResult, result);
     
    }
    
}
