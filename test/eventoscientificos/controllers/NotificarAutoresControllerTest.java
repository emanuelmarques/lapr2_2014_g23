/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controllers;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class NotificarAutoresControllerTest {
    
    public NotificarAutoresControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getEventosOrganizador method, of class NotificarAutoresController.
     */
    @Test
    public void testGetEventosOrganizador() {
        System.out.println("getEventosOrganizador");
       
        
        Empresa instanceEmp = new Empresa();
        NotificarAutoresController instance = new NotificarAutoresController(instanceEmp);

        Evento instanceE = new Evento();
        instanceEmp.getRegistaEventos().getLe().add(instanceE);

        Utilizador instanceU = new Utilizador();
        instanceU.setUsername("teste");
        instanceE.getListaOrganizadores().addOrganizador(instanceU);
        instanceEmp.getRegistaUtilizadores().getListaUtilizadores().add(instanceU);

        List<Evento> expResult = new ArrayList<>();
        expResult.add(instanceE);
        
        
        
        List<Evento> result = instance.getEventosOrganizador("teste");
        assertEquals(expResult, result);
  
    }



    

    
}
