/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controllers;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisao;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class ReverArtigoControllerTest {
    
    public ReverArtigoControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getEventosRevisor method, of class ReverArtigoController.
     */
    @Test
    public void testGetEventosRevisor() {
        System.out.println("getEventosRevisor");
        Empresa emp = new Empresa();
        Evento e = new Evento();
        emp.getRegistaEventos().getLe().add(e);
        Utilizador u = new Utilizador();
        emp.getRegistaUtilizadores().getListaUtilizadores().add(u);
        u.setUsername("teste");
        Revisor r = new Revisor (u);
        e.getCP().getListaRevisores().getListaRevisores().add(r);
        ReverArtigoController instance = new ReverArtigoController(emp);
        instance.setEvento(e);
        List<Evento> expResult = new ArrayList();
        expResult.add(e);
        List<Evento> result = instance.getEventosRevisor("teste");
        assertEquals(expResult, result);
   
    }

   

    /**
     * Test of validaRevisao method, of class ReverArtigoController.
     */
    @Test
    public void testValidaRevisao() {
        System.out.println("validaRevisao");
             Empresa emp = new Empresa();
        Revisao r = new Revisao();
        r.setAdequacao(1);
        r.setConfianca(2);
        r.setOriginalidade(3);
        r.setQualidade(4);
        r.setRecomendacao("Accept");
        ReverArtigoController instance = new ReverArtigoController(emp);
        Evento e = new Evento();
        instance.setEvento(e);
        Artigo a = new Artigo();
        Submissao s = new Submissao(e);
        s.setArtigo(a);
        e.getSubmissoes().addSubmissao(s);
        s.getListaRevisoes().getListaRevisoes().add(r);
        instance.setArtigo(a);
        boolean expResult = false;
        boolean result = instance.validaRevisao(r);
        assertEquals(expResult, result);
 
    }

   

    

    /**
     * Test of submissaoDoArtigo method, of class ReverArtigoController.
     */
    @Test
    public void testSubmissaoDoArtigo() {
        System.out.println("submissaoDoArtigo");
        
        
         Empresa emp = new Empresa();
        Revisao r = new Revisao();
        r.setAdequacao(1);
        r.setConfianca(2);
        r.setOriginalidade(3);
        r.setQualidade(4);
        r.setRecomendacao("Accept");
        ReverArtigoController instance = new ReverArtigoController(emp);
        Evento e = new Evento();
        instance.setEvento(e);
        Artigo a = new Artigo();
        Submissao s = new Submissao(e);
        s.setArtigo(a);
        e.getSubmissoes().addSubmissao(s);
        instance.setArtigo(a);
        
        Submissao expResult = s;
        Submissao result = instance.submissaoDoArtigo();
        assertEquals(expResult, result);
      
    }

   
    
}
