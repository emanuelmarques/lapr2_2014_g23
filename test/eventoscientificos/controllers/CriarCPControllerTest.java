/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.model.CP;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Organizador;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Topico;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author vitor_000
 */
public class CriarCPControllerTest {

    public CriarCPControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getEventosOrganizador method, of class CriarCPController.
     */
    @Test
    public void getEventosOrganizador() {
        System.out.println("Teste getEventosOrganizador");
        Empresa instanceEmp = new Empresa();
        CriarCPController cpCon = new CriarCPController(instanceEmp);

        Evento instanceE = new Evento();
        instanceEmp.getRegistaEventos().getLe().add(instanceE);

        Utilizador instanceU = new Utilizador();
        instanceU.setUsername("teste");
        instanceE.getListaOrganizadores().addOrganizador(instanceU);
        instanceEmp.getRegistaUtilizadores().getListaUtilizadores().add(instanceU);

        List<Evento> expResult = new ArrayList<>();
        expResult.add(instanceE);

        List<Evento> real = cpCon.getEventosOrganizador("teste");

        assertEquals(expResult, real);
    }

    /**
     * Test of addMembroCP method, of class CriarCPController.
     */
    @Test
    public void addMembroCP() {
        System.out.println("Teste addMembroCP");

        Empresa instanceEmp = new Empresa();
        CriarCPController cpCon = new CriarCPController(instanceEmp);

        Utilizador instanceU = new Utilizador();
        instanceU.setUsername("teste");
        instanceEmp.getRegistaUtilizadores().getListaUtilizadores().add(instanceU);

        Revisor real = cpCon.addMembroCP("teste");
        Revisor expResult = new Revisor(instanceU);

        assertEquals(expResult, real);

    }

    /**
     * Test of criarTopico method, of class CriarCPController.
     */
    @Test
    public void criarTopicoController() {

        System.out.println("Teste criarTopico (controller)");

        Empresa instanceEmp = new Empresa();
        CriarCPController cpCon = new CriarCPController(instanceEmp);
        CriarTopicoEventoController tCon = new CriarTopicoEventoController(instanceEmp);

        Evento evento = new Evento();
        cpCon.setEvento(evento);
        tCon.setEvento(evento);

        CriarTopicoEventoController real = cpCon.criarTopico();

        assertEquals(tCon, real);

    }

   
    

   

    /**
     * Test of addMembroCP method, of class CriarCPController.
     */
    @Test
    public void testAddMembroCP() {
        System.out.println("addMembroCP");
        Empresa emp=new Empresa();
        Utilizador u= new Utilizador();
        emp.getRegistaUtilizadores().getListaUtilizadores().add(u);
        u.setUsername("teste");
        CriarCPController instance = new CriarCPController(emp);
        Revisor expResult = new Revisor(u);
        Revisor result = instance.addMembroCP("teste");
        assertEquals(expResult, result);
        
    }

    
    /**
     * Test of criarTopico method, of class CriarCPController.
     */
    @Test
    public void testCriarTopico() {
        System.out.println("criarTopico");
        Empresa emp = new Empresa();
                Evento e = new Evento();
        CriarCPController instance = new CriarCPController(emp) ;
        instance.setEvento(e);
        CriarTopicoEventoController controllerCTE= new CriarTopicoEventoController(emp);
            controllerCTE.setEvento(e);
        CriarTopicoEventoController result = instance.criarTopico();
        assertEquals(controllerCTE, result);
     
    }

   

}
