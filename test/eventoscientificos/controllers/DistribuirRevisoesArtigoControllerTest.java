/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controllers;

import eventoscientificos.model.Distribuicao;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.ListaMecanismosDistribuicao;
import eventoscientificos.model.MecanismoDistribuicao;
import eventoscientificos.model.ProcessoDistribuicao;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class DistribuirRevisoesArtigoControllerTest {
    
    public DistribuirRevisoesArtigoControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of novaDistribuicao method, of class DistribuirRevisoesArtigoController.
     */
    @Test
    public void testNovaDistribuicao() {
        System.out.println("novaDistribuicao");
        
        Empresa instanceEmp = new Empresa();
        DistribuirRevisoesArtigoController instance = new DistribuirRevisoesArtigoController(instanceEmp);

        Evento instanceE = new Evento();
        instanceEmp.getRegistaEventos().getLe().add(instanceE);

        Utilizador instanceU = new Utilizador();
        instanceU.setUsername("teste");
        instanceE.getListaOrganizadores().addOrganizador(instanceU);
        instanceEmp.getRegistaUtilizadores().getListaUtilizadores().add(instanceU);

        List<Evento> expResult = new ArrayList<>();
        expResult.add(instanceE);
        
        

        List<Evento> result = instance.novaDistribuicao("teste");
        assertEquals(expResult, result);
     
    }

  
   

    
    
}
