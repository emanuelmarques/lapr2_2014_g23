/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisao;
import eventoscientificos.model.Submissao;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emanuelmarques
 */
public class EstatisticasEventoControllerTest {

    public EstatisticasEventoControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of calcMedias method, of class EstatisticasEventoController.
     */
    @Test
    public void testCalcMedias() {
        System.out.println("calcMedias");
        Empresa empresa = new Empresa();
        Evento evento = empresa.getRegistaEventos().novoEvento();
        empresa.getRegistaEventos().getLe().add(evento);
        EstatisticasEventoController instance = new EstatisticasEventoController(evento);
        Revisao r1 = new Revisao();

        r1.setAdequacao(2);
        r1.setConfianca(2);
        r1.setOriginalidade(2);
        r1.setQualidade(4);
        Revisao r2 = new Revisao();
        r2.setAdequacao(4);
        r2.setConfianca(4);
        r2.setOriginalidade(4);
        r2.setQualidade(2);
        Submissao s = new Submissao(evento);
        evento.getSubmissoes().addSubmissao(s);
        s.getListaRevisoes().getListaRevisoes().add(r1);
        s.getListaRevisoes().getListaRevisoes().add(r2);

        int[] expResult = {3, 3, 3, 3};
        int[] result = instance.calcMedias();
        assertArrayEquals(expResult, result);

    }

    /**
     * Test of taxaAceitacao method, of class EstatisticasEventoController.
     */
    @Test
    public void testTaxaAceitacao() {
        System.out.println("taxaAceitacao");

        Empresa empresa = new Empresa();
        Evento evento = empresa.getRegistaEventos().novoEvento();
        empresa.getRegistaEventos().getLe().add(evento);
        EstatisticasEventoController instance = new EstatisticasEventoController(evento);
        Revisao r1 = new Revisao();
        r1.setRecomendacao("aceite");
        Revisao r2 = new Revisao();
        r2.setRecomendacao("rejeitado");
        Submissao s = new Submissao(evento);
        evento.getSubmissoes().addSubmissao(s);
        s.getListaRevisoes().getListaRevisoes().add(r1);
        s.getListaRevisoes().getListaRevisoes().add(r2);
        float expResult = 50F;
        float result = instance.taxaAceitacao();
        assertEquals(expResult, result, 0.0);

    }

}
