/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Revisor;
import eventoscientificos.utils.Estatistica;

/**
 *
 * @author emanuelmarques
 */
public class EstatisticaRevisoresController {

    private Revisor revisor;
    private Empresa empresa;

    public EstatisticaRevisoresController(Empresa empresa, Revisor revisor) {
        this.revisor = revisor;
        this.empresa = empresa;

    }

    public float getDesvio() {
        return Estatistica.calcDesvio(empresa, revisor);
    }

    public void setRevisor(Revisor r) {
        this.revisor = r;
    }

    public Revisor getRevisor() {
        return revisor;
    }
    public boolean testeHipoteses(float alfa){
        
        return Estatistica.testeHipoteses(alfa, empresa, revisor);
        
        
    }
}
