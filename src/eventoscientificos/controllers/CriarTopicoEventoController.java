/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Topico;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author iazevedo
 */
public class CriarTopicoEventoController {

    /**
     * variaveis de instancia
     */
    private Empresa m_empresa;
    private Evento m_evento;

    /**
     *construtor que recebe uma variavel empresa do tipo Empresa como paramentro
     * @param empresa
     */
    public CriarTopicoEventoController(Empresa empresa) {
        m_empresa = empresa;
    }

    /**
     *metodo que acede aos eventos do organizador recebendo o seu id como parametro
     * @param strId
     * @return lista de eventos do organizador
     */
    public List<Evento> getEventosOrganizador(String strId) {
        return m_empresa.getRegistaEventos().getListaEventosOrganizador(strId);
    }
    
    /**
     *metodo que acede ao evento
     * @return evento
     */
    public Evento getEvento() {
        return this.m_evento;
    }

    /**
     *metodo que modifica o evento recebendo um evento como parametro
     * @param e
     */
    public void setEvento(Evento e) {
        m_evento = e;
        
    }

    /**
     *metodo que adiciona topico recebedo o codigo e a descricao como parametro
     * @param strCodigo
     * @param strDescricao
     * @return o topico
     */
    public Topico addTopico(String strCodigo, String strDescricao) {
        Topico t = m_evento.getListaTopicos().novoTopico();

        t.setCodigoACM(strCodigo);
        t.setDescricao(strDescricao);

        if (m_evento.getListaTopicos().validaTopico(t)) {
            return t;
        } else {
            return null;
        }
    }

    /**
     *metodo que regista topico recebendo o como parametro
     * @param t
     * @return topico
     */
    public boolean registaTopico(Topico t) {
        return m_evento.getListaTopicos().addTopico(t);
    }

    /**
     *metodo que acede a lista de eventos do organizador recebendo o seu id como parametro
     * @param id
     * @return lista eventos organizador
     */
    public List<Evento> iniciarCricao(String id) {
        return this.m_empresa.getRegistaEventos().getListaEventosOrganizador(id);
    }

    /**
     *metodo que cria um novo topico recebendo o codigo e a descricao como parametro
     * @param codigo
     * @param descricao
     * @return topico
     */
    public Topico novoTopico(String codigo, String descricao) {
        Topico t = this.m_evento.getListaTopicos().novoTopico();
        t.setCodigoACM(codigo);
        t.setDescricao(descricao);
        if (this.m_evento.getListaTopicos().valida(t)) {
            return t;
        } else {
            return null;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CriarTopicoEventoController other = (CriarTopicoEventoController) obj;
        if (!Objects.equals(this.m_empresa, other.m_empresa)) {
            return false;
        }
        return Objects.equals(this.m_evento, other.m_evento);
    }

    public void setTopicosCriados() {
        this.m_evento.setTopicosCriados();
    }
    
    
}
