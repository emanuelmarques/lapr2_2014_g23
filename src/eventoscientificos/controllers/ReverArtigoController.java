/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.model.*;
import eventoscientificos.submissaostates.SubmissaoRevistaState;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vitor_000
 */
public class ReverArtigoController {

    /**
     * variaveis de instancia
     */
    private Empresa empresa;
    private Artigo artigo;
    private Evento evento;
    private Revisao revisao;

    /**
     * construtor que recebe uma empresa como parametro
     *
     * @param empresa
     */
    public ReverArtigoController(Empresa empresa) {
        this.empresa = empresa;
    }

    /**
     * metodo que acede aos eventos de um revisor recebendo o seu id como
     * parametro
     *
     * @param id
     * @return lista de eventos de revisor
     */
    public List<Evento> getEventosRevisor(String id) {

        return this.empresa.getRegistaEventos().getListaEventosRevisor(id);
    }

    /**
     * metodo que modifica evento recebendo o como parametro
     *
     * @param e
     */
    public void setEvento(Evento e) {
        this.evento = e;
    }

    /**
     * metodo que acede aos artigos de evento
     *
     * @return artigo
     */
    public List<Artigo> getArtigosEvento() {
        List<Artigo> la = new ArrayList<>();
        for (Submissao s : this.evento.getSubmissoes().getListaSubmissoes()) {
            Artigo a = s.getArtigo();
            la.add(a);
        }
        return la;
    }

    /**
     * metodo que modifica um artigo recebendo o como parametro
     *
     * @param a
     */
    public void setArtigo(Artigo a) {
        this.artigo = a;
    }

    /**
     * metodo que valida revisao recebendo um revisor como parametri
     *
     * @param r
     * @return true ou false
     */
    public boolean validaRevisao(Revisao r) {
        if (r.validaRevisao() && submissaoDoArtigo().getListaRevisoes().valida(r)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * metodo que regista revisao recebendo um revisor como parametro
     *
     * @param r
     */
    public void registaRevisao(Revisao r) {
        submissaoDoArtigo().registaRevisao(r);
    }

    /**
     * metodo que acede ao artigo
     *
     * @return
     */
    public Artigo getArtigo() {
        return artigo;
    }

    /**
     * metodo que faz submissao do artigo
     *
     * @return submissao
     */
    public Submissao submissaoDoArtigo() {
        for (Submissao sub : evento.getSubmissoes().getListaSubmissoes()) {
            if (this.artigo != null && sub.getArtigo().equals(this.artigo)) {
                return sub;
            }

        }
        return null;
    }

    /**
     * metodo que acede ao evento
     *
     * @return
     */
    public Evento getEvento() {
        return evento;
    }

    /**
     * metodo que faz nova revisao
     *
     * @return revisao
     */
    public Revisao novaRevisao() {
        this.revisao = new Revisao();

        return this.revisao;
    }

    /**
     * metodo que acede a revisao
     *
     * @return
     */
    public Revisao getRevisao() {
        return revisao;
    }

    public void setSubmissaoRevista() {
        this.submissaoDoArtigo().setSubmissaoRevista();
    }

    public void criarSubmissaoRevista() {
        this.submissaoDoArtigo().setState(new SubmissaoRevistaState(this.submissaoDoArtigo()));
    }

    public void setEventoRevisto() {
        this.evento.setEventoRevisto();
    }

}
