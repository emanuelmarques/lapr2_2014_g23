/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controllers;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import java.util.List;

/**
 *
 * @author vitor_000
 */
public class DecidirEventosImportadosController {
    
    private Evento evento;
    private Empresa empresa;

    public DecidirEventosImportadosController(Empresa empresa) {
        this.empresa = empresa;
    }

    /**
     * @return the evento
     */
    public Evento getEvento() {
        return evento;
    }

    /**
     * @return the empresa
     */
    public Empresa getEmpresa() {
        return empresa;
    }

    /**
     * @param evento the evento to set
     */
    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    /**
     * @param empresa the empresa to set
     */
    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
    
    public void setEventoRegistado() {
        this.evento.setEventoRegistado();
    }

    public void setEventoCriado() {
        this.evento.setEventoCriado();
    }

    public void setEventoTerminado() {
        this.evento.setEventoTerminado();
    }
    
    public void criarEstadoEventoTerminado() {
        this.evento.criarEstadoEventoTerminado();
    }

    public List<Evento> getEventos() {
        return this.empresa.getRegistaEventos().getLe();
    }
    
    
}
