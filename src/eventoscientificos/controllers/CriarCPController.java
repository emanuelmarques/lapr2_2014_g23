package eventoscientificos.controllers;

import eventoscientificos.model.*;
import java.util.*;

/**
 *
 * @author Nuno Silva
 */
public class CriarCPController {

    /**
     * variaveis de instancia
     */
    private Empresa m_empresa;
    private Evento m_evento;
    private CP m_cp;
    private Revisor m_revisor;

    /**
     *construtor que uma variável empresa do tipo Empresa
     * @param empresa
     */
    public CriarCPController(Empresa empresa) {
        m_empresa = empresa;
        m_cp = new CP();
        
    }

    /**
     *metodo que acede a lista de eventos do organizador recebendo o id do organizador como parametro
     * @param strId
     * @return
     */
    public List<Evento> getEventosOrganizador(String strId) {
        return getEmpresa().getRegistaEventos().getListaEventosOrganizador(strId);
    }

    /**
     *metodo que modifica o revisor
     * @param m_revisor
     */
    public void setRevisor(Revisor m_revisor) {
        this.m_revisor = m_revisor;
    }

    /**
     *metodo que acede aos topicos de evento
     * @return a lista de topicos
     */
    public List<Topico> getTopicosEvento() {
        if (getEvento() != null) {
            return getEvento().getListaTopicos().getListaTopicos();
        } else {
            return null;
        }
    }

    /**
     *metodo que seleciona evento recebendo uma variavel e do tipo evento
     * @param e
     */
    public void selectEvento(Evento e) {
        setEvento(e);

        m_cp = getEvento().novaCP();
    }

    /**
     *metodo que adiciona um membro CP recebendo o seu id como parametro
     * @param strId
     * @return o membro adicionado
     */
    public Revisor addMembroCP(String strId) {
        Utilizador u = getEmpresa().getRegistaUtilizadores().getUtilizador(strId);

        if (u != null) {
            return m_cp.addMembroCP(strId, u);
        } else {
            return null;
        }
    }

    /**
     * metodo que regista um membro CP recebendo uma variavel r do tipo revisor
     * @param r
     * @return o membro registado
     */
    public boolean registaMembroCP(Revisor r) {
        return m_cp.registaMembroCP(r);
    }

    /**
     *metodo que modifica CP
     */
    public void setCP() {
        getEvento().setCP(m_cp);
    }

    /**
     *metodo que modifica lista de topicos de um revisor recebendo um revisor e a sua lista de topicos como parametro
     * @param r, lista de topicos de revisor
     * @param listaTopicosRevisor
     */
    public void setListaTopicosRevisor(Revisor r, List<Topico> listaTopicosRevisor) {
        if (m_cp.getListaRevisores().getListaRevisores().contains(r)) {
            r.setListaTopicos(listaTopicosRevisor);
        }
    }

    /**
     *metodo que acede a empresa
     * @return empresa
     */
    public Empresa getEmpresa() {
        return m_empresa;
    }

    /**
     *metodo que acede a string CP
     * @return String CP
     */
    public String getCPString() {
        return m_cp.cpString();
    }

    /**
     *metodo que acede a CP
     * @return CP
     */
    public CP getCP() {
        return this.m_cp;
    }

    /**
     *metodo que acede ao evento
     * @return evento
     */
    public Evento getEvento() {
        return m_evento;
    }

    /**
     *metodo que modifica evento recebendo uma variavel m_evento do tipo Evento como parametro
     * @param m_evento
     */
    public void setEvento(Evento m_evento) {
        this.m_evento = m_evento;
    }

    /**
     *metodo que cria topico
     * @return controllerCTE
     */
    public CriarTopicoEventoController criarTopico() {
            CriarTopicoEventoController controllerCTE= new CriarTopicoEventoController(this.m_empresa);
            controllerCTE.setEvento(m_evento);
           return controllerCTE;
            
    }

    /**
     *metodo que adiciona topico recebendo um por parametro
     * @param t
     */
    public void addTopico(Topico t) {
        getM_revisor().addTopicoPericia(t);
    }

    /**
     * @return o atributo revisor
     */
    public Revisor getM_revisor() {
        return m_revisor;
    }

}
