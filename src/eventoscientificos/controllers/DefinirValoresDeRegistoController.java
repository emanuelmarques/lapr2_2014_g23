/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controllers;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.FormulasDePagamento;
import java.util.List;

/**
 *
 * @author vitor_000
 */
public class DefinirValoresDeRegistoController {
    
    /**
     * variaveis de instancia
     */
    private Empresa empresa;
    private Evento evento;

    /**
     *construtor que recebe uma empresa como parametro
     * @param empresa
     */
    public DefinirValoresDeRegistoController(Empresa empresa) {
        this.empresa = empresa;
    }

    /**
     *metodo que acede a empresa
     * @return empresa
     */
    public Empresa getEmpresa() {
        return empresa;
    }

    /**
     *metodo que acede ao evento
     * @return evento
     */
    public Evento getEvento() {
        return evento;
    }

    /**
     *metodo que modifica a empresa
     * @param empresa
     */
    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    /**
     *metodo que modifica o evento
     * @param evento
     */
    public void setEvento(Evento evento) {
        this.evento = evento;
    }
    
    /**
     *metodo que modifica o preco por cada full paper recebendo o preco como parametro
     * @param price
     */
    public void setPrecoPorCadaFullPaper(float price) {
        this.evento.setPrecoFullPaper(price);
    }
    
    /**
     *metodo que modifica o preco por cada short paper recebendo o preco como parametro
     * @param price
     */
    public void setPrecoPorCadaShortPaper(float price) {
        this.evento.setPrecoShortPaper(price);
    }
    
    /**
     *metodo que modifica o preco por cada poster paper recebendo o preco como parametro
     * @param price
     */
    public void setPrecoPorCadaPosterPaper(float price) {
        this.evento.setPrecoPosterPaper(price);
    }
    
    /**
     *metodo que modifica a formula de pagamentro recebendo a como parametro
     * @param f
     */
    public void setFormulaDePagamento(FormulasDePagamento f) {
        this.evento.setFormulaDePagamento(f);
    }

    /**
     *metodo que acede aos valores String
     * @return valores String
     */
    public String getValoresString() {
        return this.evento.getValoresSring();
    }
    
    /**
     *metodo que modifica o estado de valores de registo definido da empresa
     */
    public void setEventoStateValoresDeRegistoDefinido() {
        this.evento.setValoresDeRegistoDefinido();
    }

    public List<Evento> getListaEventosOrganizador(String username) {
        return this.empresa.getRegistaEventos().getListaEventosOrganizador(username);
    }
}
