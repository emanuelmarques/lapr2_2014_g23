

package eventoscientificos.controllers;


import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import eventoscientificos.utils.CriarNotificacao;
import java.io.FileNotFoundException;
import java.util.List;

/**
 *
 * @author Paulo Silva
 */
public class NotificarAutoresController {
    
    /**
     * variaveis de instancia
     */
    private Empresa m_empresa;
   
    private Evento m_evento;
  
    /**
     *construtor que recebe uma empresa como parametro
     * @param empresa
     */
    public NotificarAutoresController(Empresa empresa){
        m_empresa=empresa;
    }
    
    /**
     *metodo que acede aos eventos do organizdor recebendo as variaveis email e u como parametro
     * @param u
     * @param email
     * @return lista de eventos do organizador
     */
    public List<Evento> getEventosOrganizador(String u){
        return this.m_empresa.getRegistaEventos().getListaEventosOrganizador(u);
        
    }
    
    /**
     *metodo que seleciona evento recebendo um evento como parametro
     * @param e
     */
    public void selectEvento(Evento e){
        m_evento=e;
    }
    
    /**
     *metodo que acede aos artigos de evento recebendo as variaveis u e email como parametro
     * @param u
     * @param email
     * @return lista de submissoes
     */
    public List<Submissao> getArtigosEvento(String u, String email){
        return m_evento.getSubmissoes().getListaSubmissoes();
        
    }

    /**
     *metodo que notifica autores
     * @throws FileNotFoundException
     */
    public void notificarAutores() throws FileNotFoundException{
        CriarNotificacao c = new CriarNotificacao(this.m_evento);
        c.notificarEvento();
    }

    /**
     *metodo que acede ao evento
     * @return evento
     */
    public Evento getEvento() {
        return m_evento;
    }

    public void setEventoNotificado() {
        this.m_evento.setEventoNotificado();
    }
    
    
    
}
