/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisao;
import eventoscientificos.utils.Estatistica;
import java.util.List;

/**
 *
 * @author emanuelmarques
 */
public class EstatisticasEventoController {

    /**
     * variavel de instancia
     */
    private Evento evento;

    /**
     * construtor que recebe um evento como parametro
     *
     * @param evento
     */
    public EstatisticasEventoController(Evento evento) {
        this.evento = evento;
    }

    /**
     * metodo que calcula as medias das avaiacoes dos revisores
     *
     * @return medias
     */
    public int[] calcMedias() {

        return Estatistica.calcMediasEvento(evento);
    }

    /**
     * metodo que calcula a taxa de aceitacao
     *
     * @return valor da taxa
     */
    public float taxaAceitacao() {
        return Estatistica.taxaAceitacao(evento);

    }
}
