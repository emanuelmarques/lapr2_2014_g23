package eventoscientificos.controllers;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Utilizador;

/**
 *
 * @author Paulo Silva
 */
public class RegistarUtilizadorController {

    /**
     * variaveis de instancia
     */
    private Empresa m_empresa;
    private Utilizador m_utilizador;

    /**
     *construtor que recebe uma empresa como parametro
     * @param empresa
     */
    public RegistarUtilizadorController(Empresa empresa) {
        m_empresa = empresa;
    }

    /**
     *metodo que cria um novo utilizador
     */
    public void novoUtilizador() {
        m_utilizador = m_empresa.getRegistaUtilizadores().novoUtilizador();
    }

    /**
     *metodo que modifica os dados recebendo o username, a password, o nome e o email do utilizador como parametro
     * @param strUsername
     * @param strPassword
     * @param strNome
     * @param strEmail
     */
    public void setDados(String strUsername, String strPassword, String strNome, String strEmail) {

        m_utilizador.setUsername(strUsername);
        m_utilizador.setPassword(strPassword);
        m_utilizador.setNome(strNome);
        m_utilizador.setEmail(strEmail);

        m_empresa.getRegistaUtilizadores().registaUtilizador(m_utilizador);

    }

}
