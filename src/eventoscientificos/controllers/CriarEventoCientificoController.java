package eventoscientificos.controllers;

import eventoscientificos.eventstates.EventoTerminadoState;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.ListaOrganizadores;
import eventoscientificos.model.Utilizador;
import eventoscientificos.utils.Data;
import static java.awt.PageAttributes.MediaType.E;
import java.security.KeyStore.Entry;
import java.util.Map;

/**
 *
 * @author Nuno Silva
 */
public class CriarEventoCientificoController {

    /**
     * variaveis de instancia
     */

    private Empresa m_empresa;
    /**
     * Evento
     */
    private Evento m_evento;
    private String id;

    /**
     *construtor que recebe uma empresa como parametro
     * @param empresa
     */
    public CriarEventoCientificoController(Empresa empresa) {
        this.m_empresa = empresa;
    }

    /**
     *metodo que cria novo evento
     */
    public void novoEvento() {
        this.m_evento = m_empresa.getRegistaEventos().novoEvento();
    }

    /**
     *metodo que acede ao evento
     * @return evento
     */
    public Evento getEvento() {
        return this.m_evento;
    }

    public Empresa getEmpresa() {
        return m_empresa;
    }

    /**
     * retorna informacoes do evento
     *
     * @return
     */
    public String getEventoToString() {
        return m_evento.toString();
    }

    /**
     * Retorna informacoes do evento
     *
     * @return
     */
    public String getEventoString() {
        return m_evento.eventoString();
    }

    /**
     *metodo que modifica o titulo recebendo o titulo como parametro
     * @param strTitulo
     */
    public void setTitulo(String strTitulo) {
        m_evento.setTitulo(strTitulo);
    }

    /**
     *metodo que modifica a descricao recebendo a descricao como parametro
     * @param strDescricao
     */
    public void setDescricao(String strDescricao) {
        m_evento.setDescricao(strDescricao);
    }

    /**
     *metodo que modifica o local recebendo o local como parametro
     * @param strLocal
     */
    public void setLocal(String strLocal) {
        m_evento.setLocal(strLocal);
    }

    /**
     *metodo que modifica data de inicio recebendo a data de inicio como parametro
     * @param strDataInicio
     */
    public void setDataInicio(Data strDataInicio) {
        m_evento.setDataInicio(strDataInicio);
    }

    /**
     *metodo que modifica data fim recebendo a data como parametro
     * @param strDataFim
     */
    public void setDataFim(Data strDataFim) {
        m_evento.setDataFim(strDataFim);
    }

    /**
     *metodo que modifica data limite submissao recebendo a como parametro
     * @param dataLimiteSubmissao
     */
    public void setDataLimiteSubmissao(Data dataLimiteSubmissao) {
        m_evento.setDataLimiteSubmissao(dataLimiteSubmissao);
    }

    /**
     *metodo que modifica evento recebendo um evento como parametro
     * @param e
     */
    public void setEvento(Evento e) {
        this.m_evento = e;
    }

    /**
     *metodo que adiciona organizador recebendo o seu id como parametro
     * @param strId
     * @return organizador 
     */
    public boolean addOrganizador(String strId) {
        Utilizador u = m_empresa.getRegistaUtilizadores().getUtilizador(strId);

        if (u != null) {
            return m_evento.getListaOrganizadores().addOrganizador(u);
        } else {
            return false;
        }
    }

    /**
     *metodo que regista evento
     * @return evento
     */
    public boolean registaEvento() {
        return m_empresa.getRegistaEventos().registaEvento(m_evento);
    }

    /**
     *metodo que modifica website recebendo a webs como parametro
     * @param webs
     */
    public void setWebsite(String webs) {
        this.m_evento.setWebsite(webs);
    }

    /**
     *metodo que acede a lista de organizadores
     * @return lista de organizadores
     */
    public ListaOrganizadores getListaOrganizadores() {
        return this.m_evento.getListaOrganizadores();
    }

    /**
     *metodo que modifica evento criado por csv
     */
    public void setEventoCriadoPorCSV() {
        this.m_evento.setEventoCriadoPorCSV();
    }

    /**
     *metodo que modifica evento criado por xml
     */
    public void setEventoCriadoPorXML() {
        this.m_evento.setEventoCriadoPorXML();
    }

    /**
     * o metodo que retorna a data inicio do evento
     *
     * @return
     */

    /**
     *metodo que acede a data de inicio
     * @return data de inicio
     */
    public Data getDataInicio() {
        return this.m_evento.getDataInicio();
    }

    /**
     *metodo que acede ao id
     * @return id
     */
    public String getID() {
       
        
        return this.id;

     }  
     public String getIDEvento() {
        m_empresa.getRegistaEventos().getMapaEventos();
        for (Map.Entry<String, Evento> e : m_empresa.getRegistaEventos().getMapaEventos().entrySet()) {
            String key = e.getKey();
            Object value = e.getValue();
            if (value==this.m_evento){
                return key;
            }
        }
        
        return null;

     }   
    
    /**
     * o metodo que retorna o titulo
     * @return 
     */

    /**
     *metodo que acede ao titulo
     * @return titulo
     */
    public String getTitulo() {
        return this.m_evento.getTitulo();
    }

    /**
     *metodo que acede a descricao
     * @return descricao
     */
    public String getDescricao() {
        return this.m_evento.getDescricao();
    }

    /**
     *metodo que acede a webSite
     * @return webSite
     */
    public String getWebsite() {
        return this.m_evento.getWebsite();
    }

    /**
     *metodo que acede a string local
     * @return local
     */
    public String getLocalString() {
        return this.m_evento.getLocal();
    }

    /**
     *metodo que acede a data fim
     * @return data fim
     */
    public Data getDataFim() {
        return this.m_evento.getDataFim();
    }

    /**
     *metodo que acede a data limite submissao
     * @return data limite de submissao
     */
    public Data getDataLimSub() {
        return this.m_evento.getDataLimSub();
    }

    /**
     *metodo que acede a data limite de submissao final
     * @return data limite de submissao final
     */
    public Data getDataLimSubFin() {
        return this.m_evento.getDataLimSubFin();
    }

    /**
     *metodo que acede a data limite de revisao
     * @return data limite de revisao
     */
    public Data getDataLimRev() {
        return this.m_evento.getDataLimRev();
    }

    /**
     *metodo que acede a data limite de registo de autores
     * @return data limite de registo de autores
     */
    public Data getDataLimRegAut() {
        return this.m_evento.getDataLimRegAut();
    }

    /**
     *metodo que modifica id recebendo o id como parametro
     * @param id
     */
    public void setId(String id) {
        this.id=id;
    }

    /**
     *metodo que modifica data limite de submissao final recebendo a como parametro
     * @param data
     */
    public void setDataLimiteDeSubmissaoFinal(Data data) {
        this.m_evento.setDataLimiteDeSubmissaoFinal(data);
    }

    /**
     *metodo que modifica data limite de revisao recebendo a como parametro
     * @param data
     */
    public void setDataLimiteDeRevisao(Data data) {
        this.m_evento.setDataLimiteDeRevisao(data);
    }

    /**
     *metodo que modifica data limite de registo de autores recebendo a como parametro
     * @param data
     */
    public void setDataLimiteDeRegistoDeAutores(Data data) {
        this.m_evento.setDataLimiteDeRegistoDeAutores(data);
    }

    /**
     *metodo que modifica evento criado
     */
    public void setEventoCriado() {
        this.m_evento.setEventoCriado();
    }

    /**
     *metodo que modifica evento registado
     */
    public void setEventoRegistado() {
        this.m_evento.setEventoRegistado();
    }

    /**
     *metodo que adiciona ao mapa de eventos
     * @param text
     */
    public void addToMap(String text) {
        this.m_empresa.getRegistaEventos().getMapaEventos().put(text, this.m_evento);
    }

    public void setEventoTerminado() {
        this.m_evento.setState(new EventoTerminadoState(this.m_evento));
    }
    
    
}
