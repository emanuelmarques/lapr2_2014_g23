/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.exceptions.ImpossivelDistribuirException;
import eventoscientificos.model.Artigo;
import eventoscientificos.model.Distribuicao;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.ListaMecanismosDistribuicao;
import eventoscientificos.model.MecanismoDistribuicao;
import eventoscientificos.model.ProcessoDistribuicao;
import eventoscientificos.model.Submissao;
import eventoscientificos.submissaostates.SubmissaoDistribuidaState;
import eventoscientificos.utils.Data;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author vitor_000
 */
public class DistribuirRevisoesArtigoController implements Serializable {

    /**
     * variaveis de instancia
     */
    private Empresa empresa;
    private Evento evento;
    private ProcessoDistribuicao processoDistribuicao;

    /**
     * construtor que recebe uma empresa como parametro
     *
     * @param empresa
     */
    public DistribuirRevisoesArtigoController(Empresa empresa) {
        this.empresa = empresa;
    }

    /**
     * metodo que inicia uma nova distribuicao recebendo o id do revisor como
     * parametro
     *
     * @param id
     * @return lista de eventos do revisor
     */
    public List<Evento> novaDistribuicao(String id) {

        return this.empresa.getRegistaEventos().getListaEventosOrganizador(id);
    }

    /**
     * metodo que modifica evento recebendo um evento como parametro
     *
     * @param e
     */
    public void setEvento(Evento e) {
        this.evento = e;
    }

    /**
     * metodo que acede a lista de mecanismos
     *
     * @return lista de mecanismos de distribuicao
     */
    public ListaMecanismosDistribuicao getListaMecanismos() {
        return this.empresa.getListaMecanismosDistribuicao();
    }

    /**
     * metodo que modifica o mecanismo de distribuicao recebendo o como
     * parametro
     *
     * @param mecanismoDistr
     * @return distribuicao
     * @throws ImpossivelDistribuirException
     */
    public List<Distribuicao> setMecanismoDistr(MecanismoDistribuicao mecanismoDistr) throws ImpossivelDistribuirException {
        this.processoDistribuicao = this.getEvento().novoProcessoDistribuicao(mecanismoDistr);

        return this.getProcessoDistribuicao().distribui();
    }

    /**
     * metodo que regista distribuicao
     */
    public void registaDistribuicao() {
        this.getEvento().setProcessoDistribuicao(this.getProcessoDistribuicao());
        List<Submissao> lsEvento = this.getEvento().getSubmissoes().getListaSubmissoes();
        for (Distribuicao d : this.getProcessoDistribuicao().getListaDistribuicoes().getListaDitr()) {
            Artigo a = d.getArtigo();
            for (Submissao s : lsEvento) {
                if (s.getArtigo() == a) {

                    s.setSubmissaoDistribuida();
                    

                }
            }
            
        }
       boolean flag=true;
        for (Submissao submissao : lsEvento) {
            if (!(submissao.getEstadoSubmissao() instanceof SubmissaoDistribuidaState)){
                flag = false;
            }
            
        }
        if (flag==true){
            this.evento.setEventoDistribuido();
        }
        
    }

    /**
     * @return the processoDistribuicao
     */
    public ProcessoDistribuicao getProcessoDistribuicao() {
        return processoDistribuicao;
    }

    /**
     * @return the evento
     */
    public Evento getEvento() {
        return evento;
    }
}
