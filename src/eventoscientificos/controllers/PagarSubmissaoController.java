/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Autor;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import eventoscientificos.utils.Data;
import eventoscientificos.utils.Utils;
import java.util.ArrayList;

/**
 *
 * @author vitor_000
 */
public class PagarSubmissaoController {

    private Empresa empresa;
    private Evento evento;
    private Submissao submissao;

    public PagarSubmissaoController(Empresa empresa) {
        this.empresa = empresa;
    }

    /**
     * @return the empresa
     */
    public Empresa getEmpresa() {
        return empresa;
    }

    /**
     * @return the evento
     */
    public Evento getEvento() {
        return evento;
    }

    /**
     * @return the submissao
     */
    public Submissao getSubmissao() {
        return submissao;
    }

    /**
     * @param evento the evento to set
     */
    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    /**
     * @param empresa the empresa to set
     */
    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    /**
     * @param submissao the submissao to set
     */
    public void setSubmissao(Submissao submissao) {
        this.submissao = submissao;
    }

    public void setSubmissaoPaga() {
        this.submissao.setPagamentoEfetuado();
        this.submissao.setSubmissaoPaga();
    }

    public float getValorAutorizacao() {
        return this.evento.getFormulaDePagamento().resultado();
    }

    public String getDataLimiteRegistoAutoresYYYYMMDD() {
        return this.evento.getDataLimRegAut().toAnoMesDiaStringHifens();
    }

    public void setQntFullPapers(int qnt) {
        this.evento.getFormulaDePagamento().setQntFullPapers(qnt);
    }

    public void setQntShortPapers(int qnt) {
        this.evento.getFormulaDePagamento().setQntShortPapers(qnt);
    }

    public void setQntPosterPapers(int qnt) {
        this.evento.getFormulaDePagamento().setQntPosterPapers(qnt);
    }

    public void setEventoCameraReady() {
        this.evento.setEventoCameraReady();
    }

    public void escreverPagamento(Autor m_autorCorrespondente, ArrayList<Artigo> artigos, Data dataAtual) {
        Utils.escreverPagamento(m_autorCorrespondente, artigos, dataAtual);
    }
}
