/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Autor;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Topico;
import eventoscientificos.submissaostates.SubmissaoRejeitadaState;
import java.util.List;

/**
 *
 * @author Paulo Silva
 */
public class SubmeterArtigoController {

    /**
     * variaveis de instancia
     */
    private Empresa m_empresa;
    private Evento m_evento;
    private Submissao m_submissao;
    private Artigo m_artigo;

    /**
     * construtor que recebe uma empresa como parametri
     *
     * @param empresa
     */
    public SubmeterArtigoController(Empresa empresa) {
        m_empresa = empresa;

    }

    /**
     * metodo que inicia submissao
     *
     * @return lista de eventos que podem submeter
     */
    public List<Evento> iniciarSubmissao() {
        return this.m_empresa.getRegistaEventos().getListaEventosPodeSubmeter();
    }

    /**
     * metodo que seleciona evento recebendo o como parametro
     *
     * @param e
     */
    public void selectEvento(Evento e) {
        m_evento = e;
        this.m_submissao = this.m_evento.getSubmissoes().novaSubmissao(m_evento);

    }

    /**
     * metodo que acede a lista de autores
     *
     * @return lista de autores
     */
    public List<Autor> getListaAutores() {
        return this.m_artigo.getListaAutores().getListaAutores();
    }

    /**
     * metodo que acede a lista de topicos de evento
     *
     * @return lista de topicos
     */
    public List<Topico> getTopicosEvento() {
        if (m_evento != null) {
            return m_evento.getListaTopicos().getListaTopicos();
        } else {
            return null;
        }
    }

    /**
     * metodo que cria novo artigo recebendo o seu titulo e resumo como
     * parametro
     *
     * @param strTitulo
     * @param strResumo
     */
    public void novoArtigo(String strTitulo, String strResumo) {
        this.m_artigo = this.m_submissao.novoArtigo();
        this.m_artigo.setTitulo(strTitulo);
        this.m_artigo.setResumo(strResumo);
    }

    /**
     * metodo que cria novo autor recebendo o seu nome e afiliacao como
     * parametro
     *
     * @param strNome
     * @param strAfiliacao
     * @return novo autor
     */
    public Autor novoAutor(String strNome, String strAfiliacao) {
        return this.m_artigo.getListaAutores().novoAutor(strNome, strAfiliacao);
    }

    /**
     * metodo que cria novo autor recebendo o seu nome, afiliacao e email como
     * parametro
     *
     * @param strNome
     * @param strAfiliacao
     * @param strEmail
     * @return novo autor
     */
    public Autor novoAutor(String strNome, String strAfiliacao, String strEmail) {
        return this.m_artigo.getListaAutores().novoAutor(strNome, strAfiliacao, strEmail, this.m_empresa.getRegistaUtilizadores().getUtilizadorEmail(strEmail));
    }

    /**
     * metodo que adiciona autor recebendo o como paramtro
     *
     * @param autor
     * @return autor
     */
    public boolean addAutor(Autor autor) {
        return this.m_artigo.getListaAutores().add(autor);
    }

    /**
     * metodo que acede aos possiveis autores correspondentes
     *
     * @return possiveis autores correspondentes
     */
    public List<Autor> getPossiveisAutoresCorrespondentes() {
        return this.m_artigo.getListaAutores().getPossiveisAutoresCorrespondentes();
    }

    /**
     * metodo que modifica autor correspondente recebendo o como parametro
     *
     * @param autor
     */
    public void setCorrespondente(Autor autor) {
        this.m_artigo.setAutorCorrespondente(autor);
    }

    /**
     * metodo que modifica ficheiro recebendo o como parametro
     *
     * @param strFicheiro
     */
    public void setFicheiro(String strFicheiro) {
        this.m_artigo.setFicheiro(strFicheiro);
    }

    /**
     * metodo que acede a info resumo
     *
     * @return info do artigo
     */
    public String getInfoResumo() {
        return this.m_submissao.getInfo() + this.m_artigo.getInfo();
    }

    /**
     * metodo que regista submissao
     *
     * @return submissao
     */
    public boolean registarSubmissao() {
        this.m_submissao.setArtigo(m_artigo);
        return this.m_evento.getSubmissoes().addSubmissao(m_submissao);
    }

    /**
     * metodo que modifica o tipo short
     */
    public void setShortType() {
        m_artigo.setTipoArtigo("Short Paper");
    }

    /**
     * metodo que modifica o tipo full
     */
    public void setFullType() {
        m_artigo.setTipoArtigo("Full Paper");
    }

    /**
     * metodo que modifica tipo poster
     */
    public void setPosterType() {
        m_artigo.setTipoArtigo("Poster Paper");
    }

    // adicionado na iteração 2
    /**
     * metodo que modifica a lista de topicos de artigo recebendo uma lista de
     * topicos de artigo como parametro
     *
     * @param listaTopicosArtigo
     */
    public void setListaTopicosArtigo(List<Topico> listaTopicosArtigo) {
        m_artigo.setListaTopicos(listaTopicosArtigo);
    }

    /**
     * metodo que acede a empresa
     *
     * @return
     */
    public Empresa getEmpresa() {
        return m_empresa;
    }

    /**
     * metodo que acede ao artigo
     *
     * @return
     */
    public Artigo getArtigo() {
        return m_artigo;
    }

    /**
     * metodo que acede a submissao
     *
     * @return
     */
    public Submissao getM_submissao() {
        return m_submissao;
    }

    /**
     * metodo que acede ao evento
     *
     * @return
     */
    public Evento getEvento() {
        return m_evento;
    }

    public void setArtigo(Artigo m_artigo) {
        this.m_artigo = m_artigo;
    }

    public void setSubmissaoCriada() {
        this.m_submissao.setSubmissaoCriada();
    }

    public void setSubmissaoSubmetidaParaRevisao() {
        this.m_submissao.setSubmissaoSubmetida();
    }

    public void criarSubmissaoRejeitada() {
        this.m_submissao.setState(new SubmissaoRejeitadaState(this.m_submissao));
    }

}
