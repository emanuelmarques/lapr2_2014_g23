/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.eventstates.EventoDecididoState;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import eventoscientificos.submissaostates.SubmissaoAceiteState;
import eventoscientificos.submissaostates.SubmissaoRejeitadaState;
import eventoscientificos.submissaostates.SubmissaoRevistaState;

/**
 *
 * @author emanuelmarques
 */
public class DecidirSobreArtigosController {

    /**
     * variaveis de instancia
     */
    private Submissao submissao;
    private Evento evento;

    /**
     * construtor que recebedo uma submissao e um evento como parametro
     *
     * @param sub
     * @param evento
     */
    public DecidirSobreArtigosController(Submissao sub, Evento evento) {
        this.submissao = sub;
        this.evento = evento;
    }

    /**
     * metodo que aceita submissao
     */
    public void aceitarSubmissao() {
        this.submissao.setSubmissaoReijeitada();
        this.evento.setEventoDecidido();

    }

    /**
     * metodo que rejeita submissao
     */
    public void rejeitarSubmissao() {
        this.submissao.setSubmissaoReijeitada();
        this.evento.setEventoDecidido();

    }
    public void decidirSubmissao(){
        this.submissao.setDecisaoAceite();
    }

}
