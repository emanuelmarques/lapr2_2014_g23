/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.submissaostates;

import eventoscientificos.model.Submissao;
import java.io.Serializable;

/**
 *
 * @author Vitor Moreira
 */
public class SubmissaoAceiteState implements SubmissaoState, Serializable{
    
    /**
     * variavel de instancia
     */
    private Submissao submissao;

    /**
     *construtor que recebe uma submissao como parametro
     * @param submissao
     */
    public SubmissaoAceiteState(Submissao submissao) {
        this.submissao = submissao;
    }

    /**
     *metodo que valida
     * @return
     */
    @Override
    public boolean valida() {
        return this.submissao.getEstadoPagamento();
    }

    /**
     *metodo que modifica submissao criada
     */
    @Override
    public void setSubmissaoCriada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica artigo para revisao submetido
     */
    @Override
    public void setArtigoParaRevisaoSubmetido() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica submissao distribuida
     */
    @Override
    public void setSubmissaoDistribuida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica submissao revista
     */
    @Override
    public void setSubmissaoRevista() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica submissao rejeitada
     */
    @Override
    public void setSubmissaoRejeitada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica submissao aceite
     */
    @Override
    public void setSubmissaoAceite() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setSubmissaoPaga() {
        if (this.valida()) {
            this.submissao.setState(new SubmissaoPagaState(this.submissao));
        }
    }
    
    
}
