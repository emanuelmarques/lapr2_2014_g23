/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.submissaostates;

import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import java.io.Serializable;

/**
 *
 * @author Vitor Moreira
 */
public class SubmissaoCriadaState implements SubmissaoState, Serializable{
    
    /**
     * variavel de instancia
     */
    private Submissao submissao;
    private Evento evento;

    /**
     *construtor que recebe uma submissao como parametro
     * @param submissao
     * @param evento
     */
    public SubmissaoCriadaState(Submissao submissao, Evento evento) {
        this.submissao = submissao;
        this.evento = evento;
    }

    /**
     *metodo que valida
     * @return true se passa validacao, false se nao.
     */
    @Override
    public boolean valida() {
        return !this.submissao.getArtigo().getTitulo().equals("")
                && !this.submissao.getArtigo().getListaAutores().getListaAutores().isEmpty();

   
    }

    /**
     *metodo que modifica submissao criada
     */
    @Override
    public void setSubmissaoCriada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica artigo para revisao submetido
     */
    @Override
    public void setArtigoParaRevisaoSubmetido() {
        if (this.valida()) {
            this.submissao.setState(new ArtigoParaRevisaoSubmetidoState(this.submissao, this.evento));
        }
    }

    /**
     *metodo que modifica submissao distribuida
     */
    @Override
    public void setSubmissaoDistribuida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica submissao revista
     */
    @Override
    public void setSubmissaoRevista() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica submissao rejeitada
     */
    @Override
    public void setSubmissaoRejeitada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica submissao aceite
     */
    @Override
    public void setSubmissaoAceite() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setSubmissaoPaga() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
