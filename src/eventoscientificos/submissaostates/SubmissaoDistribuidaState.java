/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.submissaostates;

import eventoscientificos.model.Distribuicao;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import java.io.Serializable;

/**
 *
 * @author Vitor Moreira
 */
public class SubmissaoDistribuidaState implements SubmissaoState, Serializable {

    /**
     * variavel de instancia
     */
    private Submissao submissao;
    private Evento evento;

    /**
     * construtor que recebe uma submissao como parametro
     *
     * @param submissao
     * @param evento
     */
    public SubmissaoDistribuidaState(Submissao submissao, Evento evento) {
        this.submissao = submissao;
        this.evento = evento;
    }

    /**
     * metodo que valida
     *
     * @return
     */
    @Override
    public boolean valida() {
        for (Distribuicao d : this.evento.getProcessoDistribuicao().getListaDistribuicoes().getListaDitr()) {
            if (this.submissao.getArtigo() == d.getArtigo()) {
                return d.getRevisores().size() == this.submissao.getListaRevisoes().getListaRevisoes().size();
            }
        }
        return false;
    }

    /**
     * metodo que modifica submissao criada
     */
    @Override
    public void setSubmissaoCriada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * metodo que artigo para revisao submetido
     */
    @Override
    public void setArtigoParaRevisaoSubmetido() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * metodo que modifica submissao distribuida
     */
    @Override
    public void setSubmissaoDistribuida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * metodo que modifica submissao revista
     */
    @Override
    public void setSubmissaoRevista() {
        if (this.valida()) {
            this.submissao.setState(new SubmissaoRevistaState(this.submissao));
        }
    }

    /**
     * metodo que modifica submissao rejeitada
     */
    @Override
    public void setSubmissaoRejeitada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * metodo que modifica submissao aceite
     */
    @Override
    public void setSubmissaoAceite() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setSubmissaoPaga() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
