/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.submissaostates;

/**
 *
 * @author Vitor Moreira
 */
public interface SubmissaoState {

    /**
     *metodo abstrato que valida
     * @return
     */
    public abstract boolean valida();

    /**
     *metodo abstrato que modifica submissao criada
     */
    public abstract void setSubmissaoCriada();

    /**
     *metodo abstrato que modifica artigo para revisao submetido
     */
    public abstract void setArtigoParaRevisaoSubmetido();

    /**
     *metodo abstrato que modifica submissao distribuida
     */
    public abstract void setSubmissaoDistribuida();

    /**
     *metodo abstrato que modifica submissao revista
     */
    public abstract void setSubmissaoRevista();

    /**
     *metodo abstrato que modifica submissao rejeitada
     */
    public abstract void setSubmissaoRejeitada();

    /**
     *metodo abstrato que modifica submissao aceite
     */
    public abstract void setSubmissaoAceite();
    
    public abstract void setSubmissaoPaga();
}
