/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.utils;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisao;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Submissao;
import java.util.List;

/**
 *
 * @author emanuelmarques
 */
public class Estatistica {

        public static float taxaAceitacao(Evento evento) {
        List<Revisao> revisoes = evento.getRevisoesEvento();
        int taxa = 0;
        for (Revisao revisao : revisoes) {
            if (revisao.getRecomendacao().equalsIgnoreCase("aceite") || revisao.getRecomendacao().equalsIgnoreCase("accept")) {
                taxa++;

            }

        }
        return taxa * 100 / revisoes.size();

    }

    public static int[] calcMediasEvento(Evento evento) {

        List<Revisao> revisoes = evento.getRevisoesEvento();
        int[] medias = new int[4];
        int confianca = 0;
        int adequacao = 0;
        int originalidade = 0;
        int qualidade = 0;

        for (Revisao revisao : revisoes) {

            confianca += revisao.getConfianca();
            adequacao += revisao.getAdequacao();
            originalidade += revisao.getOriginalidade();
            qualidade += revisao.getQualidade();
        }

        medias[0] = confianca / revisoes.size();

        medias[1] = adequacao / revisoes.size();

        medias[2] = originalidade / revisoes.size();

        medias[3] = qualidade / revisoes.size();

        return medias;

    }

    public static int[] calcMediasRevisor(Empresa empresa, Revisor revisor) {

        List<Revisao> revisoes = empresa.getRevisoesDoRevisor(revisor);

        int[] medias = new int[4];
        int confianca = 0;
        for (Revisao revisao : revisoes) {

            confianca += revisao.getConfianca();
        }

        medias[0] = confianca / revisoes.size();

        int adequacao = 0;
        for (Revisao revisao : revisoes) {

            adequacao += revisao.getAdequacao();
        }
        medias[1] = adequacao / revisoes.size();

        int originalidade = 0;
        for (Revisao revisao : revisoes) {

            originalidade += revisao.getOriginalidade();
        }
        medias[2] = originalidade / revisoes.size();

        int qualidade = 0;
        for (Revisao revisao : revisoes) {

            qualidade += revisao.getQualidade();
        }
        medias[3] = qualidade / revisoes.size();

        return medias;

    }

    public static float calcDesvio(Empresa empresa, Revisor revisor) {
        List<Evento> eventosRevisor = empresa.getRegistaEventos().getListaEventosRevisorEmail(revisor.getUtilizador().getEmail());
        List<Revisao> revisoesRevisor = empresa.getRevisoesDoRevisor(revisor);
        int soma = 0;
        int totalRevisoes = 0;
        for (Evento e : eventosRevisor) {
            List<Submissao> sub = e.getSubmissoes().getListaSubmissoes();
            for (Submissao submissao : sub) {
                List<Revisao> rev = submissao.getListaRevisoes().getListaRevisoes();
                for (Revisao revisao : rev) {
                    soma += revisao.getMediClassificaçoes();
                    totalRevisoes++;
                }

            }

        }
        float desvioGeral = soma / totalRevisoes;
        int diferença = 0;

        for (Revisao revisao : revisoesRevisor) {
            diferença += Math.abs(revisao.getMediClassificaçoes() - desvioGeral);

        }
        float desvio = (float) diferença / revisoesRevisor.size();
        return desvio;

    }

    public static int calcMediaClassificacoes(int a, int b, int c, int d) {
        return (a + b + c + d) / 4;
    }

    public static double calcVariancia(Empresa empresa, Revisor revisor) {
        List<Revisao> revisoesRevisor = empresa.getRevisoesDoRevisor(revisor);

        float desvioRevisor = calcDesvio(empresa, revisor);

        double somaDiferencas = 0;
        for (Revisao revisao : revisoesRevisor) {
            Submissao sub = submissaoDeRevisao(empresa, revisao);
            double desvio = Math.abs(revisao.getMediClassificaçoes() - sub.getMediaRevisoes());

            somaDiferencas = somaDiferencas + Math.pow((desvio - desvioRevisor), 2);

        }

        double variancia1 = somaDiferencas / (revisoesRevisor.size() - 1);

        double variancia = (desvioRevisor - 1) / Math.sqrt(variancia1 / revisoesRevisor.size());

        return variancia;

    }

    public static Submissao submissaoDeRevisao(Empresa empresa, Revisao revisao) {
        for (Evento e : empresa.getRegistaEventos().getLe()) {
            for (Submissao sub : e.getSubmissoes().getListaSubmissoes()) {
                for (Revisao rev : sub.getListaRevisoes().getListaRevisoes()) {
                    if (revisao == rev) {
                        return sub;
                    }

                }

            }
        }
        return null;
    }

    public static boolean testeHipoteses(float alfa, Empresa empresa, Revisor revisor) {
        float zc;
        if (alfa == 0.01) {
            zc = 2.33f;
        } else if (alfa == 0.02) {
            zc = 2.05f;
        } else if (alfa == 0.04) {
            zc = 1.75f;
        } else if (alfa == 0.05) {
            zc = 1.645f;
        } else if (alfa == 0.1) {
            zc = 1.28f;
        } else {
            zc = 0.84f;
        }

        float desvio = calcDesvio(empresa, revisor);
        double variancia = calcVariancia(empresa, revisor);
        double z0;

        z0 = ((desvio - 1) / (variancia / empresa.getRevisoesDoRevisor(revisor).size()));

        System.out.println(z0);
        if (z0 > zc) {
            return true;
        } else {
            return false;
        }

    }

}
