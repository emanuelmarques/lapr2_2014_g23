/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.utils;

import eventoscientificos.model.Revisao;
import java.util.List;

/**
 *
 * @author emanuelmarques
 */
public interface LeitorFicheiroRevisoes {

    /**
     *metodo que le ficheiro
     * @return
     */
    public List<Revisao> lerFicheiro();
}
