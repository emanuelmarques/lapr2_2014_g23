/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.utils;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Autor;
import eventoscientificos.model.Distribuicao;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author Paulo Silva
 */
public class Utils {

    /**
     *
     * @param stEmail
     * @return
     *
     * email tem de ser sempre em minúsculas, não acentuado, sem espaços e no
     * meio conter @
     *
     * email validation by mkyong.com:
     * http://www.mkyong.com/regular-expressions/how-to-validate-email-address-with-regular-expression/
     */
    static public boolean validaEmail(String stEmail) {
        String EMAIL_PATTERN
                = "^[a-z0-9-\\+]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9]+)*(\\.[a-z]{2,})$";
        
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(stEmail);
        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Separa uma data em string do formato diaDaSemana, mes diaDoMes, ano,
     * criando e devolvendo uma instancia data
     *
     * @param dataString a data a ser separada
     * @return uma instancia data
     */
    public static Data separarData(String dataString) {
        String[] dataTemp = dataString.split(", ");
        String[] separarMesDoDia = dataTemp[1].split(" ");
        int index = Data.getNumMes(separarMesDoDia[0]);
        
        Data data = new Data(Integer.parseInt(dataTemp[2]), index, Integer.parseInt(separarMesDoDia[1]));
        return data;
    }

    /**
     * metodo que escreve ficheiro recebendo uma mensagem por parametro
     *
     * @param msg
     */
    public static void escreverErro(String msg) {
        File f = new File("log.txt");
        FileWriter fileWriter;
        if (f.exists() && !f.isDirectory()) {
            try {
                fileWriter = new FileWriter(f, true);
                
                BufferedWriter bufferFileWriter = new BufferedWriter(fileWriter);
                fileWriter.append(msg + "\n");
                
                bufferFileWriter.close();
            } catch (IOException ex1) {
                JOptionPane.showMessageDialog(null, msg + "\n", "Migraçao de Dados", JOptionPane.ERROR_MESSAGE);
            }
            
        } else {
            PrintWriter writer = null;
            try {
                writer = new PrintWriter("log.txt", "UTF-8");
                writer.println(msg + "\n");
                writer.close();
            } catch (Exception ex1) {
                JOptionPane.showMessageDialog(null, msg + "\n", "Migraçao de Dados", JOptionPane.ERROR_MESSAGE);
                
            } finally {
                writer.close();
            }
        }
    }

    /**
     * transforma uma lista de distribuiçoes numa matriz
     *
     * @param list lista de distribuiçoes
     * @return matriz com as distribuiçoes
     *
     */
    public static String[][] toTable(List<Distribuicao> list) {
        String[][] ret = new String[list.size()][2];
        
        for (int i = 0; i < list.size(); i++) {
            ret[i][0] = list.get(i).getArtigo().getTitulo();
            ret[i][1] = "";
            for (int j = 0; j < list.get(i).getRevisores().size(); j++) {
                ret[i][1] += list.get(i).getRevisores().get(j).getNome() + "; ";
            }
        }
        return ret;
        
    }
    
    public static boolean validaFormatoData(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            sdf.parse(date);
            return true;
        } catch (ParseException ex) {
            return false;
        }
    }
    
    public static void escreverPagamento(Autor autor, ArrayList<Artigo> listaArtigos, Data dataPagamento) {
        try {
            File f = new File("Pagamento_" + autor.getNome() + "_" + dataPagamento+".csv");
            FileWriter fileWriter;
            
            fileWriter = new FileWriter(f);
            BufferedWriter bufferFileWriter = new BufferedWriter(fileWriter);
            fileWriter.write("Autor;Titulo;Data de pagamento\n");
            for (Artigo artigo : listaArtigos) {
                fileWriter.append(autor.getNome() + ";"+artigo.getTitulo()+";"+dataPagamento+"\n");
                
            }
            
            bufferFileWriter.close();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Ocorreu um erro ao exportar o ficheiro de pagamento", "Pagamento", JOptionPane.ERROR_MESSAGE);
        }
        
    }
}
