package eventoscientificos.utils;

import eventoscientificos.model.Evento;
import eventoscientificos.model.Notificacao;
import eventoscientificos.model.Revisao;
import eventoscientificos.model.Submissao;
import eventoscientificos.submissaostates.SubmissaoAceiteState;
import eventoscientificos.submissaostates.SubmissaoRejeitadaState;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 *
 * @author Paulo Silva
 */
public class CriarNotificacao {

    /**
     * variaveis de instancia
     */
    private PrintWriter writer;
    private Evento evento;

    /**
     * construtor que recebe um evento como parametro
     *
     * @param evento
     */
    public CriarNotificacao(Evento evento) {
        this.evento = evento;

    }

    /**
     * metodo que notifica evento
     *
     * @throws FileNotFoundException
     */
    public void notificarEvento() throws FileNotFoundException {
        writer = new PrintWriter("Notificacoes.xml");
        writer.println("<notification_list>");

        for (Submissao sub : this.evento.getSubmissoes().getListaSubmissoes()) {
            if (sub.getEstadoSubmissao() instanceof SubmissaoAceiteState || sub.getEstadoSubmissao() instanceof SubmissaoRejeitadaState) {
                Notificacao n = new Notificacao();

                sub.registaNotificacao(n);
                criarFicheiro(sub);
            }
        }

        writer.println("<notification_list>");
        writer.close();

    }

    /**
     * metodo que cria ficheiro recebendo uma submissao como parametro
     *
     * @param s
     * @throws FileNotFoundException
     */
    private void criarFicheiro(Submissao s) throws FileNotFoundException {

        writer.println("\t<submission>");
        writer.println("\t\t<paper_title/>");
        writer.println("\t\t\t" + s.getArtigo().getTitulo());
        writer.println("\t\t<paper_type/>");
        writer.println("\t\t\t" + s.getArtigo().getTipoArtigo());
        writer.println("\t\t<author_name/>");
        writer.println("\t\t\t" + s.getArtigo().getM_autorCorrespondente().getNome());
        writer.println("\t\t<author_email/>");
        writer.println("\t\t\t" + s.getArtigo().getM_autorCorrespondente().getEMail());
        writer.println("\t\t<decision/>");
        if (s.getEstadoSubmissao() instanceof SubmissaoAceiteState) {
            writer.println("\t\t\tO seu artigo foi aceite.");
        } else {
            writer.println("\t\t\tO seu artigo foi rejeitado.");
        }
        writer.println("\t\t<comments>");

        for (Revisao revisao : s.getListaRevisoes().getListaRevisoes()) {
            writer.println("\t\t\t<review_number/>");
            writer.println("\t\t\t\t" + s.getListaRevisoes().getListaRevisoes().indexOf(revisao) + 1);
            writer.println("\t\t\t<reviewer_comments/>");
            writer.println("\t\t\t\t" + revisao.getTextoJustificativo());
        }

        writer.println("\t\t</comments>");

        writer.println("\t</submission>");

    }

}
