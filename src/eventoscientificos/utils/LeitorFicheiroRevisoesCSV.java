/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.utils;

import eventoscientificos.controllers.ReverArtigoController;
import eventoscientificos.eventstates.CPDefinidaState;
import eventoscientificos.eventstates.EventoRegistadoState;
import eventoscientificos.eventstates.TopicosCriadosState;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Revisao;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Utilizador;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author emanuelmarques
 */
public class LeitorFicheiroRevisoesCSV implements LeitorFicheiroRevisoes {

    /**
     * variaveis de instancia
     */
    private String path;

    private Empresa empresa;

    /**
     * construtor que recebe a path e a empresa como parametro
     *
     * @param path
     * @param empresa
     */
    public LeitorFicheiroRevisoesCSV(String path, Empresa empresa) {
        this.empresa = empresa;

        this.path = path;
    }

    /**
     * metodo que le ficheiro
     *
     * @return revisoes ou null
     */
    @Override
    public List<Revisao> lerFicheiro() {

        this.path = this.path.replace("\\", "/");
        try {
            BufferedReader reader = new BufferedReader(new FileReader(this.path));
            String aux = null;
            List<String> revisoes = new ArrayList<>();

            while ((aux = reader.readLine()) != null) {
                aux = aux.trim();
                if (!aux.equals("")) {
                    revisoes.add(aux);
                }
            }

            reader.close();
            return lerRevisoes(revisoes);
        } catch (IOException ex) {
            Utils.escreverErro("O ficheiro a importar nao existe");
            return null;
        }
    }

    /**
     * metodo que le revisoes recebendo uma lista de revisoes linha como
     * parametro
     *
     * @param revisoesLinha
     * @return revisoes ou null
     */
    private List<Revisao> lerRevisoes(List<String> revisoesLinha) {
        ReverArtigoController controllerRA = new ReverArtigoController(empresa);
        List<Revisao> revisoes = new ArrayList<>();
        ArrayList<String> erros = new ArrayList();
        try{
        for (int i = 1; i < revisoesLinha.size(); i++) {
            String aux = revisoesLinha.get(i);
            String[] revisao = aux.split(";");

            if (empresa.getRegistaEventos().getMapaEventos().containsKey(revisao[0])) {

                controllerRA.setEvento(empresa.getRegistaEventos().getMapaEventos().get(revisao[0]));
                if (controllerRA.getEvento().getState() instanceof TopicosCriadosState||controllerRA.getEvento().getState() instanceof CPDefinidaState) {
                    if (controllerRA.getEvento().getSubmissoes().getMapaDeSubmissoes().containsKey(revisao[1])) {

                        controllerRA.setArtigo(controllerRA.getEvento().getSubmissoes().getMapaDeSubmissoes().get(revisao[1]).getArtigo());
                        controllerRA.novaRevisao();
                        controllerRA.getRevisao().setConfianca(Integer.parseInt(revisao[3]));
                        controllerRA.getRevisao().setAdequacao(Integer.parseInt(revisao[4]));
                        controllerRA.getRevisao().setOriginalidade(Integer.parseInt(revisao[5]));
                        controllerRA.getRevisao().setQualidade(Integer.parseInt(revisao[6]));
                        controllerRA.getRevisao().setRecomendacao(revisao[7]);
                        Utilizador user;
                        Revisor r;

                        if (empresa.getRegistaUtilizadores().getUtilizadorEmail(revisao[2]) != null) {
                            user = empresa.getRegistaUtilizadores().getUtilizadorEmail(revisao[2]);
                            for (Revisor string : controllerRA.getEvento().getCP().getListaRevisores().getListaRevisores()) {
                                if (string.getUtilizador().equals(user)) {
                                    controllerRA.getRevisao().setRevisor(string);

                                }

                            }

                        } else {
                            user = new Utilizador();
                            user.setEmail(revisao[2]);
                            empresa.getRegistaUtilizadores().registaUtilizador(user);
                            r = new Revisor(user);
                            controllerRA.getEvento().getCP().getListaRevisores().getListaRevisores().add(r);
                            controllerRA.getRevisao().setRevisor(r);
                        }

                        controllerRA.registaRevisao(controllerRA.getRevisao());
                        controllerRA.criarSubmissaoRevista();

                        revisoes.add(controllerRA.getRevisao());
                    }

                } else {
                    erros.add("O evento da linha " + i + " nao é válido para a operaçao");
                    Utils.escreverErro("O evento da linha " + i + " nao foi aceite pelo administrador");
                }
           

            } else {
                erros.add("O evento da linha " + i + " nao existe\n");
                Utils.escreverErro("O evento da linha " + i + " nao existe\n");
            }
        }

        if (!erros.isEmpty()) {
            JList area = new JList(erros.toArray(new String[erros.size()]));

            JScrollPane scroll = new JScrollPane(area, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
            scroll.setMaximumSize(new Dimension(100, 5));

            JOptionPane.showMessageDialog(null, scroll, "Ocorreram alguns erros", JOptionPane.ERROR_MESSAGE);
        }
        controllerRA.getEvento().setCPDefinida(controllerRA.getEvento().getCP());
        controllerRA.getEvento().setEventoDistribuido();
        controllerRA.getEvento().setEventoRevisto();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "Ocorreu um erro. Ja importou os artigos?", "Ocorreram alguns erros", JOptionPane.ERROR_MESSAGE);
        }
        return revisoes;
    }
}
