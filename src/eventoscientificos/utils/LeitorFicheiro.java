/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.utils;

import eventoscientificos.model.Evento;
import java.io.FileNotFoundException;
import java.util.List;

/**
 *
 * @author vitor_000
 */
public interface LeitorFicheiro {
    
    /**
     *metodo abstrato que le um ficheiro
     * @return
     * @throws FileNotFoundException
     */
    public abstract List<Evento> lerFicheiro() throws FileNotFoundException;
}
