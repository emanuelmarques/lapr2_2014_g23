package eventoscientificos.utils;

import eventoscientificos.controllers.CriarEventoCientificoController;
import eventoscientificos.model.*;
import static eventoscientificos.utils.Utils.separarData;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 *
 * @author vitor_000
 */
public class LeitorFicheiroCSV implements LeitorFicheiro {

    /**
     * variaveis de instancia
     */
    private String path;
    private CriarEventoCientificoController eventController;
    private Empresa empresa;

    /**
     *construtor que recebe uma path, o eventController e uma empresa como parametros
     * @param path
     * @param eventController
     * @param empresa
     */
    public LeitorFicheiroCSV(String path, CriarEventoCientificoController eventController, Empresa empresa) {
        this.empresa = empresa;
        this.eventController = eventController;
        this.path = path;
    }

    /**
     *metodo que le ficheiro
     * @return null
     * @throws FileNotFoundException
     */
    @Override
    public List<Evento> lerFicheiro() throws FileNotFoundException {

        this.path = this.path.replace("\\", "/");
        BufferedReader reader = new BufferedReader(new FileReader(this.path));
        String aux = null;
        List<String> eventos = new ArrayList<>();
        try {
            while ((aux = reader.readLine()) != null) {
                aux = aux.trim();
                if (!aux.equals("")) {
                    eventos.add(aux);
                }
            }

            reader.close();
            return lerEventos(eventos);
        } catch (IOException ex) {
            Utils.escreverErro("O ficheiro a importar nao existe");
            return null;
        }

    }

    /**
     *metodo que le eventos recebendo uma lista de eventos como parametro
     * @param eventos
     * @return ler eventos por csv, ou por csvalt ou null
     */
    public List<Evento> lerEventos(List<String> eventos) {

        String[] aux = eventos.get(0).split(";");
        if (aux[2].equalsIgnoreCase("Host")) {
            return lerEventosPorCSV(eventos);
        } else if (aux[2].equalsIgnoreCase("ID")) {
            return lerEventosPorCSVAlt(eventos);
        }
        return null;
    }

    /**
     *metodo que le eventos por csv recebendo uma lista de eventos como parametro
     * @param eventos
     * @return lista de eventos
     */
    public List<Evento> lerEventosPorCSV(List<String> eventos) {
        List<Evento> le = new ArrayList<>();
        for (int i = 1; i < eventos.size(); i++) {

            String aux = eventos.get(i);
            String[] evento = aux.split(";");

            this.eventController.novoEvento();

            this.eventController.setEventoCriadoPorCSV();

            this.eventController.setTitulo(evento[1]);
            this.eventController.setLocal(evento[2] + ", " + evento[3] + ", " + evento[4]);

            Data dataIn = separarData(evento[5]);
            Data dataFim = new Data(dataIn);
            dataFim.incrementaDias(Integer.parseInt(evento[6]));
            this.eventController.setDataInicio(dataIn);
            this.eventController.setDataFim(dataFim);

            this.eventController.setWebsite(evento[7]);

            for (int j = 8; j < evento.length; j += 2) {
                Utilizador u = this.empresa.getRegistaUtilizadores().getUtilizadorEmail(evento[j + 1]);
                if (u != null) {
                    this.eventController.getListaOrganizadores().addOrganizador(u);
                } else {
                    Utilizador ut = new Utilizador();
                    ut.setNome(evento[j]);
                    ut.setEmail(evento[j + 1]);
                    this.eventController.getListaOrganizadores().addOrganizador(ut);
                }
            }
            le.add(eventController.getEvento());

        }
        return le;
    }

    /**
     * metodo que le eventos por csvalt recebendo uma lista de eventos como parametro
     * @param eventos
     * @return  lista de eventos
     */
    private List<Evento> lerEventosPorCSVAlt(List<String> eventos) {
        List<Evento> le = new ArrayList<>();
        for (int i = 1; i < eventos.size(); i++) {

            String[] evento = eventos.get(i).split(";");
            this.eventController.novoEvento();

            this.eventController.setEventoCriadoPorCSV();

            this.eventController.setTitulo(evento[1]);
            this.eventController.setId(evento[2]);
            this.eventController.setLocal(evento[3] + ", " + evento[4] + ", " + evento[5]);

            this.eventController.setDataLimiteSubmissao(separarData(evento[6]));
            this.eventController.setDataLimiteDeRevisao(separarData(evento[7]));
            this.eventController.setDataLimiteDeSubmissaoFinal(separarData(evento[8]));
            this.eventController.setDataLimiteDeRegistoDeAutores(separarData(evento[9]));

            Data dataIn = separarData(evento[10]);
            Data dataFim = new Data(dataIn);
            dataFim.incrementaDias(Integer.parseInt(evento[11]));

            this.eventController.setDataInicio(dataIn);
            this.eventController.setDataFim(dataFim);

            this.eventController.setWebsite(evento[12]);

            for (int j = 13; j < evento.length; j += 2) {
                Utilizador u = this.empresa.getRegistaUtilizadores().getUtilizadorEmail(evento[j + 1]);
                if (u != null) {
                    this.eventController.getListaOrganizadores().addOrganizador(u);
                } else {
                    Utilizador ut = new Utilizador();
                    ut.setNome(evento[j]);
                    ut.setEmail(evento[j + 1]);
                    this.eventController.getListaOrganizadores().addOrganizador(ut);
                }
            }
            le.add(eventController.getEvento());
            empresa.getRegistaEventos().getMapaEventos().put(eventController.getID(), eventController.getEvento());
        }
        return le;
    }
}
