package eventoscientificos.utils;

import eventoscientificos.controllers.CriarEventoCientificoController;
import eventoscientificos.model.*;
import static eventoscientificos.utils.Utils.separarData;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author vitor_000
 */
public class LeitorFicheiroXML implements LeitorFicheiro {

    /**
     * variaveis de instancia
     */
    private String path;
    private CriarEventoCientificoController eventController;
    private Empresa empresa;

    /**
     *construtor que recebe uma path, eventController e uma empresa como parametro
     * @param path
     * @param eventController
     * @param empresa
     */
    public LeitorFicheiroXML(String path, CriarEventoCientificoController eventController, Empresa empresa) {
        this.empresa = empresa;
        this.eventController = eventController;
        this.path = path;
    }

    /**
     *metodo que le ficheiro
     * @return lista de eventos ou null
     * @throws FileNotFoundException
     */
    @Override
    public List<Evento> lerFicheiro() throws FileNotFoundException {

        this.path = this.path.replace("\\", "/");
        List<Evento> le = new ArrayList<>();

        try {

            File xmlFile = new File(this.path);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);

            NodeList nList = doc.getElementsByTagName("event");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    this.eventController.novoEvento();
                    this.eventController.setEventoCriadoPorXML();

                    eventController.setTitulo(eElement.getElementsByTagName("name").item(0).getTextContent());
                    eventController.setLocal(eElement.getElementsByTagName("host").item(0).getTextContent()
                            + ", " + eElement.getElementsByTagName("city").item(0).getTextContent()
                            + ", " + eElement.getElementsByTagName("country").item(0).getTextContent());
                    eventController.setDataInicio(separarData(eElement.getElementsByTagName("begin_date").item(0).getTextContent()));
                    eventController.setDataFim(separarData(eElement.getElementsByTagName("end_date").item(0).getTextContent()));
                    eventController.setWebsite(eElement.getElementsByTagName("web_page").item(0).getTextContent());

                    NodeList orgList = eElement.getElementsByTagName("organizer");
                    for (int i = 0; i < orgList.getLength(); i++) {
                        Node orgNode = orgList.item(i);

                        if (orgNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element orgElement = (Element) orgNode;

                            Utilizador u = this.empresa.getRegistaUtilizadores().getUtilizadorEmail(orgElement.getElementsByTagName("email").item(0).getTextContent());
                            if (u != null) {
                                this.eventController.getListaOrganizadores().addOrganizador(u);
                            } else {
                                Utilizador ut = new Utilizador();
                                ut.setNome(orgElement.getElementsByTagName("org_name").item(0).getTextContent());
                                ut.setEmail(orgElement.getElementsByTagName("email").item(0).getTextContent());
                                this.eventController.getListaOrganizadores().addOrganizador(ut);
                            }
                        }

                    }

                    le.add(eventController.getEvento());
                }
            }
            return le;
        } catch (IOException e) {
            Utils.escreverErro("O ficheiro a importar nao existe");
            return null;
        } catch (ParserConfigurationException ex) {
            Utils.escreverErro("O ficheiro esta mal formatado");
            return null;
        } catch (SAXException ex2) {
            Utils.escreverErro("O ficheiro esta mal formatado");
            return null;

        }

    }
}
