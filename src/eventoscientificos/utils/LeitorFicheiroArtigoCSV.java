/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.utils;

import eventoscientificos.controllers.SubmeterArtigoController;
import eventoscientificos.model.Autor;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Utilizador;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author emanuelmarques
 */
public class LeitorFicheiroArtigoCSV implements LeitorFicheiroArtigo {

    /**
     * variaveis de instancia
     */
    private String path;

    private Empresa empresa;

    /**
     * construtor que recebe uma empresa e uma path como parametro
     *
     * @param path
     * @param empresa
     */
    public LeitorFicheiroArtigoCSV(String path, Empresa empresa) {
        this.empresa = empresa;

        this.path = path;
    }

    /**
     * metodo implementado a partir da interface LeitorFicheiroArtigo que le
     * ficheiro
     *
     * @return
     */
    @Override
    public List<Submissao> lerFicheiro() {

        this.path = this.path.replace("\\", "/");
        try {
            BufferedReader reader = new BufferedReader(new FileReader(this.path));
            String aux = null;
            List<String> artigos = new ArrayList<>();

            while ((aux = reader.readLine()) != null) {
                aux = aux.trim();
                if (!aux.equals("")) {
                    artigos.add(aux);
                }
            }

            reader.close();
            return lerArtigos(artigos);
        } catch (IOException ex) {
            Utils.escreverErro("O ficheiro a importar nao existe");
            return null;
        }
    }

    /**
     * metodo que le artigos recebendo uma lista de artigos linha como parametro
     *
     * @param artigosLinha
     * @return artigos
     */
    private List<Submissao> lerArtigos(List<String> artigosLinha) {
        List<Submissao> artigos = new ArrayList<>();
        for (int i = 1; i < artigosLinha.size(); i++) {
            String aux = artigosLinha.get(i);
            String[] artigo = aux.split(";");
            SubmeterArtigoController controllerSA = new SubmeterArtigoController(empresa);

            if (empresa.getRegistaEventos().getMapaEventos().containsKey(artigo[0])) {
                controllerSA.selectEvento(empresa.getRegistaEventos().getMapaEventos().get(artigo[0]));
                controllerSA.novoArtigo(artigo[3], "");

            }

            for (int j = 4; j < artigo.length; j += 3) {
                Autor autor = new Autor();
                autor.setNome(artigo[j]);
                autor.setAfiliacao(artigo[j + 1]);
                autor.setEMail(artigo[j + 2]);
                if (!(empresa.getRegistaUtilizadores().getUtilizadorEmail(artigo[j + 2]) == null)) {
                    autor.setUtilizador(empresa.getRegistaUtilizadores().getUtilizadorEmail(artigo[j + 2]));                   

                }else if(j==4){
                    Utilizador u = new Utilizador();
                    u.setNome(autor.getNome());
                    u.setEmail(autor.getEMail());
                    autor.setUtilizador(u);
                }
                controllerSA.addAutor(autor);
                if (j==4){
                    controllerSA.setCorrespondente(autor);
                }

            }
            if (artigo[2].equalsIgnoreCase("short paper")) {
                controllerSA.setShortType();
            } else if (artigo[2].equalsIgnoreCase("full paper")) {
                controllerSA.setFullType();
            } else if (artigo[2].equalsIgnoreCase("poster")) {
                controllerSA.setPosterType();
            }

            controllerSA.getM_submissao().setArtigo(controllerSA.getArtigo());
            controllerSA.getM_submissao().setSubmissaoCriada();
            controllerSA.getM_submissao().setSubmissaoSubmetida();
            controllerSA.getEvento().getSubmissoes().addSubmissao(controllerSA.getM_submissao());
            artigos.add(controllerSA.getM_submissao());
            controllerSA.getEvento().getSubmissoes().getMapaDeSubmissoes().put(artigo[1], controllerSA.getM_submissao());

        }

        return artigos;
    }

}
