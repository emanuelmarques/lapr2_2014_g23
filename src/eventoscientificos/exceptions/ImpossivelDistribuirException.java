package eventoscientificos.exceptions;

/**
 *
 * @author Paulo Silva
 */
public class ImpossivelDistribuirException extends Exception {

    /**
     * cria uma exceçao recebendo uma mensagem por parametro
     *
     * @param msg mensagem a escrever
     */
    public ImpossivelDistribuirException(String msg) {
        super(msg);
    }

    /**
     * cria uma exceçao por defeito
     */
    public ImpossivelDistribuirException() {
        super("Não cumpre requisitos mínimos para este mecanismo!");
    }

}
