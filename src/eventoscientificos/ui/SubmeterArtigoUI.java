/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.controllers.SubmeterArtigoController;
import eventoscientificos.utils.Data;
import eventoscientificos.utils.Utils;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;

public class SubmeterArtigoUI extends JDialog {

    private Empresa empresa;
    private SubmeterArtigoController controllerSA;
    private JTextField txtTitulo;
    private JTextArea txtResumo;
    private JComboBox combo, comboType;
    private JFileChooser chooser;

    /**
     *
     * @param pai
     * @param emp
     */
    public SubmeterArtigoUI(Frame pai, Empresa emp) {
        super(pai, "Submeter artigo", true);
        this.empresa = emp;
        controllerSA = new SubmeterArtigoController(this.empresa);

        JPanel painel = new JPanel(new GridLayout(2, 1));
        JPanel painel1 = new JPanel(new GridLayout(2, 1));
        JPanel painel2 = new JPanel(new GridLayout(1, 1));

        painel.add(criarPainelEscolhaEvento());
        painel.add(criarPainelTitulo());
        painel1.add(criarPainelResumo());
        painel1.add(criarPainelBrowse());

        painel2.add(criarPainelButoes());
        add(painel, BorderLayout.NORTH);
        add(painel1, BorderLayout.CENTER);
        add(painel2, BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(null);

        setResizable(false);
        setVisible(true);

    }

    private JPanel criarPainelEscolhaEvento() {
        JPanel p = new JPanel();

        ArrayList<String> titulos = new ArrayList();
        for (Evento evento : this.controllerSA.iniciarSubmissao()) {
            titulos.add(evento.getTitulo());

        }

        combo = new JComboBox(titulos.toArray(new String[titulos.size()]));
        p.add(combo);

        return p;

    }

    private JPanel criarPainelTitulo() {
        JPanel p = new JPanel();
        JLabel titulo = new JLabel("Título:");
        txtTitulo = new JTextField(10);
        p.add(titulo);
        p.add(txtTitulo);

        return p;

    }

    private JPanel criarPainelResumo() {
        JPanel p = new JPanel(new GridLayout(3, 1));
        JLabel resumo = new JLabel("Resumo:");
        txtResumo = new JTextArea(5, 20);
        JScrollPane scrollPane = new JScrollPane(txtResumo);

        p.add(resumo);
        p.add(scrollPane);
        JPanel p1 = new JPanel();
        String[] tipos = {"Full Paper", "Short Paper", "Poster"};
        comboType = new JComboBox(tipos);
        p1.add(comboType);
        p.add(p1);

        return p;
    }

    private JPanel criarPainelBrowse() {
        JPanel p = new JPanel();
        JLabel escFicheiro = new JLabel("Ficheiro:");
        final JTextField path = new JTextField(20);
        path.setEditable(false);
        JButton btnBrowse = new JButton("Procurar");
        btnBrowse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chooser = new JFileChooser();
                int retorno1 = chooser.showOpenDialog(null);
                if (retorno1 == JFileChooser.APPROVE_OPTION) {
                    path.setText(chooser.getSelectedFile().getPath());
                }
            }

        });

        p.add(escFicheiro);
        p.add(path);
        p.add(btnBrowse);
        return p;

    }

    private JPanel criarPainelButoes() {
        JPanel p4 = new JPanel();
        JButton ok = new JButton("OK");
        JButton cancelar = new JButton("Cancelar");

        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controllerSA.selectEvento(controllerSA.iniciarSubmissao().get(combo.getSelectedIndex()));
                controllerSA.setSubmissaoCriada();
                controllerSA.novoArtigo(txtTitulo.getText(), txtResumo.getText());
                if (comboType.getSelectedIndex() == 0) {
                    controllerSA.setFullType();
                } else if (comboType.getSelectedIndex() == 1) {
                    controllerSA.setShortType();
                } else if (comboType.getSelectedIndex() == 2) {
                    controllerSA.setPosterType();
                }

                controllerSA.setFicheiro(chooser.getSelectedFile().getPath());
                DialogCriarAutor l = new DialogCriarAutor(SubmeterArtigoUI.this, controllerSA);

                DialogCriarTopicosArtigo c = new DialogCriarTopicosArtigo(SubmeterArtigoUI.this, controllerSA);

                JTextArea confirmacao = new JTextArea(controllerSA.getInfoResumo());

                int i = JOptionPane.showConfirmDialog(SubmeterArtigoUI.this, confirmacao, "Submeter Artigo", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (i == 1) {
                    dispose();

                } else {
                    controllerSA.registarSubmissao();
                    if (Data.getDataAtual().isMaior(controllerSA.getEvento().getDataLimSub())) {
                        controllerSA.criarSubmissaoRejeitada();
                    } else {
                        controllerSA.setSubmissaoSubmetidaParaRevisao();
                    }
                    JOptionPane.showMessageDialog(SubmeterArtigoUI.this, "Artigo submetido com sucesso", "Submeter Artigo", JOptionPane.INFORMATION_MESSAGE);
                    SubmeterArtigoUI.this.dispose();
                }

            }
        });

        cancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SubmeterArtigoUI.this.dispose();
            }
        });

        p4.add(ok);
        p4.add(cancelar);

        return p4;
    }
}
