/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Revisao;
import eventoscientificos.utils.LeitorFicheiroRevisoes;
import eventoscientificos.utils.LeitorFicheiroRevisoesCSV;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author emanuelmarques
 */
public class ImportarRevisoesUI extends JDialog {

    private JTextField txtPath;
    private String pathFicheiro;
    private Empresa emp;

    /**
     *
     * @param pai
     * @param emp
     */
    public ImportarRevisoesUI(Frame pai, Empresa emp) {

        super(pai, "Importar revisoes por ficheiro", true);
        
        this.emp = emp;

        JPanel painel = new JPanel(new GridLayout(2, 1));

        painel.add(criarPainel());
        painel.add(criarPainelButoes());

        add(painel);
        pack();
        setLocationRelativeTo(null);

        setResizable(false);
        setVisible(true);
    }

    private JPanel criarPainel() {

        JPanel painel = new JPanel();

        JLabel path = new JLabel("Caminho: ");
        txtPath = new JTextField(20);
        JButton btnBrowse = new JButton("Procurar");
        btnBrowse.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                JFileChooser chooser = new JFileChooser();
                int i = chooser.showOpenDialog(null);
                if (i == JFileChooser.APPROVE_OPTION) {
                    txtPath.setText(chooser.getSelectedFile().getPath());
                    pathFicheiro = chooser.getSelectedFile().getAbsolutePath();
                }
            }
        });

        painel.add(path);
        painel.add(txtPath);
        painel.add(btnBrowse);

        return painel;
    }

    private JPanel criarPainelButoes() {
        JPanel panel = new JPanel();
        JButton ok = new JButton("OK");
        JButton cancelar = new JButton("Cancelar");

        ok.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    LeitorFicheiroRevisoes leitor;
                    if (pathFicheiro.endsWith(".csv")) {
                        leitor = new LeitorFicheiroRevisoesCSV(pathFicheiro, emp);
                        List<Revisao> revisoes = leitor.lerFicheiro();
                        if (revisoes.size() > 0) {

                            JOptionPane.showMessageDialog(ImportarRevisoesUI.this, "As revisoes foram adicionadas ao evento pretendido", "Importar artigos", JOptionPane.INFORMATION_MESSAGE);
                            ImportarRevisoesUI.this.dispose();

                        }
                    }else{
                        JOptionPane.showMessageDialog(ImportarRevisoesUI.this, "Tipo de ficheiro invalida", "Erro", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(ImportarRevisoesUI.this, "Evento não encontrado", "Erro", JOptionPane.ERROR_MESSAGE);
                }

            }
        }
        );

        cancelar.addActionListener(
                new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e
                    ) {
                        dispose();
                    }
                }
        );

        panel.add(ok);

        panel.add(cancelar);

        return panel;
    }
}
