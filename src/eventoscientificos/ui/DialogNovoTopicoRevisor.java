/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.CriarCPController;
import eventoscientificos.controllers.CriarTopicoEventoController;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Topico;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author emanuelmarques
 */
public class DialogNovoTopicoRevisor extends JDialog {

    private JTextField txtCodigo, txtDescricao;
    private Empresa empresa;
    private CriarCPController controllerCP;
    private CriarTopicoEventoController criarTopicoController;

    /**
     *
     * @param pai
     * @param emp
     * @param controller
     * @param controllerCP
     */
    public DialogNovoTopicoRevisor(JDialog pai, Empresa emp, CriarTopicoEventoController controller, CriarCPController controllerCP) {

        super(pai, "Criar Topicos de Evento", true);
        this.criarTopicoController = controller;
        this.controllerCP=controllerCP;
        this.empresa = emp;

        

        
        JPanel p1 = new JPanel(new GridLayout(3, 1));

        p1.add(criarPainelCodigo());
        p1.add(criarPainelDescricao());
        p1.add(criarPainelBotoes());
        add(p1);
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);

    }

    private JPanel criarPainelCodigo() {
        JLabel lbl = new JLabel("Codigo:");

        final int CAMPO_LARGURA = 20;
        txtCodigo = new JTextField(CAMPO_LARGURA);
        txtCodigo.requestFocus();

        JPanel p = new JPanel();

        p.add(lbl);
        p.add(txtCodigo);

        return p;
    }

    private JPanel criarPainelDescricao() {
        JLabel lbl = new JLabel("Descrição:");

        final int CAMPO_LARGURA = 20;
        txtDescricao = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(lbl);
        p.add(txtDescricao);

        return p;
    }

    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoOK() {
        JButton btn = new JButton("Confirmar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Topico t = criarTopicoController.novoTopico(txtCodigo.getText(), txtDescricao.getText());
                
                if (t == null) {
                    JOptionPane.showMessageDialog(DialogNovoTopicoRevisor.this, "Tópico inválido", "Erro ao criar tópico", JOptionPane.ERROR_MESSAGE);

                } else if (!empresa.getListaTopicosACM().getListaTopicos().contains(t)) {
                    JOptionPane.showMessageDialog(DialogNovoTopicoRevisor.this, "Tópico inválido", "Erro ao criar tópico", JOptionPane.ERROR_MESSAGE);

                } else if (controllerCP.getM_revisor().getM_listaTopicos().getListaTopicos().contains(t)) {
                    JOptionPane.showMessageDialog(DialogNovoTopicoRevisor.this, "Tópico já existente", "Erro ao criar tópico", JOptionPane.ERROR_MESSAGE);

                } else {
                    
                    JTextArea topico = new JTextArea(t.toString());
                    int i = JOptionPane.showConfirmDialog(DialogNovoTopicoRevisor.this, topico, "Confirmar", 0, JOptionPane.QUESTION_MESSAGE);
                    if (i == 0) {
                        criarTopicoController.registaTopico(t);
                        controllerCP.addTopico(t);
                    }
                }
                int k = JOptionPane.showConfirmDialog(DialogNovoTopicoRevisor.this, "Pretende adicionar mais tópicos?", "Continuar", 0, JOptionPane.QUESTION_MESSAGE);

                if (k == 0) {
                    txtCodigo.setText("");
                    txtDescricao.setText("");
                } else {

                    
                    JOptionPane.showMessageDialog(DialogNovoTopicoRevisor.this, "Tópicos criados e atribuidos com sucesso", "Atribuir Topicos a revisor", JOptionPane.INFORMATION_MESSAGE);
                    DialogNovoTopicoRevisor.this.dispose();
                }
            }
        });
        return btn;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

}
