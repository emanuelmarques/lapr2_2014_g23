package eventoscientificos.ui;

import eventoscientificos.model.Evento;
import eventoscientificos.model.MecanismoSugestao;
import eventoscientificos.model.Submissao;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;

import javax.swing.*;

/**
 *
 * @author Paulo Silva
 */
public class DecidirSobreArtigosUI extends JDialog {

    private JList list;

    private Evento evento;
    private MecanismoSugestao mec;

    /**
     *
     * @param pai
     * @param evento
     * @param mec
     */
    public DecidirSobreArtigosUI(Frame pai, Evento evento, MecanismoSugestao mec) {
        super(pai, "Decidir sobre artigos", true);
        this.evento = evento;
        this.mec=mec;
        JPanel painel = new JPanel(new GridLayout(2, 1));
        painel.add(criarPainelLista());
        painel.add(criarPainelButoes());
        add(painel);
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setResizable(true);
        setVisible(true);
    }

    private JPanel criarPainelLista() {
        JPanel p = new JPanel();

        list = new JList(this.evento.getSubmissoes().getListaSubmissoes().toArray(new Submissao[this.evento.getSubmissoes().getListaSubmissoes().size()])); 
        JScrollPane scroll = new JScrollPane(list);

        p.add(scroll);

        return p;

    }

    private JPanel criarPainelButoes() {
        JPanel p = new JPanel();
        JButton abrir = new JButton("Abrir");
        JButton terminar = new JButton("Terminar");

        p.add(abrir);
        p.add(terminar);

        abrir.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                
                int index = DecidirSobreArtigosUI.this.list.getSelectedIndex();
                if (index==-1){
                    JOptionPane.showMessageDialog(DecidirSobreArtigosUI.this, "Nao esta selecionada nenhuma submissao", "Decidir sobre Artigo", JOptionPane.ERROR_MESSAGE);
                }else{
                    
                   
                     DialogDecidirArtigo d = new DialogDecidirArtigo(DecidirSobreArtigosUI.this, DecidirSobreArtigosUI.this.evento.getSubmissoes().getListaSubmissoes().get(index), DecidirSobreArtigosUI.this.evento, mec);
                     d.setVisible(true);
                }
                
                
               

            }

        });
        terminar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                DecidirSobreArtigosUI.this.dispose();
            }

        });
        return p;
    }

}
