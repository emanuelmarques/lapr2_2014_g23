/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.CriarTopicoEventoController;
import eventoscientificos.eventstates.ValoresDeRegistoDefinidoState;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Topico;
import eventoscientificos.model.Utilizador;
import eventoscientificos.utils.Data;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author M&M
 */
public class CriarTopicosEventoUI extends JDialog {

    private JTextField txtCodigo, txtDescricao;
    private Empresa empresa;
    private Utilizador utilizadorAtual;
    private CriarTopicoEventoController criarTopicoController;
    private final Frame MAINFRAME;

    /**
     *
     * @param pai
     * @param emp
     * @param u
     */
    public CriarTopicosEventoUI(Frame pai, Empresa emp, Utilizador u) {

        super(pai, "Criar Topicos de Evento", true);
        MAINFRAME = pai;

        this.empresa = emp;
        this.utilizadorAtual = u;
        criarTopicoController = new CriarTopicoEventoController(this.empresa);

        ArrayList<String> titulos = new ArrayList();
        ArrayList<Evento> eventoCorrespondente = new ArrayList();
        for (Evento evento : criarTopicoController.iniciarCricao(this.utilizadorAtual.getUsername())) {
            if (Data.getDataAtual().isMaior(evento.getDataFim())) {
                evento.criarEstadoEventoTerminado();
            }
            if (evento.getState() instanceof ValoresDeRegistoDefinidoState) {
                titulos.add(evento.getTitulo());
                eventoCorrespondente.add(evento);
            }

        }

        if (titulos.size() > 0) {

            JComboBox eventosC = new JComboBox(titulos.toArray(new String[titulos.size()]));
            String[] opts = {"Ok", "Cancelar"};
            int i = JOptionPane.showOptionDialog(pai, eventosC, "Escolher evento", 0, JOptionPane.QUESTION_MESSAGE, null, opts, null);
            if (i == 1) {
                dispose();
            } else {
                criarTopicoController.setEvento(eventoCorrespondente.get(eventosC.getSelectedIndex()));
                JPanel p1 = new JPanel(new GridLayout(3, 1));

                p1.add(criarPainelCodigo());
                p1.add(criarPainelDescricao());
                p1.add(criarPainelBotoes());
                add(p1);
                pack();
                setLocationRelativeTo(null);
                setResizable(false);
                setVisible(true);
            }
        } else {
            JOptionPane.showMessageDialog(CriarTopicosEventoUI.this, "Nao ha eventos validos!", "Erro", JOptionPane.INFORMATION_MESSAGE);
            dispose();
        }

    }

    private JPanel criarPainelCodigo() {
        JLabel lbl = new JLabel("Codigo:");

        final int CAMPO_LARGURA = 20;
        txtCodigo = new JTextField(CAMPO_LARGURA);
        txtCodigo.requestFocus();

        JPanel p = new JPanel();

        p.add(lbl);
        p.add(txtCodigo);

        return p;
    }

    private JPanel criarPainelDescricao() {
        JLabel lbl = new JLabel("Descrição:");

        final int CAMPO_LARGURA = 20;
        txtDescricao = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel();
      
        p.add(lbl);
        p.add(txtDescricao);

        return p;
    }

    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoOK() {
        JButton btn = new JButton("Confirmar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Topico t = criarTopicoController.novoTopico(txtCodigo.getText(), txtDescricao.getText());

                if (t == null) {
                     JOptionPane.showMessageDialog(MAINFRAME, "Tópico inválido", "Erro ao criar tópico", JOptionPane.ERROR_MESSAGE);

                } else if (!empresa.getListaTopicosACM().getListaTopicos().contains(t)) {
                    JOptionPane.showMessageDialog(MAINFRAME, "Tópico inválido", "Erro ao criar tópico", JOptionPane.ERROR_MESSAGE);

                } else if (criarTopicoController.getEvento().getListaTopicos().getListaTopicos().contains(t)) {
                    JOptionPane.showMessageDialog(MAINFRAME, "Tópico já existente", "Erro ao criar tópico", JOptionPane.ERROR_MESSAGE);

                } else {

                    JTextArea topico = new JTextArea(t.toString());
                    int i = JOptionPane.showConfirmDialog(MAINFRAME, topico, "Confirmar", 0, JOptionPane.QUESTION_MESSAGE);
                    if (i == 0) {
                        criarTopicoController.registaTopico(t);
                    }
                }
                int k = JOptionPane.showConfirmDialog(MAINFRAME, "Pretende adicionar mais tópicos?", "Continuar", 0, JOptionPane.QUESTION_MESSAGE);

                if (k == 0) {
                    txtCodigo.setText("");
                    txtDescricao.setText("");
                } else {

                    criarTopicoController.setTopicosCriados();
                    JOptionPane.showMessageDialog(MAINFRAME, "Tópicos definidos com sucesso!", "Tópicos definidos", JOptionPane.INFORMATION_MESSAGE);
                    CriarTopicosEventoUI.this.dispose();
                }
            }
        });
        return btn;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

}
