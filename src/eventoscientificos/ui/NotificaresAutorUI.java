package eventoscientificos.ui;

import eventoscientificos.controllers.NotificarAutoresController;
import eventoscientificos.eventstates.EventoDecididoState;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Utilizador;
import eventoscientificos.utils.Data;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author M&M
 */
public class NotificaresAutorUI extends JDialog {

    private NotificarAutoresController controllerNA;
    private Empresa empresa;
    private Utilizador utilizadorAtual;
  private ArrayList<Evento> eventoCorr;
    private JComboBox combo;

    /**
     *
     * @param pai
     * @param empresa
     * @param user
     */
    public NotificaresAutorUI(Frame pai, Empresa empresa, Utilizador user) {

        super(pai, "Notificar Autor", true);
        this.utilizadorAtual = user;
        this.empresa = empresa;
        this.controllerNA = new NotificarAutoresController(this.empresa);
        if (controllerNA.getEventosOrganizador(utilizadorAtual.getUsername()).isEmpty()) {
            JOptionPane.showMessageDialog(NotificaresAutorUI.this, "Nao e organizador de nenhum evento", "Notificar autor", JOptionPane.ERROR_MESSAGE);
        } else {
            JPanel p1 = criarPainelEvento();
            if (this.combo.getItemCount() > 0) {
                JPanel p3 = criarPainelBotoes();

                add(p1, BorderLayout.CENTER);
                add(p3, BorderLayout.SOUTH);
                pack();
                setLocationRelativeTo(null);

                setResizable(false);
                setVisible(true);
            } else {
                JOptionPane.showMessageDialog(NotificaresAutorUI.this, "Nao ha eventos validos!", "Erro", JOptionPane.INFORMATION_MESSAGE);
                dispose();
            }
        }
    }

    private JPanel criarPainelEvento() {

        JLabel lbl = new JLabel("Escolha um evento");
        JPanel p = new JPanel(new GridLayout(2, 1));

        ArrayList<String> titulos = new ArrayList();
        eventoCorr = new ArrayList();
        for (Evento evento : controllerNA.getEventosOrganizador(utilizadorAtual.getUsername())) {
            if (Data.getDataAtual().isMaior(evento.getDataFim())) {
                evento.criarEstadoEventoTerminado();
            }
            if (evento.getState() instanceof EventoDecididoState) {
                titulos.add(evento.getTitulo());
                eventoCorr.add(evento);
            }

        }

        combo = new JComboBox(titulos.toArray(new String[titulos.size()]));

        p.add(lbl);
        p.add(combo);

        return p;
    }

    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();

        JButton btnCancelar = criarBotaoCancel();

        JPanel p = new JPanel();

        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    controllerNA.selectEvento(eventoCorr.get(combo.getSelectedIndex()));
                    controllerNA.notificarAutores();
                    controllerNA.setEventoNotificado();
                    JOptionPane.showMessageDialog(NotificaresAutorUI.this, "Autores Notificados", "Notificar Autores", JOptionPane.INFORMATION_MESSAGE);
                    NotificaresAutorUI.this.dispose();
                } catch (FileNotFoundException ex) {
                    JOptionPane.showMessageDialog(NotificaresAutorUI.this, "Nao foi possivel criar o ficheiro", "NotificarAutores", JOptionPane.ERROR_MESSAGE);
                    NotificaresAutorUI.this.dispose();
                }
            }
        });
        return btn;
    }

    private JButton criarBotaoCancel() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                NotificaresAutorUI.this.dispose();

            }
        });
        return btn;
    }
}
