package eventoscientificos.ui;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Utilizador;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import javax.swing.*;

/**
 *
 * @author Paulo Silva
 */
public class LogInFrame extends JFrame {

    private Empresa empresa;
    private JLabel lblUsername, lblPassword;
    private JTextField txtUsername;
    private JPasswordField txtPassword;
    private JButton btnRegistar, btnLogin;

    /**
     *
     * @param emp
     */
    public LogInFrame(Empresa emp) {

        super("Gestão eventos científicos - Acesso");
        this.empresa = emp;
        JPanel painel = new JPanel(new GridLayout(3, 1));

        painel.add(criarPainelUser());
        painel.add(criarPainelPassword());
        painel.add(criarPainelButoes());

        add(painel);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                escrever(LogInFrame.this.empresa);
            }
        });

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);

    }

    /**
     *
     * @return
     */
    public Empresa getEmpresa() {
        return this.empresa;
    }
    

    private JPanel criarPainelUser() {
        JPanel painel = new JPanel();
        lblUsername = new JLabel();
        lblUsername.setText("Username: ");
        painel.add(lblUsername);
        txtUsername = new JTextField(20);
        painel.add(txtUsername);

        return painel;
    }

    private JPanel criarPainelPassword() {
        JPanel painel = new JPanel();
        lblPassword = new JLabel();
        lblPassword.setText("Password: ");
        painel.add(lblPassword);
        txtPassword = new JPasswordField(20);
        painel.add(txtPassword);

        return painel;
    }

    private JPanel criarPainelButoes() {
        JPanel painel = new JPanel();
        btnLogin = new JButton("Login");
        
        btnLogin.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                boolean flag = false;
                Utilizador u = new Utilizador();
                String pw = "";
                for (int i = 0; i < txtPassword.getPassword().length; i++) {
                    pw += txtPassword.getPassword()[i];

                }
                for (Utilizador o : LogInFrame.this.empresa.getRegistaUtilizadores().getListaUtilizadores()) {
                    if (o.getUsername().equals(txtUsername.getText())) {
                        flag = true;
                        u = o;
                    }

                }
                if (flag == true) {
                    if (u.getPassword().equals(pw)) {
                        LogInFrame.this.setVisible(false);
                        Janela j = new Janela(LogInFrame.this.empresa, LogInFrame.this, u);

                    } else {
                        JOptionPane.showMessageDialog(LogInFrame.this, "A password e o username nao correspondem", "Gestão de Eventos Cientificos - Acesso", JOptionPane.ERROR_MESSAGE);
                    }

                } else {
                    JOptionPane.showMessageDialog(LogInFrame.this, "A password e o username nao correspondem", "Gestão de Eventos Cientificos - Acesso", JOptionPane.ERROR_MESSAGE);

                }

            }

        });
        painel.add(btnLogin);
        btnRegistar = new JButton("Registar");
        btnRegistar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LogInFrame.this.setVisible(false);
                RegistarUtilizadorUI r = new RegistarUtilizadorUI(LogInFrame.this.empresa, LogInFrame.this);

            }

        });
        painel.add(btnRegistar);
        return painel;

    }

    /**
     *
     * @param emp
     */
    public static void escrever(Empresa emp) {
        ObjectOutputStream objectOut = null;
        try {
            objectOut = new ObjectOutputStream(new FileOutputStream("dados.bin"));
            objectOut.writeObject(emp);
            objectOut.close();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Impossível gravar em ficheiro!\n" + ex, "Gravar", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                objectOut.close();
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Impossível gravar ficheiro!\n" + ex, "Gravar", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

}
