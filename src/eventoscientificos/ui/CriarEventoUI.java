package eventoscientificos.ui;

import eventoscientificos.controllers.CriarEventoCientificoController;
import eventoscientificos.model.Empresa;
import eventoscientificos.utils.Data;
import java.awt.*;
import javax.swing.*;

/**
 *
 * @author Rita
 */
public class CriarEventoUI extends JDialog {

    private final Frame MAINFRAME;

    private CriarEventoCientificoController eventController;

    /**
     *
     * @param pai
     * @param emp
     */
    public CriarEventoUI(Frame pai, Empresa emp) {

        super(pai, "Criar Evento Cientifico", true);
        this.MAINFRAME = pai;
        eventController = new CriarEventoCientificoController(emp);

        JPanel p = new JPanel();

        run();

        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(false);
    }

    private void run() {
        eventController.novoEvento();
        eventController.setEventoCriado();
        DialogCompletarEventos dialog = new DialogCompletarEventos(MAINFRAME, true, eventController, true);
        if (dialog.isFlag()) {
            JTextArea infoEvento = new JTextArea(eventController.getEventoString());
            infoEvento.setEditable(false);
            int i = JOptionPane.showConfirmDialog(MAINFRAME, infoEvento, "Confirmar", 0, JOptionPane.QUESTION_MESSAGE);
            if (i == 1) {
                dispose();
            } else {
                eventController.registaEvento();
                if (Data.getDataAtual().isMaior(eventController.getEvento().getDataFim())) {
                    eventController.setEventoTerminado();
                } else {
                    eventController.setEventoRegistado();
                }
                JOptionPane.showMessageDialog(MAINFRAME, "Evento registado com sucesso!", "Evento criado", JOptionPane.INFORMATION_MESSAGE);
            }
            dispose();
        }
        dispose();
    }
}
