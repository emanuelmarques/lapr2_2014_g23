package eventoscientificos.ui;

import eventoscientificos.controllers.*;
import eventoscientificos.eventstates.TopicosCriadosState;
import eventoscientificos.model.*;
import eventoscientificos.ui.*;
import eventoscientificos.utils.Data;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.*;

/**
 *
 * @author Rita
 */
public class CriarCPUI extends JDialog {

    private JTextField txtID_Membro;

    private Utilizador utilizadorAtual;
    private CriarCPController cpController;
    private Empresa empresa;

    /**
     *
     * @param pai
     * @param emp
     * @param u
     */
    public CriarCPUI(Frame pai, Empresa emp, Utilizador u) {

        super(pai, "Criar Comissão de Programa", true);

        this.empresa = emp;
        this.utilizadorAtual = u;
        cpController = new CriarCPController(this.empresa);

        ArrayList<String> titulos = new ArrayList();
        ArrayList<Evento> eventoCorr = new ArrayList();
        
        for (Evento evento : cpController.getEventosOrganizador(utilizadorAtual.getUsername())) {
            if (Data.getDataAtual().isMaior(evento.getDataFim())) {
                evento.criarEstadoEventoTerminado();
            }
            if (evento.getState() instanceof TopicosCriadosState) {
                titulos.add(evento.getTitulo());
                eventoCorr.add(evento);
            }

        }

        if (titulos.size() > 0) {
            JComboBox combo = new JComboBox(titulos.toArray(new String[titulos.size()]));

            String[] opts = {"OK", "Cancelar"};
            int i = JOptionPane.showOptionDialog(pai, combo, "Escolher evento", 0, JOptionPane.QUESTION_MESSAGE, null, opts, null);
            if (i == 1) {
                dispose();
            } else {
                cpController.selectEvento(eventoCorr.get(combo.getSelectedIndex()));

                JPanel p1 = new JPanel(new GridLayout(2, 1));

                p1.add(criarPainelNovoMembro());
                p1.add(criarPainelBotoes());
                add(p1);
                pack();
                setLocationRelativeTo(null);
                setResizable(false);
                setVisible(true);
            }
        } else {
            JOptionPane.showMessageDialog(CriarCPUI.this, "Nao ha eventos validos!", "Erro", JOptionPane.INFORMATION_MESSAGE);
            dispose();
        }

    }

    private JPanel criarPainelNovoMembro() {
        JLabel lbl = new JLabel("ID do Novo Membro:", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        txtID_Membro = new JTextField(CAMPO_LARGURA);
        txtID_Membro.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtID_Membro);

        return p;
    }

    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Revisor r = cpController.addMembroCP(txtID_Membro.getText());

                    JTextArea membro = new JTextArea(r.toString());
                    int i = JOptionPane.showConfirmDialog(CriarCPUI.this, membro, "Confirmar", 0, JOptionPane.QUESTION_MESSAGE);
                    if (i == 0) {
                        cpController.registaMembroCP(r);
                        int j = JOptionPane.showConfirmDialog(CriarCPUI.this, "Deseja indicar topicos de pericia?", "Confirmar", 0, JOptionPane.QUESTION_MESSAGE);
                        if (j == 0) {
                            DialogAddTopicosPericia a = new DialogAddTopicosPericia(CriarCPUI.this, cpController);
                        }
                    }

                    int m = JOptionPane.showConfirmDialog(CriarCPUI.this, "Pretende adicionar mais membros?",
                            "Continuar", 0, JOptionPane.QUESTION_MESSAGE);

                    if (m == 0) {
                        txtID_Membro.setText("");
                    } else {

                        cpController.getEvento().setCPDefinida(cpController.getEvento().getCP());
                        JOptionPane.showMessageDialog(CriarCPUI.this, "Comissão de Programa definida com sucesso!",
                                "CP definida", JOptionPane.INFORMATION_MESSAGE);
                        CriarCPUI.this.dispose();
                    }
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(CriarCPUI.this, "Dados invalidos", "Criar CP", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        return btn;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

}
