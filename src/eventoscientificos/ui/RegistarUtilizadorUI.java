package eventoscientificos.ui;

import eventoscientificos.controllers.*;
import eventoscientificos.model.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author Paulo Silva
 */
public class RegistarUtilizadorUI extends JDialog {

    private JLabel lblUsername, lblNome, lblEmail, lblPassword, lblConfPassword;
    private JTextField txtUsername, txtNome, txtEmail;
    private JPasswordField txtPassword, txtConfPassword;
    private JButton btnRegistar;
    private Empresa empresa;
    private Window janela;

    /**
     *
     * @param emp
     * @param logIn
     */
    public RegistarUtilizadorUI(Empresa emp, LogInFrame logIn) {

        super(logIn, "Gestão eventos científicos - Registar", true);
        this.empresa = emp;
        this.janela = logIn;
        JPanel painel = new JPanel(new GridLayout(6, 1));

        painel.add(criarPainelUser());
        painel.add(criarPainelNome());
        painel.add(criarPainelEmail());
        painel.add(criarPainelPassword());
        painel.add(criarPainelConfPassword());
        painel.add(criarPainelButoes());
        add(painel);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                RegistarUtilizadorUI.this.janela.setVisible(true);
            }
        });

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);

    }

    /**
     *
     * @param emp
     * @param janela
     * @param nome
     */
    public RegistarUtilizadorUI(Empresa emp, Dialog janela, String nome) {

        super(janela, "Gestão eventos científicos - Registar", true);
        this.empresa = emp;
        this.janela = janela;
        JPanel painel = new JPanel(new GridLayout(6, 1));

        painel.add(criarPainelUser());
        painel.add(criarPainelNome());
        painel.add(criarPainelEmail());
        painel.add(criarPainelPassword());
        painel.add(criarPainelConfPassword());
        painel.add(criarPainelButoes());
        add(painel);

        this.txtNome.setText(nome);
        this.txtNome.setEditable(false);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);

    }

    private JPanel criarPainelUser() {
        JPanel painel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        //lblUsername = new JLabel();
        JLabel lblUsername = new JLabel("Username:", JLabel.RIGHT);
        painel.add(lblUsername);
        txtUsername = new JTextField(20);
        painel.add(txtUsername);

        return painel;
    }

    private JPanel criarPainelNome() {
        JPanel painel = new JPanel();
        lblNome = new JLabel();
        lblNome.setText("Nome: ");
        painel.add(lblNome);
        txtNome = new JTextField(20);
        painel.add(txtNome);

        return painel;
    }

    private JPanel criarPainelEmail() {
        JPanel painel = new JPanel();
        lblEmail = new JLabel();
        lblEmail.setText("Email: ");
        painel.add(lblEmail);
        txtEmail = new JTextField(20);
        painel.add(txtEmail);

        return painel;
    }

    private JPanel criarPainelPassword() {
        JPanel painel = new JPanel();
        lblPassword = new JLabel();
        lblPassword.setText("Password: ");
        painel.add(lblPassword);
        txtPassword = new JPasswordField(20);
        painel.add(txtPassword);

        return painel;
    }

    private JPanel criarPainelConfPassword() {
        JPanel painel = new JPanel();
        lblConfPassword = new JLabel();
        lblConfPassword.setText("Confirmar password: ");
        painel.add(lblConfPassword);
        txtConfPassword = new JPasswordField(20);
        painel.add(txtConfPassword);

        return painel;
    }

    private JPanel criarPainelButoes() {
        JPanel painel = new JPanel();

        btnRegistar = new JButton("Registar");
        painel.add(btnRegistar);
        btnRegistar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                RegistarUtilizadorController control = new RegistarUtilizadorController(RegistarUtilizadorUI.this.empresa);
                control.novoUtilizador();
                String pw = "";

                for (int i = 0; i < txtPassword.getPassword().length; i++) {
                    pw += txtPassword.getPassword()[i];

                }

                control.setDados(txtUsername.getText(), pw, txtNome.getText(), txtEmail.getText());
                RegistarUtilizadorUI.this.dispose();

                if (RegistarUtilizadorUI.this.janela instanceof LogInFrame) {
                    RegistarUtilizadorUI.this.janela.setVisible(true);
                }
            }

        });
        return painel;

    }

    /**
     *
     * @return
     */
    public Empresa getEmpresa() {
        return empresa;
    }

}
