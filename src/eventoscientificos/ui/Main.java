package eventoscientificos.ui;

import eventoscientificos.model.Empresa;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import javax.swing.JOptionPane;

public class Main {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        try {

            Empresa tocs = new Empresa();
            tocs = Main.ler(tocs);
            LogInFrame l = new LogInFrame(tocs);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Foi encontrado um erro inesperado \n"+ex, "Gestao de eventos cientificos", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     *
     * @param tocs
     * @return empresa com dados carregados
     */
    public static Empresa ler(Empresa tocs) {

        ObjectInputStream in = null;
        try {
            in = new ObjectInputStream(new FileInputStream("dados.bin"));
            tocs = (Empresa) in.readObject();
            in.close();
            return tocs;
        } catch (IOException ex) {
           
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Foi encontrado um erro critico: \n" + ex, "Retomar sessao anterior", JOptionPane.ERROR_MESSAGE);
        }
        return tocs;
    }

}
