/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.CriarEventoCientificoController;
import eventoscientificos.eventstates.EventoCriadoPorFicheiroCSVState;
import eventoscientificos.eventstates.EventoCriadoPorFicheiroXMLState;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.utils.LeitorFicheiro;
import eventoscientificos.utils.LeitorFicheiroCSV;
import eventoscientificos.utils.LeitorFicheiroXML;
import eventoscientificos.utils.Utils;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author vitor_000
 */
public class DialogImportarEventos extends JDialog {

    private JTextField txtPath;
    private String pathFicheiro;
    private Empresa emp;
    private Frame framePai;

    /**
     *
     * @param pai
     * @param emp
     */
    public DialogImportarEventos(Frame pai, Empresa emp) {

        super(pai, "Import eventos por ficheiro", true);
        this.framePai = pai;
        this.emp = emp;

        JPanel painel = new JPanel(new GridLayout(2, 1));

        painel.add(criarPainel());
        painel.add(criarPainelButoes());

        add(painel);
        pack();
        setLocationRelativeTo(null);

        setResizable(false);
        setVisible(true);
    }

    private JPanel criarPainel() {

        JPanel painel = new JPanel();

        JLabel path = new JLabel("Caminho: ");
        txtPath = new JTextField(20);
        JButton btnBrowse = new JButton("Procurar");
        btnBrowse.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                JFileChooser chooser = new JFileChooser();
                int i = chooser.showOpenDialog(null);
                if (i == JFileChooser.APPROVE_OPTION) {
                    txtPath.setText(chooser.getSelectedFile().getPath());
                    pathFicheiro = chooser.getSelectedFile().getAbsolutePath();
                }
            }
        });

        painel.add(path);
        painel.add(txtPath);
        painel.add(btnBrowse);

        return painel;
    }

    private JPanel criarPainelButoes() {
        JPanel panel = new JPanel();
        JButton ok = new JButton("OK");
        JButton cancelar = new JButton("Cancelar");

        ok.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LeitorFicheiro leitor;
                if (pathFicheiro.endsWith(".csv")) {

                    CriarEventoCientificoController controller = new CriarEventoCientificoController(emp);
                    leitor = new LeitorFicheiroCSV(pathFicheiro, controller, emp);
                    try {

                        List<Evento> le = leitor.lerFicheiro();
                        for (Evento evento : le) {
                            if (evento.getState() instanceof EventoCriadoPorFicheiroCSVState) {
                                controller.setEvento(evento);
                                DialogCompletarEventos dialog = new DialogCompletarEventos(framePai, true, controller, false);
                                if (dialog.isFlag()) {
                                    JTextArea infoEvento = new JTextArea(controller.getEventoString());
                                    infoEvento.setEditable(false);
                                    int i = JOptionPane.showConfirmDialog(DialogImportarEventos.this, infoEvento, "Confirmar", 0, JOptionPane.QUESTION_MESSAGE);
                                    if (i == 1) {
                                        dispose();
                                    } else {
                                        controller.getEvento().setEventoLidoPorFicheiroCSV();
                                        controller.registaEvento();
                                        JOptionPane.showMessageDialog(DialogImportarEventos.this, "Evento lido com sucesso!", "Evento lido", JOptionPane.INFORMATION_MESSAGE);
                                    }
                                    dispose();
                                }
                            }
                        }

                    } catch (FileNotFoundException ex) {
                        JOptionPane.showMessageDialog(DialogImportarEventos.this, "Ficheiro não encontrado", "Ficheiro não encontrado", JOptionPane.ERROR_MESSAGE);
                       Utils.escreverErro("O ficheiro a importar nao existe");

                    } catch (NullPointerException ex) {
                        JOptionPane.showMessageDialog(DialogImportarEventos.this, "Lista de eventos do ficheiro vazia", "Erro", JOptionPane.ERROR_MESSAGE);
                        Utils.escreverErro("O ficheiro esta vazio");
                    }
                } else if (pathFicheiro.endsWith(".xml")) {

                    CriarEventoCientificoController controller = new CriarEventoCientificoController(emp);
                    leitor = new LeitorFicheiroXML(pathFicheiro, controller, emp);
                    try {

                        List<Evento> le = leitor.lerFicheiro();
                        for (Evento evento : le) {
                            if (evento.getState() instanceof EventoCriadoPorFicheiroXMLState) {
                                controller.setEvento(evento);
                                DialogCompletarEventos dialog = new DialogCompletarEventos(framePai, true, controller, false);
                                if (dialog.isFlag()) {
                                    JTextArea infoEvento = new JTextArea(controller.getEventoString());
                                    infoEvento.setEditable(false);
                                    int i = JOptionPane.showConfirmDialog(DialogImportarEventos.this, infoEvento, "Confirmar", 0, JOptionPane.QUESTION_MESSAGE);
                                    if (i == 1) {
                                        dispose();
                                    } else {
                                        controller.registaEvento();
                                        controller.getEvento().setEventoLidoPorFicheiroXML();
                                        JOptionPane.showMessageDialog(DialogImportarEventos.this, "Evento lido com sucesso!", "Evento lido", JOptionPane.INFORMATION_MESSAGE);
                                    }
                                    dispose();
                                }
                            }
                        }

                    } catch (FileNotFoundException ex) {
                        JOptionPane.showMessageDialog(DialogImportarEventos.this, "Ficheiro não encontrado", "Ficheiro não encontrado", JOptionPane.ERROR_MESSAGE);
                        Utils.escreverErro("O ficheiro a importar nao existe");
                        
                    } catch (NullPointerException ex) {
                        JOptionPane.showMessageDialog(DialogImportarEventos.this, "Lista de eventos do ficheiro vazia", "Erro", JOptionPane.ERROR_MESSAGE);
                        Utils.escreverErro("O ficheiro esta vazio");
                    }
                } else {
                    Utils.escreverErro("O formato do ficheiro nao e valido");
                    JOptionPane.showMessageDialog(DialogImportarEventos.this, "O formato do ficheiro nao e valido", "Formato invalido", JOptionPane.ERROR_MESSAGE);
                    dispose();
                }

            }
        });

        cancelar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        panel.add(ok);
        panel.add(cancelar);

        return panel;
    }
}
