/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.EstatisticaRevisoresController;
import eventoscientificos.controllers.EstatisticasEventoController;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author emanuelmarques
 */
public class EstatisticasRevisorUI extends javax.swing.JDialog {

    private Empresa empresa;
    private Utilizador utilizadorAtual;
    private List<Revisor> revisores;
    EstatisticaRevisoresController controllerER;

    /**
     *
     * @param parent
     * @param modal
     * @param empresa
     * @param utilizadorAtual
     */
    public EstatisticasRevisorUI(java.awt.Frame parent, boolean modal, Empresa empresa, Utilizador utilizadorAtual) {
        super(parent, modal);
        this.empresa = empresa;
        this.utilizadorAtual = utilizadorAtual;
        initComponents();

        jLabel2.setVisible(false);

        preencherComboBoxRevisores();

        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox();
        jButton2 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        btnVerificar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jButton2.setText("Fechar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        btnVerificar.setText("Verificar?");
        btnVerificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerificarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(btnVerificar))
                    .addComponent(jLabel2))
                .addContainerGap(201, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addGap(49, 49, 49))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnVerificar))
                .addGap(51, 51, 51)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        EstatisticasRevisorUI.this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btnVerificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerificarActionPerformed
        Revisor r = this.revisores.get(jComboBox1.getSelectedIndex());
        controllerER = new EstatisticaRevisoresController(empresa, r);

        if (this.empresa.getRevisoesDoRevisor(controllerER.getRevisor()).size() >= 30) {
            String[] alfas = {"0.01", "0.02", "0.04", "0.05", "0.1", "0.2"};

            JComboBox combo = new JComboBox(alfas);
            JOptionPane.showMessageDialog(EstatisticasRevisorUI.this, combo, "Escolha um alfa", JOptionPane.INFORMATION_MESSAGE);

            boolean teste = controllerER.testeHipoteses(Float.parseFloat((String) combo.getSelectedItem()));

            if (teste == true) {
                jLabel2.setText("O Revisor tem um desvio superior a 1");

            } else {
                jLabel2.setText("O revisor tem um desvio inferior a 1");

            }
            jLabel2.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(EstatisticasRevisorUI.this, "O revisor tem menos de 30 revisoes.\n Nao e possivel efetuar"
                    + " um teste de hipoteses", "Teste de hipoteses", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnVerificarActionPerformed

    /**
     * @param args the command line arguments
     */
    private void preencherComboBoxRevisores() {

        this.revisores = new ArrayList<>();
        for (Evento evento : empresa.getRegistaEventos().getLe()) {
            revisores.addAll((evento.getCP().getListaRevisores()).getListaRevisores());
        }
        for (Revisor e : revisores) {
            this.jComboBox1.addItem(e.getNome() + " - " + e.getUtilizador().getEmail());
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnVerificar;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}
