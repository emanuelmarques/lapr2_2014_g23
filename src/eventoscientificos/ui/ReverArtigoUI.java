/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.ReverArtigoController;
import eventoscientificos.eventstates.EventoDistribuidoState;
import eventoscientificos.model.*;
import eventoscientificos.utils.Data;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class ReverArtigoUI extends JDialog {

    private ReverArtigoController reverArtigoController;

    private JComboBox comboConfianca;
    private JComboBox comboAdequacao;
    private JComboBox comboOriginalidade;
    private JComboBox comboQualidade;
    private JComboBox comboRecomendacao;

    private Utilizador utilizadorAtual;

    /**
     *
     * @param pai
     * @param emp
     * @param user
     */
    public ReverArtigoUI(Frame pai, Empresa emp, Utilizador user) {
        super(pai, "Rever Artigo", true);
        this.utilizadorAtual = user;
        reverArtigoController = new ReverArtigoController(emp);

        Evento e = evento();
        if (e == null) {
            dispose();
        } else {
            reverArtigoController.setEvento(e);
            Artigo a = artigo();
            if (a == null) {
                dispose();
            } else {
                reverArtigoController.setArtigo(a);
            }
            JPanel p1 = criarPaineInfo();
            JPanel p2 = criarBotoes();
            add(p1, BorderLayout.NORTH);
            add(p2, BorderLayout.CENTER);
            pack();
            setLocationRelativeTo(null);

            setResizable(false);
            setVisible(true);
        }

    }

    private JPanel criarPaineInfo() {
        JPanel p1 = confianca();
        JPanel p2 = adequacao();
        JPanel p3 = originalidade();
        JPanel p4 = qualidade();
        JPanel p5 = recomendacao();
        JPanel p = new JPanel(new GridLayout(5, 1));

        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);
        p.add(p5);

        return p;

    }

    private Evento evento() {

        Evento[] e = reverArtigoController.getEventosRevisor(this.utilizadorAtual.getUsername()).toArray(new Evento[reverArtigoController.getEventosRevisor(this.utilizadorAtual.getUsername()).size()]);
        ArrayList<Evento> eventoCorr = new ArrayList();
        if (e.length < 1) {
            JOptionPane.showMessageDialog(ReverArtigoUI.this, "Nao é membro de nenhuma CP", "Rever Artigo", JOptionPane.ERROR_MESSAGE);
            dispose();
            return null;
        } else {

            JComboBox eventos = new JComboBox();
            for (Evento evento : e) {
                if (Data.getDataAtual().isMaior(evento.getDataFim())) {
                    evento.criarEstadoEventoTerminado();
                }
                if (evento.getState() instanceof EventoDistribuidoState) {
                    eventos.addItem(evento.getTitulo());
                    eventoCorr.add(evento);
                }

            }
            if (eventos.getItemCount() > 0) {
                String[] opts = {"Escolher", "Cancelar"};
                int i = JOptionPane.showOptionDialog(ReverArtigoUI.this,
                        eventos,
                        "Escolher evento",
                        0,
                        JOptionPane.INFORMATION_MESSAGE,
                        null,
                        opts,
                        opts[0]);
                if (i == 0) {
                    return eventoCorr.get(eventos.getSelectedIndex());
                } else {
                    return null;
                }
            } else {
                JOptionPane.showMessageDialog(ReverArtigoUI.this, "Nao ha eventos validos!", "Erro", JOptionPane.INFORMATION_MESSAGE);
                return null;
            }
        }
    }

    private Artigo artigo() {
        JComboBox artigos = new JComboBox(reverArtigoController.getArtigosEvento().toArray(
                new Artigo[reverArtigoController.getArtigosEvento().size()]));
        String[] opts = {"Escolher", "Cancelar"};
        int i = JOptionPane.showOptionDialog(ReverArtigoUI.this,
                artigos,
                "Escolher artigo",
                0,
                JOptionPane.INFORMATION_MESSAGE,
                null,
                opts,
                opts[0]);
        if (i == 0) {
            reverArtigoController.setArtigo((Artigo) artigos.getSelectedItem());
            if (Data.getDataAtual().isMaior(reverArtigoController.getEvento().getDataLimRev())) {
                JOptionPane.showMessageDialog(ReverArtigoUI.this, "Data limite de revisao expirada", "Erro", JOptionPane.ERROR_MESSAGE);
                reverArtigoController.submissaoDoArtigo().criarSubmissaoRejeitada();
                return null;
            }
            return (Artigo) artigos.getSelectedItem();
        } else {
            return null;
        }

    }

    private JPanel confianca() {
        JLabel lbl = new JLabel("Confianca do revisor nos topicos do artigo:", JLabel.RIGHT);
        String[] confianca = {"0", "1", "2", "3", "4", "5"};
        comboConfianca = new JComboBox(confianca);
        JPanel p = new JPanel();

        p.add(lbl);
        p.add(comboConfianca);
        return p;

    }

    private JPanel adequacao() {
        JLabel lbl = new JLabel("Adequacao ao evento:", JLabel.RIGHT);
        String[] adequecao = {"0", "1", "2", "3", "4", "5"};
        comboAdequacao = new JComboBox(adequecao);
        JPanel p = new JPanel();

        p.add(lbl);
        p.add(comboAdequacao);
        return p;

    }

    private JPanel originalidade() {
        JLabel lbl = new JLabel("Originalidade do artigo:", JLabel.RIGHT);
        String[] originalidade = {"0", "1", "2", "3", "4", "5"};
        comboOriginalidade = new JComboBox(originalidade);
        JPanel p = new JPanel();

        p.add(lbl);
        p.add(comboOriginalidade);
        return p;

    }

    private JPanel qualidade() {
        JLabel lbl = new JLabel("Qualidade de apresentacao:", JLabel.RIGHT);
        String[] qualidade = {"0", "1", "2", "3", "4", "5"};
        comboQualidade = new JComboBox(qualidade);
        JPanel p = new JPanel();

        p.add(lbl);
        p.add(comboQualidade);
        return p;

    }

    private JPanel recomendacao() {
        JLabel lbl = new JLabel("Recomentacao global:", JLabel.RIGHT);
        String[] recomendacao = {"Aceitar", "Rejeitar"};
        comboRecomendacao = new JComboBox(recomendacao);
        JPanel p = new JPanel();

        p.add(lbl);
        p.add(comboRecomendacao);
        return p;

    }

    private JPanel criarBotoes() {
        JButton btnSub = criarBotaoSubmeter();
        getRootPane().setDefaultButton(btnSub);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();

        p.add(btnSub);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoSubmeter() {
        JButton btn = new JButton("Submeter");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Revisao r = reverArtigoController.submissaoDoArtigo().getListaRevisoes().novaRevisao();
                r.setConfianca(Integer.parseInt((String) comboConfianca.getSelectedItem()));
                r.setAdequacao(Integer.parseInt((String) comboConfianca.getSelectedItem()));
                r.setOriginalidade(Integer.parseInt((String) comboConfianca.getSelectedItem()));
                r.setQualidade(Integer.parseInt((String) comboConfianca.getSelectedItem()));
                r.setRecomendacao((String) comboConfianca.getSelectedItem());
                JTextArea textoJ = new JTextArea();
                String[] opts = {"OK", "Cancelar"};
                int i = JOptionPane.showOptionDialog(ReverArtigoUI.this, textoJ, "Escreve um breve texto justificativo", 0, JOptionPane.QUESTION_MESSAGE, null, opts, opts[0]);
                if (i == 1) {
                    JOptionPane.showMessageDialog(ReverArtigoUI.this,
                            "Revisão cancelada.",
                            "Insucesso",
                            JOptionPane.INFORMATION_MESSAGE);
                    dispose();
                } else {
                    r.setTextoJustificativo(textoJ.getText());
                    if (reverArtigoController.validaRevisao(r)) {
                        JTextArea confirmar = new JTextArea(r.toString());
                        confirmar.setEditable(false);
                        i = JOptionPane.showConfirmDialog(ReverArtigoUI.this,
                                confirmar,
                                "Confirmar",
                                JOptionPane.YES_NO_OPTION,
                                JOptionPane.QUESTION_MESSAGE);
                        if (i == 1) {
                            JOptionPane.showMessageDialog(ReverArtigoUI.this,
                                    "Revisão cancelada.",
                                    "Insucesso",
                                    JOptionPane.INFORMATION_MESSAGE);
                            dispose();
                        } else {
                            reverArtigoController.registaRevisao(r);
                            reverArtigoController.setSubmissaoRevista();
                            reverArtigoController.setEventoRevisto();
                            JOptionPane.showMessageDialog(ReverArtigoUI.this,
                                    "Revisão submetida com sucesso!",
                                    "Revisão submetida",
                                    JOptionPane.INFORMATION_MESSAGE);
                        }
                    }
                    dispose();
                }
            }
        });
        return btn;

    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }
}
