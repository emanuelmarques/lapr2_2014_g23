package eventoscientificos.ui;

import eventoscientificos.eventstates.EventoRevistoState;
import eventoscientificos.model.*;
import eventoscientificos.utils.Data;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;

/**
 *
 * @author Paulo Silva
 */
public class Janela extends JFrame {

    private Empresa empresa;
    private LogInFrame loginFrame;
    private Utilizador utilizadorAtual;

    /**
     *
     * @param emp
     * @param login
     * @param u
     */
    public Janela(Empresa emp, LogInFrame login, Utilizador u) {
        super("Gestão de eventos cientificos");
        this.empresa = emp;
        this.loginFrame = login;
        this.utilizadorAtual = u;

        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        menuBar.add(criarMenuAdmistrador());
        menuBar.add(criarMenuOrganizador());
        menuBar.add(criarMenuRevisor());
        menuBar.add(criarMenuUtilizadorAtor());
        menuBar.add(ajuda());

        JLabel fundo = new JLabel(new ImageIcon("ISEP.jpg"));
        add(fundo);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                fechar();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        pack();
        setVisible(true);
        setResizable(true);

        setLocationRelativeTo(null);
    }

    /**
     *
     * @return
     */
    public Empresa getEmpresa() {
        return empresa;
    }

    private JMenu criarMenuAdmistrador() {
        JMenu menu = new JMenu("Admistrador");
        menu.setMnemonic(KeyEvent.VK_A);
        menu.add(criarItemCriarEventoCientifico());
        menu.add(criarItemDecidirEventoCientificoImportado());
        menu.add(criarItemEstatisticaRevisor());
        menu.addSeparator();
        menu.add(criarItemSair());
        return menu;
    }

    private JMenuItem criarItemEstatisticaRevisor() {
        JMenuItem item = new JMenuItem("Estatistica de Revisor", KeyEvent.VK_R);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EstatisticasRevisorUI ui = new EstatisticasRevisorUI(Janela.this, true, empresa, utilizadorAtual);

            }
        });

        return item;
    }

    private JMenuItem criarItemCriarEventoCientifico() {
        JMenuItem item = new JMenuItem("Criar evento cientifico", KeyEvent.VK_E);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CriarEventoUI ui = new CriarEventoUI(Janela.this, Janela.this.empresa);

            }
        });

        return item;
    }

    private JMenuItem criarItemDecidirEventoCientificoImportado() {
        JMenuItem item = new JMenuItem("Aceitar/Rejeitar eventos importados", KeyEvent.VK_E);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DecidirEventosImportadosUI ui = new DecidirEventosImportadosUI(Janela.this, true, Janela.this.empresa);

            }
        });

        return item;
    }

    private JMenuItem criarItemImportarEventoCientifico() {
        JMenuItem item = new JMenuItem("Importar evento(s) cientifico(s)", KeyEvent.VK_I);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogImportarEventos ui = new DialogImportarEventos(Janela.this, Janela.this.empresa);
            }
        });

        return item;
    }

    private JMenuItem criarItemImportarArtigos() {
        JMenuItem item = new JMenuItem("Importar artigo(s)", KeyEvent.VK_S);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ImportarArtigoUI ui = new ImportarArtigoUI(Janela.this, Janela.this.empresa);
            }
        });

        return item;
    }

    private JMenuItem criarItemImportarRevisoes() {
        JMenuItem item = new JMenuItem("Importar revisão(ões)", KeyEvent.VK_R);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ImportarRevisoesUI ui = new ImportarRevisoesUI(Janela.this, Janela.this.empresa);
            }
        });

        return item;
    }

    private JMenuItem criarItemEstatisticaEvento() {
        JMenuItem item = new JMenuItem("Estatisticas de evento", KeyEvent.VK_E);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EstatisticasUI ui = new EstatisticasUI(Janela.this, true, Janela.this.empresa, Janela.this.utilizadorAtual);
            }
        });

        return item;
    }

    private JMenuItem criarItemDefinirValoresDeRegisto() {
        JMenuItem item = new JMenuItem("Definir valores de registo", KeyEvent.VK_R);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DefinirValoresDeRegistoUI ui = new DefinirValoresDeRegistoUI(Janela.this.empresa, Janela.this.utilizadorAtual);
            }
        });

        return item;
    }

    private JMenuItem criarItemSair() {
        JMenuItem item = new JMenuItem("Sair", KeyEvent.VK_S);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fechar();

            }
        });

        return item;

    }

    private JMenu criarMenuOrganizador() {
        JMenu menu = new JMenu("Organizador");
        menu.setMnemonic(KeyEvent.VK_O);
        menu.add(criarItemCriarCP());
        menu.add(criarItemDistribuiRevisoesArtigos());
        menu.add(criarItemDecidirArtigos());
        menu.add(criarItemNotificarAutores());
        menu.add(criarItemCriarTopicoEvento());
        menu.add(criarItemImportarEventoCientifico());
        menu.add(criarItemImportarArtigos());
        menu.add(criarItemImportarRevisoes());
        menu.add(criarItemDefinirValoresDeRegisto());
        menu.add(criarItemEstatisticaEvento());

        return menu;

    }

    private JMenuItem criarItemCriarCP() {
        JMenuItem item = new JMenuItem("Criar comissão de programa", KeyEvent.VK_P);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CriarCPUI cp = new CriarCPUI(Janela.this, Janela.this.empresa, Janela.this.utilizadorAtual);

            }
        });

        return item;
    }

    private JMenuItem criarItemDistribuiRevisoesArtigos() {
        JMenuItem item = new JMenuItem("Destribuir revisões de artigos", KeyEvent.VK_R);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DistribuirRevisoesArtigoUI dialog = new DistribuirRevisoesArtigoUI(Janela.this, empresa, Janela.this.utilizadorAtual);

            }
        });

        return item;
    }

    private JMenuItem criarItemDecidirArtigos() {
        JMenuItem item = new JMenuItem("Decidir artigo", KeyEvent.VK_A);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] opt = {"OK"};
                ArrayList<String> titulos = new ArrayList();
                ArrayList<Evento> eventoCorr = new ArrayList();
                for (Evento evento : Janela.this.empresa.getRegistaEventos().getLe()) {
                    if (Data.getDataAtual().isMaior(evento.getDataFim())) {
                        evento.criarEstadoEventoTerminado();
                    }
                    if (evento.getState() instanceof EventoRevistoState) {
                        titulos.add(evento.getTitulo());
                        eventoCorr.add(evento);
                    }

                }

                if (titulos.size() > 0) {
                    String[] comboMec = {"Mecanismo 1"};
                    JComboBox combo = new JComboBox(titulos.toArray(new String[titulos.size()]));
                    JComboBox combo1 = new JComboBox(comboMec);
                    int i = JOptionPane.showOptionDialog(Janela.this, combo, "Escolher Evento", 0, JOptionPane.QUESTION_MESSAGE, null, opt, opt[0]);
                    if (i == 0) {

                        int j = JOptionPane.showOptionDialog(Janela.this, combo1, "Escolher Mecanismo", 0, JOptionPane.QUESTION_MESSAGE, null, opt, opt[0]);
                        MecanismoSugestao mec;
                        switch (combo1.getSelectedIndex()) {
                            default:
                                mec = new MecanismoSugestao1();
                                break;

                        }

                        if (j == 0) {
                            DecidirSobreArtigosUI dialog = new DecidirSobreArtigosUI(Janela.this, eventoCorr.get(combo.getSelectedIndex()), mec);
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(Janela.this, "Nao ha eventos validos!", "Erro", JOptionPane.INFORMATION_MESSAGE);

                }
            }
        }
        );

        return item;
    }

    private JMenuItem criarItemNotificarAutores() {
        JMenuItem item = new JMenuItem("Notificar autores", KeyEvent.VK_N);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NotificaresAutorUI n = new NotificaresAutorUI(Janela.this, Janela.this.empresa, Janela.this.utilizadorAtual);
            }
        });

        return item;
    }

    private JMenuItem criarItemCriarTopicoEvento() {
        JMenuItem item = new JMenuItem("Definir tópicos de evento", KeyEvent.VK_T);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CriarTopicosEventoUI c = new CriarTopicosEventoUI(Janela.this, Janela.this.empresa, Janela.this.utilizadorAtual);

            }
        });

        return item;
    }

    private JMenu criarMenuRevisor() {
        JMenu menu = new JMenu("Revisor");
        menu.setMnemonic(KeyEvent.VK_R);
        menu.add(criarItemSubmeterRevisaoArtigo());

        return menu;
    }

    private JMenuItem criarItemSubmeterRevisaoArtigo() {
        JMenuItem item = new JMenuItem("Submeter revisão de artigo", KeyEvent.VK_R);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ReverArtigoUI c = new ReverArtigoUI(Janela.this, Janela.this.empresa, Janela.this.utilizadorAtual);

            }
        });

        return item;
    }

    private JMenu criarMenuUtilizadorAtor() {
        JMenu menu = new JMenu("Utilizador/Ator");
        menu.setMnemonic(KeyEvent.VK_R);
        menu.add(criarItemSubmeterArtigo());
        menu.add(criarItemPagarSubmissao());
        return menu;
    }

    private JMenuItem criarItemSubmeterArtigo() {
        JMenuItem item = new JMenuItem("Submeter artigo", KeyEvent.VK_S);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SubmeterArtigoUI s = new SubmeterArtigoUI(Janela.this, Janela.this.empresa);

            }
        });

        return item;
    }

    private JMenuItem criarItemPagarSubmissao() {
        JMenuItem item = new JMenuItem("Pagar submissao", KeyEvent.VK_S);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PagarSubmissaoUI p = new PagarSubmissaoUI(Janela.this, true, Janela.this.empresa, Janela.this.utilizadorAtual);

            }
        });

        return item;
    }

    /**
     *
     * @return
     */
    public JMenu ajuda() {

        JMenu menu = new JMenu("Ajuda");
        menu.add(guardar());
        menu.add(acerca());

        return menu;

    }

    /**
     * cria o item "acerca"
     *
     * @return item "acerca"
     */
    private JMenuItem acerca() {
        JMenuItem item = new JMenuItem("Acerca");
        item.setAccelerator(KeyStroke.getKeyStroke("ctrl A"));
        item.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(Janela.this, "Instituto Superior de Engenharia do Porto\nLicenciatura em Engenharia Informática\n"
                        + "Laboratório e Projeto II\n\nRealizado por:\nEmanuel Marques - 1130553@isep.ipp.pt\nVitor Moreira - 1130564@isep.ipp.pt\n"
                        + "Rita Carvalho - 1130511@isep.ipp.pt\nAria Jozi - 1130777@isep.ipp.pt\nPaulo Silva - 1130298@isep.ipp.pt", "Acerca", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        );
        return item;
    }

    private JMenuItem guardar() {
        JMenuItem item = new JMenuItem("Guardar");
        item.setAccelerator(KeyStroke.getKeyStroke("ctrl A"));
        item.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LogInFrame.escrever(Janela.this.empresa);
                JOptionPane.showMessageDialog(Janela.this, "Informaçoes guardadas com sucesso", "Guardar informaçoes", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        );
        return item;
    }

    private void fechar() {
        String[] OpSimNao = {"Sim", "Não"};
        int resposta = JOptionPane.showOptionDialog(Janela.this, "Deseja sair da aplicação?", "Gestão de Eventos Científicos", 0, JOptionPane.QUESTION_MESSAGE, null, OpSimNao, OpSimNao[1]);
        if (resposta == 0) {
            LogInFrame.escrever(this.empresa);
            dispose();
            this.loginFrame.dispose();
        }

    }

}
