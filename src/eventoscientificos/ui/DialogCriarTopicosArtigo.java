/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.SubmeterArtigoController;
import eventoscientificos.model.Topico;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author emanuelmarques
 */
public class DialogCriarTopicosArtigo extends JDialog {

    List<JCheckBox> topicosEvento;
    List<Topico> topicosParaAdicionar;
    SubmeterArtigoController controllerSA;
    ArrayList<JCheckBox> check;

    /**
     *
     * @param pai
     * @param controller
     */
    public DialogCriarTopicosArtigo(JDialog pai, SubmeterArtigoController controller) {
        super(pai, "Adicionar Topicos", true);
        controllerSA = controller;
        JPanel painel = new JPanel(new GridLayout(2, 1));

        painel.add(criarPainel());
        painel.add(criarPainelButoes());

        add(painel);
        pack();
        setLocationRelativeTo(null);

        setResizable(false);
        setVisible(true);

    }

    /**
     *
     * @return
     */
    public JPanel criarPainel() {
        JPanel painel = new JPanel(new GridLayout(2, 2));
        topicosEvento = new ArrayList<>();
        for (Topico topico : controllerSA.getTopicosEvento()) {
            JCheckBox box = new JCheckBox(topico.getDescricao());
            box.setSelected(false);
            topicosEvento.add(box);

        }

        for (JCheckBox jCheckBox : topicosEvento) {

            painel.add(jCheckBox);

        }

        return painel;

    }

    private JPanel criarPainelButoes() {
        JPanel p4 = new JPanel();
        JButton ok = new JButton("Adicionar");
        JButton cancelar = new JButton("Cancelar");

        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                topicosParaAdicionar=new ArrayList<>();
                for (int i = 0; i < topicosEvento.size(); i++) {
                    if (topicosEvento.get(i).isSelected()) {
                        topicosParaAdicionar.add(controllerSA.getTopicosEvento().get(i));

                    }

                }
                controllerSA.setListaTopicosArtigo(topicosParaAdicionar);
                JOptionPane.showMessageDialog(DialogCriarTopicosArtigo.this, "Topicos definidos com sucesso", "Definir topicos de Artigo", JOptionPane.INFORMATION_MESSAGE);
                DialogCriarTopicosArtigo.this.dispose();
            }
        });

        cancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogCriarTopicosArtigo.this.dispose();
            }
        });

        p4.add(ok);
        p4.add(cancelar);

        return p4;
    }

}
