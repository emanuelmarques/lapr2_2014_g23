package eventoscientificos.ui;

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import eventoscientificos.controllers.CriarCPController;
import eventoscientificos.controllers.CriarTopicoEventoController;
import eventoscientificos.controllers.SubmeterArtigoController;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Topico;
import eventoscientificos.ui.DialogNovoTopicoRevisor;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author emanuelmarques
 */
public class DialogAddTopicosPericia extends JDialog {

    private List<JCheckBox> topicosEvento;
    private List<Topico> topicosParaAdicionar;
    private CriarCPController controllerCP;
    private Revisor revisor;
    private ArrayList<JCheckBox> check;

    /**
     *
     * @param pai
     * @param controller
     */
    public DialogAddTopicosPericia(JDialog pai, CriarCPController controller) {
        super(pai, "Adicionar Topicos", true);
        controllerCP = controller;
        this.revisor=controllerCP.getCP().getListaRevisores().getListaRevisores().get(controllerCP.getCP().getListaRevisores().getListaRevisores().size()-1);
        JPanel painel = new JPanel(new GridLayout(2, 1));

        painel.add(criarPainel());
        painel.add(criarPainelButoes());

        add(painel);
        pack();
        setLocationRelativeTo(null);

        setResizable(false);
        setVisible(true);

    }

    /**
     *
     * @return
     */
    public JPanel criarPainel() {
        JPanel painel = new JPanel(new GridLayout(2, 2));
        topicosEvento = new ArrayList<>();
        for (Topico topico : controllerCP.getTopicosEvento()) {
            JCheckBox box = new JCheckBox(topico.getDescricao());
            box.setSelected(false);
            topicosEvento.add(box);

        }

        for (JCheckBox jCheckBox : topicosEvento) {

            painel.add(jCheckBox);

        }

        return painel;

    }

    private JPanel criarPainelButoes() {
        JPanel p4 = new JPanel();
        JButton ok = new JButton("Adicionar");
        JButton cancelar = new JButton("Cancelar");

        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                topicosParaAdicionar = new ArrayList<>();
                for (int i = 0; i < topicosEvento.size(); i++) {
                    if (topicosEvento.get(i).isSelected()) {
                        topicosParaAdicionar.add(controllerCP.getTopicosEvento().get(i));

                    }

                }
                controllerCP.setListaTopicosRevisor(revisor, topicosParaAdicionar);
                JOptionPane.showMessageDialog(DialogAddTopicosPericia.this, "Topicos inseridos com sucesso", "Definir topicos de Revisor", JOptionPane.INFORMATION_MESSAGE);
                int i = JOptionPane.showConfirmDialog(DialogAddTopicosPericia.this, "Inserir mais topicos manualmente?", "Definir topicos de pericia", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (i == 0) {
                    controllerCP.setRevisor(revisor);
                    CriarTopicoEventoController controllerCTE = controllerCP.criarTopico();
                    DialogNovoTopicoRevisor n = new DialogNovoTopicoRevisor(DialogAddTopicosPericia.this, controllerCP.getEmpresa(), controllerCTE, controllerCP);
                }

                DialogAddTopicosPericia.this.dispose();
            }
        });

        cancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogAddTopicosPericia.this.dispose();
            }
        });

        p4.add(ok);
        p4.add(cancelar);

        return p4;
    }

}
