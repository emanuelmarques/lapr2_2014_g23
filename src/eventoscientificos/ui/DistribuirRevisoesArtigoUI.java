/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.DistribuirRevisoesArtigoController;
import eventoscientificos.eventstates.CPDefinidaState;
import eventoscientificos.exceptions.ImpossivelDistribuirException;
import eventoscientificos.model.*;
import eventoscientificos.utils.Data;
import eventoscientificos.utils.Utils;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Paulo Silva
 */
public class DistribuirRevisoesArtigoUI extends JDialog {

    private JComboBox cmbEventos;
    private JScrollPane scroll;
    private Empresa empresa;
    private DistribuirRevisoesArtigoController distController;
    private Utilizador utilizadorAtual;
    private ArrayList<Evento> eventoCorr;

    /**
     *
     * @param pai
     * @param emp
     * @param uAtual
     */
    public DistribuirRevisoesArtigoUI(JFrame pai, Empresa emp, Utilizador uAtual) {

        super(pai, "Distribuir revisoes de artigo", true);
        this.empresa = emp;
        this.utilizadorAtual = uAtual;
        this.distController = new DistribuirRevisoesArtigoController(this.empresa);
        if (distController.novaDistribuicao(utilizadorAtual.getUsername()).isEmpty()) {
            JOptionPane.showMessageDialog(DistribuirRevisoesArtigoUI.this, "Nao é organizador de nenhum evento\n", "Impossivel distribuir", JOptionPane.ERROR_MESSAGE);

        } else {

            JPanel p1 = criarPainelEventos();
            if (this.cmbEventos.getItemCount() > 0) {
                JPanel p2 = criarPainelBotoes();

                add(p1, BorderLayout.CENTER);
                add(p2, BorderLayout.SOUTH);
                pack();
                setLocationRelativeTo(null);

                setResizable(false);
                setVisible(true);
            } else {
                JOptionPane.showMessageDialog(DistribuirRevisoesArtigoUI.this, "Nao ha eventos validos!", "Erro", JOptionPane.INFORMATION_MESSAGE);
                dispose();
            }
        }

    }

    private JPanel criarPainelEventos() {
        JLabel lbl1 = new JLabel("Evento: ");

        ArrayList<String> titulos = new ArrayList();
        eventoCorr = new ArrayList();
        for (Evento evento : distController.novaDistribuicao(utilizadorAtual.getUsername())) {
            if (Data.getDataAtual().isMaior(evento.getDataFim())) {
                evento.criarEstadoEventoTerminado();
            }
            if (evento.getState() instanceof CPDefinidaState) {
                titulos.add(evento.getTitulo());
                eventoCorr.add(evento);
            }

        }

        cmbEventos = new JComboBox(titulos.toArray(new String[titulos.size()]));

        JPanel p = new JPanel();

        p.add(lbl1);
        p.add(cmbEventos);
        return p;
    }

    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();

        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoOK() {
        JButton btnOk = new JButton("Escolher");
        getRootPane().setDefaultButton(btnOk);
        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                distController.setEvento(eventoCorr.get(cmbEventos.getSelectedIndex()));

                int i = 1;
                while (i != 0) {
                    try {
                        MecanismoDistribuicao[] mecs = empresa.getListaMecanismosDistribuicao().getListaMecanismos().
                                toArray(new MecanismoDistribuicao[empresa.getListaMecanismosDistribuicao().getListaMecanismos().size()]);

                        JComboBox listaMec = new JComboBox(mecs);
                        String[] opts = {"Escolher", "cancelar"};
                        i = JOptionPane.showOptionDialog(DistribuirRevisoesArtigoUI.this, listaMec, "Escolher mecanismo", 0, JOptionPane.QUESTION_MESSAGE, null, opts, opts[0]);

                        if (i == 1) {
                            dispose();
                        } else {
                            List<Distribuicao> ld = distController.setMecanismoDistr((MecanismoDistribuicao) listaMec.getSelectedItem());

                            if (!(scroll == null)) {
                                DistribuirRevisoesArtigoUI.this.remove(scroll);
                            }
                            String[][] tabela = Utils.toTable(ld);
                            String[] titulos = {"Artigo", "Revisor"};

                            DefaultTableModel model = new DefaultTableModel(tabela, titulos);
                            JTable lista = new JTable(model);
                            scroll = new JScrollPane(lista);

                            i = JOptionPane.showConfirmDialog(DistribuirRevisoesArtigoUI.this, scroll, "Confirmar distribuicao",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) ;
                            if (i == 0) {
                                distController.registaDistribuicao();
                                JOptionPane.showMessageDialog(DistribuirRevisoesArtigoUI.this, "Distribuicao efetuada com sucesso", "Distribuicao com sucesso", JOptionPane.INFORMATION_MESSAGE);
                                dispose();
                            }
                        }
                    } catch (ImpossivelDistribuirException ex) {
                        JOptionPane.showMessageDialog(DistribuirRevisoesArtigoUI.this, "Impossivel distribuir com este mecanismo\n", "Impossivel distribuir", JOptionPane.ERROR_MESSAGE);
                    }
                }
                dispose();
            }

        });
        return btnOk;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

}
