/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;


import eventoscientificos.model.Empresa;
import eventoscientificos.model.Submissao;
import eventoscientificos.utils.LeitorFicheiroArtigo;
import eventoscientificos.utils.LeitorFicheiroArtigoCSV;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author emanuelmarques
 */
public class ImportarArtigoUI extends JDialog {

    private JTextField txtPath;
    private String pathFicheiro;
    private Empresa emp;
    private Frame framePai;

    /**
     *
     * @param pai
     * @param emp
     */
    public ImportarArtigoUI(Frame pai, Empresa emp) {

        super(pai, "Importar artigos por ficheiro", true);
        this.framePai = pai;
        this.emp = emp;

        JPanel painel = new JPanel(new GridLayout(2, 1));

        painel.add(criarPainel());
        painel.add(criarPainelButoes());

        add(painel);
        pack();
        setLocationRelativeTo(null);

        setResizable(false);
        setVisible(true);
    }

    private JPanel criarPainel() {

        JPanel painel = new JPanel();

        JLabel path = new JLabel("Caminho: ");
        txtPath = new JTextField(20);
        JButton btnBrowse = new JButton("Procurar");
        btnBrowse.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                JFileChooser chooser = new JFileChooser();
                int i = chooser.showOpenDialog(null);
                if (i == JFileChooser.APPROVE_OPTION) {
                    txtPath.setText(chooser.getSelectedFile().getPath());
                    pathFicheiro = chooser.getSelectedFile().getAbsolutePath();
                }
            }
        });

        painel.add(path);
        painel.add(txtPath);
        painel.add(btnBrowse);

        return painel;
    }

    private JPanel criarPainelButoes() {
        JPanel panel = new JPanel();
        JButton ok = new JButton("OK");
        JButton cancelar = new JButton("Cancelar");

        ok.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    LeitorFicheiroArtigo leitor;
                    if (pathFicheiro.endsWith(".csv")) {
                        leitor = new LeitorFicheiroArtigoCSV(pathFicheiro, emp);
                        List<Submissao> sub = leitor.lerFicheiro();
                        if (sub.size() > 0) {

                            JOptionPane.showMessageDialog(ImportarArtigoUI.this, "Os artigos foram adicionados ao evento pretendido", "Importar artigos", JOptionPane.INFORMATION_MESSAGE);
                            ImportarArtigoUI.this.dispose();

                        }
                    }else{
                        JOptionPane.showMessageDialog(ImportarArtigoUI.this, "Tipo de ficheiro invalida", "Erro", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(ImportarArtigoUI.this, "Evento não encontrado", "Erro", JOptionPane.ERROR_MESSAGE);
                }

            }
        }
        );

        cancelar.addActionListener(
                new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e
                    ) {
                        dispose();
                    }
                }
        );

        panel.add(ok);

        panel.add(cancelar);

        return panel;
    }
}
