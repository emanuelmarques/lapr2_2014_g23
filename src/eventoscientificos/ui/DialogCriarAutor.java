package eventoscientificos.ui;

import eventoscientificos.controllers.SubmeterArtigoController;
import eventoscientificos.model.Autor;
import eventoscientificos.model.Utilizador;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;
import javax.swing.*;

/**
 *
 * @author Paulo Silva
 */
public class DialogCriarAutor extends JDialog {

    JTextField txtNome;
    JTextField txtAfiliacao;
    SubmeterArtigoController controllerSA;

    /**
     *
     * @param pai
     * @param controller
     */
    public DialogCriarAutor(JDialog pai, SubmeterArtigoController controller) {
        super(pai, "Adicionar autor", true);
        controllerSA = controller;
        JPanel painel = new JPanel(new GridLayout(2, 1));

        painel.add(criarPainel());
        painel.add(criarPainelButoes());

        add(painel);
        pack();
        setLocationRelativeTo(null);

        setResizable(false);
        setVisible(true);

    }

    /**
     *
     * @return
     */
    public JPanel criarPainel() {
        JPanel painel = new JPanel(new GridLayout(2, 1));
        JPanel painel1 = new JPanel();
        JPanel painel2 = new JPanel();

        JLabel nome = new JLabel("Nome:");
        txtNome = new JTextField(20);
        painel1.add(nome);
        painel1.add(txtNome);

        JLabel afiliacao = new JLabel("Afiliacao:");
        txtAfiliacao = new JTextField(20);
        painel2.add(afiliacao);
        painel2.add(txtAfiliacao);

        painel.add(painel1);
        painel.add(painel2);

        return painel;

    }

    private JPanel criarPainelButoes() {
        JPanel p4 = new JPanel();
        JButton ok = new JButton("Adicionar");
        JButton cancelar = new JButton("Terminar");

        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Autor autor = controllerSA.novoAutor(txtNome.getText(), txtAfiliacao.getText());

                JTextArea confirmacao = new JTextArea(autor.toString());

                int i = JOptionPane.showConfirmDialog(DialogCriarAutor.this, confirmacao, "Adicionar Autor", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (i == 1) {
                    dispose();

                } else {
                    controllerSA.addAutor(autor);
                    JOptionPane.showMessageDialog(DialogCriarAutor.this, "Autor adicionado com sucesso", "Adicionar Autores", JOptionPane.INFORMATION_MESSAGE);
                    txtNome.setText("");
                    txtAfiliacao.setText("");

                }

            }
        });

        cancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogCriarAutor.this.dispose();
              

                JPanel p3 = new JPanel();
                JLabel lblCorrespondente = new JLabel("Autor Correspondente:");
                Autor[] array = controllerSA.getListaAutores().toArray(new Autor[controllerSA.getListaAutores().size()]);
                Utilizador[] users = controllerSA.getEmpresa().getRegistaUtilizadores().getListaUtilizadores().toArray(new Utilizador[controllerSA.getEmpresa().getRegistaUtilizadores().getListaUtilizadores().size()]);
                JComboBox comboCorrespondente = new JComboBox(array);
                JComboBox utilizadores = new JComboBox(users);

                p3.add(lblCorrespondente);
                p3.add(comboCorrespondente);

                JOptionPane.showOptionDialog(DialogCriarAutor.this, comboCorrespondente, "Adicionar autor correspondete", JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
                String[] opt = {"Selecionar", "Registar novo"};
                int opt1 = JOptionPane.showOptionDialog(DialogCriarAutor.this, utilizadores, "Atribuir utilizador a autor", JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE, null, opt, opt[0]);

                if (opt1 == 0) {
                    
                    ((Autor) comboCorrespondente.getSelectedItem()).setUtilizador((Utilizador) utilizadores.getSelectedItem());
                    controllerSA.setCorrespondente((Autor) comboCorrespondente.getSelectedItem());

                } else {
                    RegistarUtilizadorUI c = new RegistarUtilizadorUI(DialogCriarAutor.this.controllerSA.getEmpresa(), DialogCriarAutor.this, ((Autor) comboCorrespondente.getSelectedItem()).getNome());
                    ((Autor) comboCorrespondente.getSelectedItem()).setUtilizador(controllerSA.getEmpresa().getRegistaUtilizadores().getListaUtilizadores().get(controllerSA.getEmpresa().getRegistaUtilizadores().getListaUtilizadores().size()-1));
                    controllerSA.setCorrespondente((Autor) comboCorrespondente.getSelectedItem());
                    JOptionPane.showMessageDialog(DialogCriarAutor.this, "Autor registado como utilizador e definido como autor correspondente", "Definir autor correspondente", JOptionPane.INFORMATION_MESSAGE);
                }

            }
        });

        p4.add(ok);
        p4.add(cancelar);

        return p4;
    }

}
