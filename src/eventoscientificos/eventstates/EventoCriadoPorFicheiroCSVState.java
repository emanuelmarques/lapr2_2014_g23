/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.eventstates;

import eventoscientificos.model.Evento;
import java.io.Serializable;

/**
 *
 * @author Vitor Moreira
 */
public class EventoCriadoPorFicheiroCSVState implements EventoState, Serializable{

    /**
     * variavel de instancia
     */
    private Evento evento;
    
    /**
     *construtor que recebe um evento como parametro
     * @param e
     */
    public EventoCriadoPorFicheiroCSVState(Evento e) {
        this.evento = e;
    }

    /**
     *metodo que modifica evento criado por ficheiro csv
     */
    @Override
    public void setEventoCriadoPorFicheiroCSV() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica evento lido de ficheiro csv
     */
    @Override
    public void setEventoLidoDeFicheiroCSV() {
        if (this.valida()) {
            EventoState s = new EventoLidoDeFicheiroCSV(this.evento);
            this.evento.setState(s);    
        }
    }

    /**
     *metodo que modifica evento criado por ficheiro xml
     */
    @Override
    public void setEventoCriadoPorFicheiroXML() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica evento lido de ficheiro xml
     */
    @Override
    public void setEventoLidoDeFicheiroXML() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica evento criado
     */
    @Override
    public void setEventoCriado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica evento registado
     */
    @Override
    public void setEventoRegistado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica valores de registo definido
     */
    @Override
    public void setValoresDeRegistoDefinido() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica topicos criados
     */
    @Override
    public void setTopicosCriados() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica CP definida
     */
    @Override
    public void setCPDefinida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica evento distribuido
     */
    @Override
    public void setEventoDistribuido() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica evento revisto
     */
    @Override
    public void setEventoRevisto() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica evento decidido
     */
    @Override
    public void setEventoDecidido() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica evento notificado
     */
    @Override
    public void setEventoNotificado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica camera ready
     */
    @Override
    public void setCameraReady() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que valida
     * @return valida
     */
    @Override
    public boolean valida() {
        return this.evento.valida();
    }

    @Override
    public void setEventoTerminado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
