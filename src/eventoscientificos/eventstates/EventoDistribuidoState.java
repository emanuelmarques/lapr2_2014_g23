/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.eventstates;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Distribuicao;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import eventoscientificos.submissaostates.*;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Vitor Moreira
 */
public class EventoDistribuidoState implements EventoState, Serializable {

    /**
     * variaveis de instancia
     */
    private Evento evento;

    /**
     *construtor que recebe um evento como parametro
     * @param e
     */
    public EventoDistribuidoState(Evento e) {
        this.evento = e;
    }

    /**
     *metodo que modifica evento criado por ficheiro csv
     */
    @Override
    public void setEventoCriadoPorFicheiroCSV() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica evento lido de ficheiro csv
     */
    @Override
    public void setEventoLidoDeFicheiroCSV() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica evento criado por ficheiro xml
     */
    @Override
    public void setEventoCriadoPorFicheiroXML() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica evento lido de ficheiro xml
     */
    @Override
    public void setEventoLidoDeFicheiroXML() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica evento criado
     */
    @Override
    public void setEventoCriado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica evento registado
     */
    @Override
    public void setEventoRegistado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica valores de registo definido
     */
    @Override
    public void setValoresDeRegistoDefinido() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica topicos criados
     */
    @Override
    public void setTopicosCriados() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica CP definida
     */
    @Override
    public void setCPDefinida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica evento distribuido
     */
    @Override
    public void setEventoDistribuido() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica evento revisto
     */
    @Override
    public void setEventoRevisto() {
        if (this.valida()) {
            EventoState s = new EventoRevistoState(evento);
            this.evento.setState(s);
        }
    }

    /**
     *metodo que modifica evento decidido
     */
    @Override
    public void setEventoDecidido() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica evento notificado
     */
    @Override
    public void setEventoNotificado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que modifica camera ready
     */
    @Override
    public void setCameraReady() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *metodo que valida
     * @return true ou false
     */
    @Override
    public boolean valida() {
        List<Submissao> ls = this.evento.getSubmissoes().getListaSubmissoes();
        for (Distribuicao d : this.evento.getProcessoDistribuicao().getListaDistribuicoes().getListaDitr()) {
            for (Submissao submissao : ls) {
                Artigo a = submissao.getArtigo();
                if (a == d.getArtigo()) {
                    if (!(submissao.getEstadoSubmissao() instanceof SubmissaoRevistaState)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    @Override
    public void setEventoTerminado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
