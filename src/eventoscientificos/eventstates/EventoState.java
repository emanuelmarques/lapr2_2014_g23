package eventoscientificos.eventstates;

/**
 *
 * @author Paulo Silva
 */
public interface EventoState {

    /**
     *metodo que valida
     * @return
     */
    public abstract boolean valida();
    
    /**
     *metodo que modifica evento criado por ficheiro csv
     */
    public abstract void setEventoCriadoPorFicheiroCSV();

    /**
     *metodo que modifica evento lido de ficheiro csv
     */
    public abstract void setEventoLidoDeFicheiroCSV();
    
    /**
     *metodo que modifica evento criado por ficheiro xml
     */
    public abstract void setEventoCriadoPorFicheiroXML();

    /**
     *metodo que modifica evento lido de ficheiro xml
     */
    public abstract void setEventoLidoDeFicheiroXML();

    /**
     *metodo que modifica evento criado
     */
    public abstract void setEventoCriado();

    /**
     *metodo que modifica evento registado
     */
    public abstract void setEventoRegistado();

    /**
     *metodo que modifica valores de registo definido
     */
    public abstract void setValoresDeRegistoDefinido();

    /**
     *metodo que modifica topicos criados
     */
    public abstract void setTopicosCriados();

    /**
     *metodo que modifica CP definida
     */
    public abstract void setCPDefinida();

    /**
     *metodo que modifica evento distribuido
     */
    public abstract void setEventoDistribuido();

    /**
     *metodo que modifica evento revisto
     */
    public abstract void setEventoRevisto();

    /**
     *metodo que modifica evento decidido
     */
    public abstract void setEventoDecidido();

    /**
     *metodo que modifica evento notificado
     */
    public abstract void setEventoNotificado();

    /**
     *metodo que modifica camera ready
     */
    public abstract void setCameraReady();
    
    /**
     *metodo que modifica terminado
     */
    public abstract void setEventoTerminado();
}
