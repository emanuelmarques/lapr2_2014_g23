/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.eventstates;

import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import eventoscientificos.submissaostates.SubmissaoAceiteState;
import eventoscientificos.submissaostates.SubmissaoRejeitadaState;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Vitor Moreira
 */
public class EventoDecididoState implements EventoState, Serializable {

    /**
     * variavel de instancia
     */
    private Evento evento;

    /**
     * construtor que recebe um evento como parametro
     *
     * @param e
     */
    public EventoDecididoState(Evento e) {
        this.evento = e;
    }

    /**
     * metodo que modifica evento criado por ficheiro csv
     */
    @Override
    public void setEventoCriadoPorFicheiroCSV() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * metodo que modifica evento lido por ficheiro csv
     */
    @Override
    public void setEventoLidoDeFicheiroCSV() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * metodo que modifica evento criado por ficheiro xml
     */
    @Override
    public void setEventoCriadoPorFicheiroXML() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * metodo que modifica evento lido por ficheiro xml
     */
    @Override
    public void setEventoLidoDeFicheiroXML() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * metodo que modifica evento criado
     */
    @Override
    public void setEventoCriado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * metodo que modifica evento registado
     */
    @Override
    public void setEventoRegistado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * metodo que modifica valores de registo definido
     */
    @Override
    public void setValoresDeRegistoDefinido() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * metodo que modifica topicos criados
     */
    @Override
    public void setTopicosCriados() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * metodo que modifica CP definida
     */
    @Override
    public void setCPDefinida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * metodo que modifica evento distribuido
     */
    @Override
    public void setEventoDistribuido() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * metodo que modifica evento revisto
     */
    @Override
    public void setEventoRevisto() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * metodo que modifica evento decidido
     */
    @Override
    public void setEventoDecidido() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * metodo que modifica evento notificado
     */
    @Override
    public void setEventoNotificado() {
        if (this.valida()) {
            EventoState s = new EventoNotificadoState(evento);
            this.evento.setState(s);
        }
    }

    /**
     * metodo que modifica camera ready
     */
    @Override
    public void setCameraReady() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * metodo que valida
     *
     * @return false ou true
     */
    @Override
    public boolean valida() {
        List<Submissao> ls = this.evento.getSubmissoes().getListaSubmissoes();
        for (Submissao s : ls) {
            if ((s.getEstadoSubmissao() instanceof SubmissaoAceiteState)
                    || (s.getEstadoSubmissao() instanceof SubmissaoRejeitadaState)) {
                if (s.getNotificacao() == null) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void setEventoTerminado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
