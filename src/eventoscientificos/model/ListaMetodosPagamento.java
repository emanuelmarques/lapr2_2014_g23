/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vitor_000
 */
public class ListaMetodosPagamento implements Serializable{

    private List<String> listaMetodosPagamento;

    public ListaMetodosPagamento() {
        this.listaMetodosPagamento = new ArrayList<>();
        this.listaMetodosPagamento.add("Visao Light");
        this.listaMetodosPagamento.add("Canada Express");
    }

    /**
     * @return the listaMetodosPagamento
     */
    public List<String> getListaMetodosPagamento() {
        return listaMetodosPagamento;
    }

    /**
     * @param listaMetodosPagamento the listaMetodosPagamento to set
     */
    public void setListaMetodosPagamento(List<String> listaMetodosPagamento) {
        this.listaMetodosPagamento = listaMetodosPagamento;
    }

    public void addMetodoPagamento(String metodo) {
        this.listaMetodosPagamento.add(metodo);
    }

}
