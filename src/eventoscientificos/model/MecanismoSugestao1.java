/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

/**
 *
 * @author emanuelmarques
 */
public class MecanismoSugestao1 implements MecanismoSugestao{
    public MecanismoSugestao1(){
        
    }

    @Override
    public String sugestao(Submissao sub) {
        int aceites=0;
        for (Revisao rev : sub.getListaRevisoes().getListaRevisoes()) {
            if( rev.getRecomendacao().equalsIgnoreCase("accept")||rev.getRecomendacao().equalsIgnoreCase("aceite")){
                aceites++;
            }
            
        }
        
        if ((float)aceites/sub.getListaRevisoes().getListaRevisoes().size()>0.5){
            return "Aceitar";
        }else{
            return "Rejeitar";
        }
        
        
    }
    
}
