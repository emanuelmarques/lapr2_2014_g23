/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vitor_000
 */
public class RegistaUtilizadores implements Serializable {

    private Empresa empresa;
    private List<Utilizador> listaUtilizadores;

    /**
     *
     * @param emp
     */
    public RegistaUtilizadores(Empresa emp) {
        this.listaUtilizadores=new ArrayList<>();

        this.empresa = emp;
    }

    /**
     *
     * @param id
     * @return
     */
    public Utilizador getUtilizador(String id) {
        for (Utilizador utilizador : this.getListaUtilizadores()) {
            if (utilizador.getUsername().equalsIgnoreCase(id)) {
                return utilizador;
            }
        }
        return null;
    }

    /**
     *
     * @param strEmail
     * @return
     */
    public Utilizador getUtilizadorEmail(String strEmail) {
        for (Utilizador u : this.getListaUtilizadores()) {
            
            String s1 = u.getEmail();
            if (s1.equalsIgnoreCase(strEmail)) {
                return u;
            }
        }

        return null;
    }

    /**
     *
     * @return
     */
    public List<Utilizador> getListaUtilizadores() {
        return this.listaUtilizadores;
    }

    /**
     *
     * @param listaUtilizadores
     */
    public void setListaUtilizadores(List<Utilizador> listaUtilizadores) {
        this.listaUtilizadores = listaUtilizadores;
    }

    /**
     *
     * @return
     */
    public Utilizador novoUtilizador() {
        return new Utilizador();
    }

    /**
     *
     * @param u
     * @return
     */
    public boolean registaUtilizador(Utilizador u) {

        if (u.valida() && validaUtilizador(u)) {
            addUtilizador(u);

            return true;
        } else {
            return false;
        }
    }

    // alterada na iteração 2

    /**
     *
     * @param u
     * @return
     */
        public boolean validaUtilizador(Utilizador u) {
        if (this.getListaUtilizadores().isEmpty()) {
            return true;
        } else {
            for (Utilizador uExistente : this.getListaUtilizadores()) {
                if (uExistente.mesmoQueUtilizador(u)) {
                    return false;
                }
            }
            return true;
        }
    }

    private boolean addUtilizador(Utilizador u) {

        return this.getListaUtilizadores().add(u);
    }

}
