/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

/**
 *
 * @author vitor_000
 */
public interface FormulasDePagamento {
    
    /**
     *
     * @return
     */
    public abstract float resultado();
    /**
     * @return the qntFullPapers
     */
    public abstract int getQntFullPapers();

    /**
     * @return the qntShortPapers
     */
    public abstract int getQntShortPapers();
    

    /**
     * @return the qntPosterPapers
     */
    public abstract int getQntPosterPapers();

    /**
     * @param qntFullPapers the qntFullPapers to set
     */
    public abstract void setQntFullPapers(int qntFullPapers);

    /**
     * @param qntShortPapers the qntShortPapers to set
     */
    public abstract void setQntShortPapers(int qntShortPapers);

    /**
     * @param qntPosterPapers the qntPosterPapers to set
     */
    public abstract void setQntPosterPapers(int qntPosterPapers);
}
