package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author vitor_000
 */
public class RegistaEventos implements Serializable {

    private List<Evento> listaEventos;
    private Empresa empresa;
    private HashMap<String, Evento> mapaEventos;

    /**
     *
     * @param emp
     */
    public RegistaEventos(Empresa emp) {
        this.listaEventos = new ArrayList<>();
        this.empresa = emp;
        this.mapaEventos = new HashMap<>();

    }

    /**
     *
     * @return
     */
    public List<Evento> getLe() {
        return this.listaEventos;
    }

    /**
     *
     * @param le
     */
    public void setLe(List<Evento> le) {
        this.listaEventos = le;
    }

    /**
     *
     * @return
     */
    public Evento novoEvento() {
        return new Evento();
    }

    /**
     *
     * @param e
     * @return
     */
    public boolean registaEvento(Evento e) {
        if (e.valida() && validaEvento(e)) {
            return addEvento(e);
        } else {
            return false;
        }
    }

    private boolean validaEvento(Evento e) {
        return !this.getLe().contains(e);
    }

    private boolean addEvento(Evento e) {
        return listaEventos.add(e);
    }

    /**
     *
     * @return
     */
    public HashMap<String, Evento> getMapaEventos() {
        return mapaEventos;
    }

    /**
     *
     * @param id
     * @return
     */
    public List<Evento> getListaEventosOrganizador(String id) {
        RegistaUtilizadores ru = this.empresa.getRegistaUtilizadores();
        Utilizador u = ru.getUtilizador(id);

        List<Evento> le = new ArrayList<>();
        System.out.println(listaEventos.size());       
        for (Evento evento : this.listaEventos) {

            List<Organizador> lo = evento.getOrganizadores().getListaOrganizadores();

            for (Organizador o : lo) {

                if (o.getUtilizador() == u) {
                    le.add(evento);

                }
            }
        }
        //System.out.println(le.size());
        return le;
    }

    /**
     *
     * @param id
     * @return
     */
    public List<Evento> getListaEventosRevisor(String id) {
        RegistaUtilizadores ru = this.empresa.getRegistaUtilizadores();
        Utilizador u = ru.getUtilizador(id);
        List<Evento> le = new ArrayList<>();
        for (Evento evento : this.listaEventos) {
            List<Revisor> lr = evento.getCP().getListaRevisores().getListaRevisores();
            for (Revisor revisor : lr) {
                if (revisor.getUtilizador() == u) {
                    le.add(evento);
                }
            }

        }
        return le;
    }

    public List<Evento> getListaEventosRevisorEmail(String email) {
        RegistaUtilizadores ru = this.empresa.getRegistaUtilizadores();

        Utilizador u = ru.getUtilizadorEmail(email);

        List<Evento> le = new ArrayList<>();
        for (Evento evento : this.listaEventos) {
            List<Revisor> lr = evento.getCP().getListaRevisores().getListaRevisores();

            for (Revisor revisor : lr) {
                if (revisor.getUtilizador() == u) {
                    le.add(evento);
                }

            }

        }
        return le;
    }

    /**
     *
     * @return
     */
    public List<Evento> getListaEventosPodeSubmeter() {
        List<Evento> le = new ArrayList<Evento>();

        for (Evento e : this.getLe()) {
            if (e.aceitaSubmissoes()) {
                le.add(e);
            }
        }

        return le;
    }

}
