package eventoscientificos.model;

import eventoscientificos.exceptions.ImpossivelDistribuirException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Paulo Silva
 */
public class MecanismoDistribuicao1 implements MecanismoDistribuicao, Serializable {

    /**
     * cria um mecanismo1
     */
    public MecanismoDistribuicao1() {

    }

    /**
     * distribui os artigos por revisores
     *
     * @param pd processo de distribuiçao a utilizar
     * @return lista de distribuiçao
     * @throws ImpossivelDistribuirException
     */
    @Override
    public List<Distribuicao> distribui(ProcessoDistribuicao pd)
            throws ImpossivelDistribuirException {

        List<Revisor> lr = pd.getListaRevisores();
        Random random = new Random();

        if (lr.size() >= 3) {

            List<Distribuicao> ld = new ArrayList<>();
            List<Submissao> ls = pd.getListaSubmissoes();

            for (Submissao s : ls) {
                Artigo a = s.getArtigo();
                System.out.println(a.getTitulo());
                if (verificaRevisores(lr, a)) {
                    Distribuicao d = pd.novaDistribuicao();
                    d.setArtigo(a);
                    while (d.getRevisores().size() < 3) {
                        int index = random.nextInt(lr.size());
                        Revisor r = lr.get(index);
                        if (!d.getRevisores().contains(r) && verificaAfinidade(a, r)) {
                            d.addRevisor(r);
                        }
                    }
                    ld.add(d);
                }
            }
            return ld;
        } else {
            throw new ImpossivelDistribuirException();
        }

    }

    /**
     * verifica a afinidade entre um revisor e um artigo
     *
     * @param a artigo a verificar
     * @param r revisor a verificar
     * @return true se tiver topicos de afinidade, false em caso contrario
     */
    public boolean verificaAfinidade(Artigo a, Revisor r) {

        for (Topico t : a.getM_listaTopicos().getListaTopicos()) {
            if (r.getM_listaTopicos().getListaTopicos().contains(t)) {
                return true;
            }
        }

        return false;
    }

    /**
     * verifica se existem pelo menos 3 revisores com afinidade
     *
     * @param lr lista de revisores
     * @param a artigo
     * @return true se existirem, false em caso contrario
     */
    public boolean verificaRevisores(List<Revisor> lr, Artigo a) {

        int cont = 0;
        for (Revisor r : lr) {
            for (Topico t : a.getM_listaTopicos().getListaTopicos()) {
                if (r.getM_listaTopicos().getListaTopicos().contains(t)) {
                    cont++;
                    break;
                }
            }
        }
        if (cont >= 3) {
            return true;
        } else {
            return false;
        }
    }
}
