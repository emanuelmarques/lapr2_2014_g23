/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

/**
 *
 * @author vitor_000
 */
public class ListaTopicos implements Serializable{
    
    private List<Topico> listaTopicos;

    /**
     *
     */
    public ListaTopicos() {
        this.listaTopicos = new ArrayList<>();
    }
    
    /**
     *
     * @return
     */
    public List<Topico> getListaTopicos() {
        return this.listaTopicos;
    }
    
    /**
     *
     * @param t
     * @return
     */
    public boolean addTopico(Topico t) {
        return this.listaTopicos.add(t);
    }
    
    /**
     *
     * @return
     */
    public Topico novoTopico() {
        return new Topico();
    }
    
    /**
     *
     * @param t
     * @return
     */
    public boolean validaTopico(Topico t) {
        if (t.valida() && this.valida(t)) {
            return true;
        } else {
            return false;
        }
        
    }
    
    /**
     *
     * @param t
     * @return
     */
    public boolean valida(Topico t) {
        if (this.listaTopicos.contains(t)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     *
     * @param listaTopicos
     */
    public void addAll(List<Topico> listaTopicos) {
        this.listaTopicos = listaTopicos;
        
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ListaTopicos other = (ListaTopicos) obj;
        if (!Objects.equals(this.listaTopicos, other.listaTopicos)) {
            return false;
        }
        return true;
    }
    
    
    
}
