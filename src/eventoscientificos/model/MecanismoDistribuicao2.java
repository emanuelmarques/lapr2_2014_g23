package eventoscientificos.model;

import java.util.ArrayList;
import java.util.List;
import eventoscientificos.exceptions.ImpossivelDistribuirException;
import java.io.Serializable;

/**
 *
 * @author Paulo Silva
 */
public class MecanismoDistribuicao2 implements MecanismoDistribuicao, Serializable {

    /**
     * cria um mecanismo2
     */
    public MecanismoDistribuicao2() {

    }

    /**
     * distribui os artigos por revisores
     *
     * @param pd processo de distribuiçao a utilizar
     * @return lista de distribuiçao
     * @throws ImpossivelDistribuirException
     */
    @Override
    public List<Distribuicao> distribui(ProcessoDistribuicao pd)
            throws ImpossivelDistribuirException {

        List<Revisor> lr = pd.getListaRevisores();

        if (lr.size() >= 2) {

            List<Distribuicao> ld = new ArrayList<>();
            List<Submissao> ls = pd.getListaSubmissoes();

            for (Submissao s : ls) {
                Artigo a = s.getArtigo();
                if (verifica(lr, a)) {

                    Distribuicao d = pd.novaDistribuicao();
                    d.setArtigo(a);
                    d.getRevisores().addAll(lr);

                    if (d.getRevisores().size() >= 2) {
                        ld.add(d);
                    }
                }
            }
            ;
            return ld;
        } else {
            throw new ImpossivelDistribuirException();
        }
    }

    /**
     * verifica se existe pelo menos um revisor com afinidade com o artigo
     *
     * @param lr lista de revisores
     * @param a artigo a testar
     * @return true se a condiçao se verificar, false em caso contrario
     */
    public boolean verifica(List<Revisor> lr, Artigo a) {

        for (Revisor r : lr) {
            for (Topico t : a.getM_listaTopicos().getListaTopicos()) {
                if (r.getM_listaTopicos().getListaTopicos().contains(t)) {
                    System.out.println("a");
                    return true;
                }
            }
        }
        return false;
    }

}
