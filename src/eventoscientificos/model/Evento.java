package eventoscientificos.model;

import eventoscientificos.eventstates.EventoCriadoPorFicheiroCSVState;
import eventoscientificos.eventstates.EventoCriadoPorFicheiroXMLState;
import eventoscientificos.eventstates.EventoCriadoState;
import eventoscientificos.eventstates.EventoState;
import eventoscientificos.eventstates.EventoTerminadoState;
import eventoscientificos.utils.Data;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Paulo Silva
 */
public class Evento implements Serializable {

    private String m_strTitulo;
    private String m_strDescricao;
    private Local m_local;
    private Data m_strDataInicio;
    private Data m_strDataFim;
    private Data m_strDataLimiteSubmissao;
    private ListaOrganizadores m_listaOrganizadores;
    private ListaSubmissoes m_listaSubmissoes;

    private CP m_cp;
    private ListaTopicos m_listaTopicos;
    private EventoState estadoEvento;
    private String website;

    private Data dataLimiteDeSubmissaoFinal;
    private Data dataLimiteDeRevisao;
    private Data dataLimiteDeRegistoDeAutores;
    private float precoFullPaper;
    private float precoShortPaper;
    private float precoPosterPaper;
    private FormulasDePagamento formulaDePagamento;
    private ProcessoDistribuicao processoDistribuicao;

    /**
     *
     */
    public Evento() {
        m_local = new Local();
        m_listaOrganizadores = new ListaOrganizadores();
        m_listaSubmissoes = new ListaSubmissoes();
        m_listaTopicos = new ListaTopicos();
        this.processoDistribuicao = new ProcessoDistribuicao();
        this.m_cp = new CP();
        this.m_strDataInicio = new Data();
        this.m_strDataFim = new Data();
        this.m_strDataLimiteSubmissao = new Data();
        this.dataLimiteDeSubmissaoFinal = new Data();
        this.dataLimiteDeRevisao = new Data();
        this.dataLimiteDeRegistoDeAutores = new Data();
        this.precoFullPaper = 0;
        this.precoShortPaper = 0;
        this.precoPosterPaper = 0;
    }

    /**
     *
     * @param titulo
     * @param descricao
     */
    public Evento(String titulo, String descricao) {
        m_local = new Local();
        m_listaOrganizadores = new ListaOrganizadores();
        m_listaSubmissoes = new ListaSubmissoes();
        m_listaTopicos = new ListaTopicos();
        this.setTitulo(titulo);
        this.setDescricao(descricao);
    }

    /**
     *
     * @return
     */
    public CP novaCP() {
        m_cp = new CP();

        return m_cp;
    }

    /**
     *
     * @param strTitulo
     */
    public final void setTitulo(String strTitulo) {
        this.m_strTitulo = strTitulo;
    }

    /**
     *
     * @param strDescricao
     */
    public final void setDescricao(String strDescricao) {
        this.m_strDescricao = strDescricao;
    }

    /**
     *
     * @param strDataInicio
     */
    public void setDataInicio(Data strDataInicio) {
        this.m_strDataInicio = strDataInicio;
    }

    /**
     *
     * @param strDataFim
     */
    public void setDataFim(Data strDataFim) {
        this.m_strDataFim = strDataFim;
    }

    // adicionada na iteração 2

    /**
     *
     * @param strDataLimiteSubmissão
     */
        public void setDataLimiteSubmissao(Data strDataLimiteSubmissão) {
        this.m_strDataLimiteSubmissao = strDataLimiteSubmissão;
    }

    /**
     *
     * @param strLocal
     */
    public void setLocal(String strLocal) {
        this.m_local.setLocal(strLocal);
    }

    /**
     *
     * @param website
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     *
     * @return
     */
    public String getTitulo() {
        return m_strTitulo;
    }

    /**
     *
     * @return
     */
    public EventoState getState() {
        return this.estadoEvento;
    }

    /**
     *
     * @return
     */
    public boolean valida() {
        if (this.m_strTitulo==null||this.m_strDescricao==null||this.m_listaOrganizadores.getListaOrganizadores()==null){
            return false;
        }
        if ((!this.m_strTitulo.equals(""))
                && (!this.m_strDescricao.equals(""))
                && this.m_local != null
                && this.m_strDataInicio != null
                && this.m_strDataFim != null
                && this.m_strDataLimiteSubmissao != null
                && !this.m_listaOrganizadores.getListaOrganizadores().isEmpty()) {
            
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param cp
     */
    public void setCP(CP cp) {
        m_cp = cp;
    }

    @Override
    public String toString() {
        return "Evento:" + "\nTítulo: " + this.m_strTitulo
                + "\nDescrição: " + this.m_strDescricao
                + "\nLocal: " + this.m_local
                + "\nData de início: " + this.m_strDataInicio
                + "\nData de fim: " + this.m_strDataFim
                + "\nWebsite: " + this.website
                + "\nData limite de submissão: " + this.m_strDataLimiteSubmissao
                + "\nEstado do evento: " + this.estadoEvento;
    }

    /**
     *
     * @return
     */
    public String eventoString() {
        return "Evento:"
                + "\nTítulo: " + this.m_strTitulo
                + "\nDescrição: " + this.m_strDescricao
                + "\nLocal: " + this.m_local
                + "\nData de início: " + this.m_strDataInicio
                + "\nData de fim: " + this.m_strDataFim
                + "\nData limite de submissão: " + this.m_strDataLimiteSubmissao
                + "\nOrganizadores: " + this.m_listaOrganizadores.getListaOrganizadores();
    }

    /**
     *
     * @return
     */
    public boolean aceitaSubmissoes() {
        if (Data.getDataAtual().isMaior(this.m_strDataLimiteSubmissao)){
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    public CP getCP() {
        return this.m_cp;
    }

    /**
     *
     * @return
     */
    public ListaOrganizadores getOrganizadores() {
        return this.m_listaOrganizadores;
    }

    /**
     *
     * @return
     */
    public ListaSubmissoes getSubmissoes() {
        return this.m_listaSubmissoes;
    }

    /**
     *
     * @return
     */
    public ListaTopicos getListaTopicos() {
        return this.m_listaTopicos;
    }

    /**
     *
     * @return
     */
    public ListaOrganizadores getListaOrganizadores() {
        return m_listaOrganizadores;
    }

    /**
     *
     * @param s
     */
    public void setState(EventoState s) {
        this.estadoEvento = s;
    }

    /**
     *
     */
    public void setEventoCriado() {
        this.estadoEvento = new EventoCriadoState(this);
    }

    /**
     *
     */
    public void setEventoRegistado() {
        this.estadoEvento.setEventoRegistado();
    }

    /**
     *
     */
    public void setTopicosCriados() {
        this.estadoEvento.setTopicosCriados();
    }

    /**
     *
     * @param cp
     */
    public void setCPDefinida(CP cp) {
        this.setCP(cp);
        this.estadoEvento.setCPDefinida();
    }

    /**
     *
     */
    public void setEventoLidoPorFicheiroCSV() {
        this.estadoEvento.setEventoLidoDeFicheiroCSV();
    }

    /**
     *
     */
    public void setEventoLidoPorFicheiroXML() {
        this.estadoEvento.setEventoLidoDeFicheiroXML();
    }

    /**
     *
     */
    public void setEventoCriadoPorCSV() {
        this.estadoEvento = new EventoCriadoPorFicheiroCSVState(this);
    }

    /**
     *
     * @param dataLimiteDeSubmissaoFinal
     */
    public void setDataLimiteDeSubmissaoFinal(Data dataLimiteDeSubmissaoFinal) {
        this.dataLimiteDeSubmissaoFinal = dataLimiteDeSubmissaoFinal;
    }

    /**
     *
     * @param dataLimiteDeRevisao
     */
    public void setDataLimiteDeRevisao(Data dataLimiteDeRevisao) {
        this.dataLimiteDeRevisao = dataLimiteDeRevisao;
    }

    /**
     *
     * @param dataLimiteDeRegistoDeAutores
     */
    public void setDataLimiteDeRegistoDeAutores(Data dataLimiteDeRegistoDeAutores) {
        this.dataLimiteDeRegistoDeAutores = dataLimiteDeRegistoDeAutores;
    }

    /**
     *
     * @return
     */
    public String getDescricao() {
        return this.m_strDescricao;
    }

    /**
     *
     * @return
     */
    public String getWebsite() {
        return this.website;
    }

    /**
     *
     * @return
     */
    public Data getDataInicio() {
        return this.m_strDataInicio;
    }

    /**
     *
     * @return
     */
    public String getLocal() {
        return this.m_local.getLocal();
    }

    /**
     *
     * @return
     */
    public Data getDataFim() {
        return this.m_strDataFim;
    }

    /**
     *
     * @return
     */
    public Data getDataLimSub() {
        return this.m_strDataLimiteSubmissao;
    }

    /**
     *
     * @return
     */
    public Data getDataLimSubFin() {
        return this.dataLimiteDeSubmissaoFinal;
    }

    /**
     *
     * @return
     */
    public Data getDataLimRev() {
        return this.dataLimiteDeRevisao;
    }

    /**
     *
     * @return
     */
    public Data getDataLimRegAut() {
        return this.dataLimiteDeRegistoDeAutores;
    }

    /**
     *
     */
    public void setEventoNotificado() {
        this.estadoEvento.setEventoNotificado();
    }

    /**
     *
     */
    public void setEventoCriadoPorXML() {
        this.estadoEvento = new EventoCriadoPorFicheiroXMLState(this);
    }

    /**
     *
     * @return
     */
    public float getPrecoFullPaper() {
        return this.precoFullPaper;
    }

    /**
     *
     * @return
     */
    public float getPrecoShortPaper() {
        return this.precoShortPaper;
    }

    /**
     *
     * @return
     */
    public float getPrecoPosterPaper() {
        return this.precoPosterPaper;
    }

    /**
     *
     * @param precoFullPaper
     */
    public void setPrecoFullPaper(float precoFullPaper) {
        this.precoFullPaper = precoFullPaper;
    }

    /**
     *
     * @param precoShortPaper
     */
    public void setPrecoShortPaper(float precoShortPaper) {
        this.precoShortPaper = precoShortPaper;
    }

    /**
     *
     * @param precoPosterPaper
     */
    public void setPrecoPosterPaper(float precoPosterPaper) {
        this.precoPosterPaper = precoPosterPaper;
    }

    /**
     *
     * @return
     */
    public FormulasDePagamento getFormulaDePagamento() {
        return formulaDePagamento;
    }

    /**
     *
     * @param formulaDePagamento
     */
    public void setFormulaDePagamento(FormulasDePagamento formulaDePagamento) {
        this.formulaDePagamento = formulaDePagamento;
    }

    /**
     *
     * @return
     */
    public String getValoresSring() {
        return "Preco por cada FullPaper = " + this.precoFullPaper
                + "\nPreco por cada ShortPaper = " + this.precoShortPaper
                + "\nPreco por cada PosterPaper = " + this.precoPosterPaper;
    }

    /**
     *
     */
    public void setValoresDeRegistoDefinido() {
        this.estadoEvento.setValoresDeRegistoDefinido();
    }

    /**
     *
     * @param mecanismoDistr
     * @return
     */
    public ProcessoDistribuicao novoProcessoDistribuicao(MecanismoDistribuicao mecanismoDistr) {
        return new ProcessoDistribuicao(this, mecanismoDistr);
    }

    /**
     *
     * @return
     */
    public ProcessoDistribuicao getProcessoDistribuicao() {
        return processoDistribuicao;
    }

    /**
     *
     * @param processoDistribuicao
     */
    public void setProcessoDistribuicao(ProcessoDistribuicao processoDistribuicao) {
        this.processoDistribuicao = processoDistribuicao;
    }

    /**
     *
     * @return
     */
    public List<Revisao> getRevisoesEvento() {
       List<Revisao>revisoesEvento=new ArrayList<>();
        for (Submissao sub : this.m_listaSubmissoes.getListaSubmissoes()) {
          
            for (Revisao rev : sub.getListaRevisoes().getListaRevisoes()) {
                revisoesEvento.add(rev);

            }
            
        }
        return revisoesEvento;
    }
    public List<Revisao> getRevisoesDoRevisor(Revisor revisor){
        List<Revisao> revisoes = new ArrayList<>();
        for (Submissao sub : this.getSubmissoes().getListaSubmissoes()) {
            revisoes.addAll(sub.getRevisoesDoRevisor(revisor));
            
        }
        return revisoes;
        
    }
    
    public void setEventoDecidido()  {
        this.estadoEvento.setEventoDecidido();
    }
    
    public void setEventoDistribuido(){
        this.estadoEvento.setEventoDistribuido();
    }
    public void setEventoRevisto() {
        this.estadoEvento.setEventoRevisto();
    }
    

    public void setEventoCameraReady() {
        this.estadoEvento.setCameraReady();
    }

    public void setEventoTerminado() {
        this.estadoEvento.setEventoTerminado();
    }
    
    public void criarEstadoEventoTerminado() {
        this.estadoEvento = new EventoTerminadoState(this);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Evento other = (Evento) obj;
        if (!Objects.equals(this.m_strTitulo, other.m_strTitulo)) {
            return false;
        }
        if (!Objects.equals(this.m_strDescricao, other.m_strDescricao)) {
            return false;
        }
        if (!Objects.equals(this.m_local, other.m_local)) {
            return false;
        }
        return true;
    }

    
    
    

    
}
