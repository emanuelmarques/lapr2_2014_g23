package eventoscientificos.model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Artigo implements Serializable {

    private String m_strTitulo;
    private String m_strResumo;
    private ListaAutores m_listaAutores;
    private Autor m_autorCorrespondente;
    private String m_strFicheiro;
    private ListaTopicos m_listaTopicos;
   
    private String tipoArtigo;
    
    /**
     *
     */
    public Artigo() {
        this.m_listaAutores = new ListaAutores();
        this.m_listaTopicos = new ListaTopicos();
    
    }

    /**
     *
     * @param strTitulo
     */
    public void setTitulo(String strTitulo) {
        this.m_strTitulo = strTitulo;
    }

    /**
     *
     * @return
     */
    public String getTitulo() {
        return this.m_strTitulo;
    }

    /**
     *
     * @param strResumo
     */
    public void setResumo(String strResumo) {
        this.m_strResumo = strResumo;
    }

    /**
     *
     * @return
     */
    public String getResumo() {
        return this.m_strResumo;
    }

    /**
     *
     * @return
     */
    public ListaAutores getListaAutores() {
        return m_listaAutores;
    }

    /**
     *
     * @param strNome
     * @param strAfiliacao
     * @return
     */
    /**
     *
     * @param autor
     */
    public void setAutorCorrespondente(Autor autor) {

        this.setM_autorCorrespondente(autor);

    }

    /**
     *
     * @param strFicheiro
     */
    public void setFicheiro(String strFicheiro) {
        this.setM_strFicheiro(strFicheiro);
    }

    /**
     *
     * @param listaTopicos
     *
     * adicionado na iteraçao 2
     */
    public void setListaTopicos(List<Topico> listaTopicos) {
        this.getM_listaTopicos().addAll(listaTopicos);
    }

    /**
     *
     * @return
     */
    public String getInfo() {
        return this.toString();
    }

    /**
     *
     * @return
     */
    public boolean valida() {
        return true;
    }

    @Override
    public String toString() {
        return this.m_strTitulo + "+ ...";
    }

    /**
     *
     * @param obj
     * @return
     *
     * Quando e que dois artigos sao iguais? Quando tem o mesmo titulo e mesmo
     * autor correspondente? Ou quando todos os autores forem tambem os mesmos?
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else {
            if (obj instanceof Artigo) {
                Artigo aux = (Artigo) obj;
                if(this.getM_autorCorrespondente()== null && aux.getM_autorCorrespondente()== null){
                    return this.m_strTitulo.equals(aux.m_strTitulo);
                }
                return this.getM_autorCorrespondente().equals(aux.getM_autorCorrespondente())
                        && this.m_strTitulo.equals(aux.m_strTitulo);
            } else {
                return false;
            }
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + (this.m_strTitulo != null ? this.m_strTitulo.hashCode() : 0);
        hash = 19 * hash + (this.m_strResumo != null ? this.m_strResumo.hashCode() : 0);
        return hash;
    }

    /**
     *
     * @return
     */
    public Autor getM_autorCorrespondente() {
        return m_autorCorrespondente;
    }

    /**
     *
     * @param m_autorCorrespondente
     */
    public void setM_autorCorrespondente(Autor m_autorCorrespondente) {
        this.m_autorCorrespondente = m_autorCorrespondente;
    }

    /**
     *
     * @return
     */
    public String getTipoArtigo() {
        return tipoArtigo;
    }

    /**
     *
     * @param tipoArtigo
     */
    public void setTipoArtigo(String tipoArtigo) {
        this.tipoArtigo = tipoArtigo;
    }

    /**
     *
     * @return
     */
    public ListaTopicos getM_listaTopicos() {
        return m_listaTopicos;
    }

    /**
     * @return the m_strFicheiro
     */
    public String getM_strFicheiro() {
        return m_strFicheiro;
    }

    /**
     * @param m_strFicheiro the m_strFicheiro to set
     */
    public void setM_strFicheiro(String m_strFicheiro) {
        this.m_strFicheiro = m_strFicheiro;
    }
}
