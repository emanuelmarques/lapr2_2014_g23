/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import eventoscientificos.submissaostates.SubmissaoCriadaState;
import eventoscientificos.submissaostates.SubmissaoRejeitadaState;
import eventoscientificos.submissaostates.SubmissaoState;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Submissao implements Serializable {

    private Artigo m_artigo;
    private SubmissaoState estadoSubmissao;
    private String sugestao;
    private Notificacao notificacao;
    private ListaRevisoes listaRevisoes;
    private boolean decisao;
    private boolean estadoPagamento;
    private Evento evento;

    /**
     *
     */
    public Submissao(Evento e) {
        this.notificacao = null;
        this.evento = e;
        this.estadoPagamento = false;
        this.listaRevisoes = new ListaRevisoes();
        this.estadoSubmissao = new SubmissaoCriadaState(this, this.evento);
        this.estadoPagamento = false;

    }

    /**
     *
     * @return
     */
    public Artigo novoArtigo() {
        return new Artigo();
    }

    /**
     *
     * @return
     */
    public String getInfo() {
        return this.toString();
    }

    /**
     *
     * @param artigo
     */
    public void setArtigo(Artigo artigo) {
        this.m_artigo = artigo;
    }

    /**
     *
     * @return
     */
    public Artigo getArtigo() {
        return this.m_artigo;
    }

    /**
     *
     * @return
     */
    public boolean valida() {
        if (this.m_artigo == null) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Submissão:\n" + this.m_artigo + "\nEstado da submissao:" + this.estadoSubmissao;
    }

    /**
     *
     * @param s
     */
    public void setState(SubmissaoState s) {
        this.estadoSubmissao = s;
    }

    /**
     *
     * @return
     */
    public SubmissaoState getEstadoSubmissao() {
        return estadoSubmissao;
    }

    /**
     *
     * @return
     */
    public String getSugestao() {
        return sugestao;
    }

    /**
     *
     * @param sugestao
     */
    public void setSugestao(String sugestao) {
        this.sugestao = sugestao;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {

            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Submissao other = (Submissao) obj;
        if (!Objects.equals(this.m_artigo, other.m_artigo)) {
            return false;
        }

        if (!Objects.equals(this.sugestao, other.sugestao)) {

            return false;
        }
        return true;
    }

    /**
     *
     */
    public void setSubmissaoAceite() {
        this.estadoSubmissao.setSubmissaoAceite();
    }

    /**
     *
     */
    public void setSubmissaoSubmetida() {
        this.estadoSubmissao.setArtigoParaRevisaoSubmetido();
    }

    /**
     *
     */
    public void setSubmissaoCriada() {
        this.estadoSubmissao= new SubmissaoCriadaState(this, evento);
    }

    /**
     *
     */
    public void setSubmissaoDistribuida() {
        this.estadoSubmissao.setSubmissaoDistribuida();
    }

    /**
     *
     */
    public void setSubmissaoReijeitada() {
        this.estadoSubmissao.setSubmissaoRejeitada();
    }

    /**
     *
     */
    public void setSubmissaoRevista() {
        this.estadoSubmissao.setSubmissaoRevista();
    }

    /**
     *
     * @return
     */
    public Notificacao getNotificacao() {
        return notificacao;
    }

    /**
     *
     * @param n
     */
    public void registaNotificacao(Notificacao n) {
        this.notificacao = n;

    }

    /**
     *
     * @return
     */
    public ListaRevisoes getListaRevisoes() {
        return this.listaRevisoes;
    }

    public float getMediaRevisoes() {
        int somaMedias = 0;
        for (Revisao rev : this.getListaRevisoes().getListaRevisoes()) {
            somaMedias += rev.getMediClassificaçoes();

        }
        return somaMedias / this.getListaRevisoes().getListaRevisoes().size();
    }

    /**
     *
     * @param listaRevisoes
     */
    public void setListaRevisoes(ListaRevisoes listaRevisoes) {
        this.listaRevisoes = listaRevisoes;
    }

    /**
     *
     * @param r
     */
    public void registaRevisao(Revisao r) {
        this.listaRevisoes.addRevisao(r);
    }

    public List<Revisao> getRevisoesDoRevisor(Revisor revisor) {
        List<Revisao> revisoes = new ArrayList<>();
        for (Revisao rev : this.getListaRevisoes().getListaRevisoes()) {
            if (rev.getRevisor().equals(revisor)) {
                revisoes.add(rev);

            }

        }
        return revisoes;

    }

    public void setSubmissaoPaga() {
        this.estadoSubmissao.setSubmissaoPaga();
    }

    public void setDecisaoAceite() {
        this.decisao = true;
    }

    public void setDecisaoRejeitada() {
        this.decisao = false;
    }

    public boolean getDecisao() {
        return this.decisao;
    }

    /**
     * @return true se foi paga, false se nao foi efetuado pagamento
     */
    public boolean getEstadoPagamento() {
        return estadoPagamento;
    }

    /**
     * define que a submissao esta paga
     */
    public void setPagamentoEfetuado() {
        this.estadoPagamento = true;
    }

    public void criarSubmissaoRejeitada() {
        this.estadoSubmissao = new SubmissaoRejeitadaState(this);
    }

}
