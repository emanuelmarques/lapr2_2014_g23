/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import pt.ipp.isep.dei.eapli.visaolight.VisaoLight;

/**
 *
 * @author vitor_000
 */
public class VisaoLightPagamento implements MetodosDePagamento {

    private String numCC;
    private String dtValidadeCC;
    private String dtLimiteAutorizacao;
    private float valorAutorizacao;

    public VisaoLightPagamento() {
        this.numCC = "";
        this.dtValidadeCC = "";
        this.dtLimiteAutorizacao = "";
        this.valorAutorizacao = 0;
    }

    public String getAutorizacaoDCC() {
        return VisaoLight.getAutorizacaoDCC(this.getNumCC(), this.getDtValidadeCC(), this.getValorAutorizacao(), this.getDtLimiteAutorizacao());
    }

    /**
     * @return the numCC
     */
    @Override
    public String getNumCC() {
        return numCC;
    }

    /**
     * @return the dtValidadeCC
     */
    @Override
    public String getDtValidadeCC() {
        return dtValidadeCC;
    }

    /**
     * @return the dtLimiteAutorizacao
     */
    @Override
    public String getDtLimiteAutorizacao() {
        return dtLimiteAutorizacao;
    }

    /**
     * @return the valorAutorizacao
     */
    @Override
    public float getValorAutorizacao() {
        return valorAutorizacao;
    }

    /**
     * @param numCC the numCC to set
     */
    @Override
    public void setNumCC(String numCC) {
        this.numCC = numCC;
    }

    /**
     * @param dtValidadeCC the dtValidadeCC to set
     */
    @Override
    public void setDtValidadeCC(String dtValidadeCC) {
        this.dtValidadeCC = dtValidadeCC;
    }

    /**
     * @param dtLimiteAutorizacao the dtLimiteAutorizacao to set
     */
    @Override
    public void setDtLimiteAutorizacao(String dtLimiteAutorizacao) {
        this.dtLimiteAutorizacao = dtLimiteAutorizacao;
    }

    /**
     * @param valorAutorizacao the valorAutorizacao to set
     */
    @Override
    public void setValorAutorizacao(float valorAutorizacao) {
        this.valorAutorizacao = valorAutorizacao;
    }
}
