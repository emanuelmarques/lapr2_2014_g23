/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vitor_000
 */
public class ListaRevisoes implements Serializable{

    private List<Revisao> listaRevisoes;

    /**
     *
     */
    public ListaRevisoes() {
        this.listaRevisoes = new ArrayList<>();
    }

    /**
     *
     * @param listaRevisoes
     */
    public void setListaRevisoes(List<Revisao> listaRevisoes) {
        this.listaRevisoes = listaRevisoes;
    }

    /**
     *
     * @return
     */
    public Revisao novaRevisao() {
        return new Revisao();
    }

    /**
     *
     * @param rev
     */
    public void addRevisao(Revisao rev) {
        this.listaRevisoes.add(rev);
    }

    /**
     *
     * @return
     */
    public List<Revisao> getListaRevisoes() {
        return listaRevisoes;
    }

    /**
     *
     * @param rev
     * @return
     */
    public boolean valida(Revisao rev) {
        return !this.listaRevisoes.contains(rev);
    }

    
}
