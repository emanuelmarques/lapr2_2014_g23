package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vitor Moreira
 */
public class ListaRevisores implements Serializable {
 
    private List<Revisor> listaRevisores;

    /**
     *
     */
    public ListaRevisores() {
        this.listaRevisores = new ArrayList<>();
    }
    
    /**
     * @return the listaRevisores
     */
    public List<Revisor> getListaRevisores() {
        return this.listaRevisores;
    }

    /**
     * @param listaRevisores the listaRevisores to set
     */
    public void setListaRevisores(List<Revisor> listaRevisores) {
        this.listaRevisores = listaRevisores;
    }

    /**
     *
     * @param r
     * @return
     */
    public boolean addMembroCP(Revisor r) {
        return this.listaRevisores.add(r);
    }

    
    
    
}
