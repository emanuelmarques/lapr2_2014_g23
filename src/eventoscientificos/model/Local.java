/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author Nuno Silva
 */
public class Local implements Serializable {

    private String m_strLocal;

    /**
     *
     */
    public Local() {
    }

    /**
     *
     * @return
     */
    public String getLocal() {
        return this.m_strLocal;
    }
    
    /**
     *
     * @param strLocal
     */
    public void setLocal(String strLocal) {
        m_strLocal = strLocal;
    }

    @Override
    public String toString() {
        return m_strLocal;
    }
}
