/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vitor_000
 */
public class ListaDistribuicoes implements Serializable{
    
    private List<Distribuicao> listaDitr;

    /**
     *
     */
    public ListaDistribuicoes() {
        this.listaDitr = new ArrayList<>();
    }

    /**
     *
     * @return
     */
    public List<Distribuicao> getListaDitr() {
        return listaDitr;
    }

    /**
     *
     * @param listaDitr
     */
    public void setListaDitr(List<Distribuicao> listaDitr) {
        this.listaDitr = listaDitr;
    }
    
    
}
