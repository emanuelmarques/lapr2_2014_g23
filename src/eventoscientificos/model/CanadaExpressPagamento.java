/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import pt.ipp.isep.dei.eapli.canadaexpress.*;

/**
 *
 * @author vitor_000
 */
public class CanadaExpressPagamento implements MetodosDePagamento {

    private String dtValidadeCC;
    private String numCC;
    private float valorAutorizacao;
    private String dtLimiteAutorizacao;
    private CanadaExpress canadaE;

    public CanadaExpressPagamento() {
        this.dtValidadeCC = "";
        this.numCC = "";
        this.valorAutorizacao = 0;
        this.dtLimiteAutorizacao = "";
        this.canadaE = new CanadaExpress();
    }

    /**
     * @return the dataValidade
     */
    @Override
    public String getDtValidadeCC() {
        return dtValidadeCC;
    }

    /**
     * @return the numCC
     */
    @Override
    public String getNumCC() {
        return numCC;
    }

    /**
     * @return the fValorADCC
     */
    @Override
    public float getValorAutorizacao() {
        return valorAutorizacao;
    }

    /**
     * @return the dataLimite
     */
    @Override
    public String getDtLimiteAutorizacao() {
        return dtLimiteAutorizacao;
    }

    /**
     * @return the canadaE
     */
    public CanadaExpress getCanadaE() {
        return canadaE;
    }

    /**
     * @param dataValidade the dataValidade to set
     */
    @Override
    public void setDtValidadeCC(String dataValidade) {
        this.dtValidadeCC = dataValidade;
    }

    /**
     * @param numCC the numCC to set
     */
    @Override
    public void setNumCC(String numCC) {
        this.numCC = numCC;
    }

    /**
     * @param fValorADCC the fValorADCC to set
     */
    @Override
    public void setValorAutorizacao(float fValorADCC) {
        this.valorAutorizacao = fValorADCC;
    }

    /**
     * @param dataLimite the dataLimite to set
     */
    @Override
    public void setDtLimiteAutorizacao(String dataLimite) {
        this.dtLimiteAutorizacao = dataLimite;
    }

    /**
     * @param canadaE the canadaE to set
     */
    public void setCanadaE(CanadaExpress canadaE) {
        this.canadaE = canadaE;
    }

    public boolean efetuarPagamento() throws ParseException {
        if (!this.canadaE.Init("#CANADA#EXPRESS#EAPLI#")) {
            return false;
        }
        
        DateFormat formatter;
        formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date dataValidadeCC = (Date) formatter.parse(this.dtValidadeCC);
        Date dataLimiteRegisto = (Date) formatter.parse(this.dtLimiteAutorizacao);
        
        
        Pedido p = new Pedido(dataValidadeCC, this.numCC, this.valorAutorizacao, dataLimiteRegisto);
        return !this.canadaE.ValidaPedido(p).equalsIgnoreCase("**invalido**");

    }

    public boolean finish() {
        return this.canadaE.Finish();
    }

}
