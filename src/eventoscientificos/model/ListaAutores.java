

package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Silva
 */
public class ListaAutores implements Serializable {
    List<Autor> m_listaAutores;

    /**
     *
     */
    public ListaAutores() {
        this.m_listaAutores = new ArrayList<>();
    }

    /**
     *
     * @param strNome
     * @param strAfiliacao
     * @return
     */
    public Autor novoAutor(String strNome, String strAfiliacao) {
        Autor autor = new Autor();

        autor.setNome(strNome);
        autor.setAfiliacao(strAfiliacao);

        return autor;
    }

    /**
     *
     * @param strNome
     * @param strAfiliacao
     * @param strEmail
     * @param utilizador
     * @return
     */
    public Autor novoAutor(String strNome, String strAfiliacao, String strEmail, Utilizador utilizador) {
        Autor autor = new Autor();

        autor.setNome(strNome);
        autor.setAfiliacao(strAfiliacao);
        autor.setEMail(strEmail);
        autor.setUtilizador(utilizador);

        return autor;
    }

    /**
     *
     * @param autor
     * @return
     */
    public boolean addAutor(Autor autor) {
        if (validaAutor(autor)) {
            return m_listaAutores.add(autor);
        } else {
            return false;
        }

    }

    /**
     *
     * @return
     */
    public List<Autor> getListaAutores() {
        return this.m_listaAutores;
    }
    

    /**
     *
     * @param autor
     * @return
     */
    private boolean validaAutor(Autor autor) {
        return autor.valida();
    }

    /**
     *
     * @return
     */
    public List<Autor> getPossiveisAutoresCorrespondentes() {
        List<Autor> la = new ArrayList<Autor>();

        for (Autor autor : this.m_listaAutores) {
            if (autor.podeSerCorrespondente()) {
                la.add(autor);
            }
        }
        return la;
    }

    public boolean add(Autor autor) {
        return this.m_listaAutores.add(autor);
    }
    
}
