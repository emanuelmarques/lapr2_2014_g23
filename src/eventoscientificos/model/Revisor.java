/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Nuno Silva
 */
public class Revisor implements Serializable {

    private String m_strNome;
    private Utilizador m_utilizador;

    // adicionada na iteração 1
    private ListaTopicos m_listaTopicos;

    /**
     *
     * @param u
     */
    public Revisor(Utilizador u) {
        m_strNome = u.getNome();
        m_utilizador = u;
        this.m_listaTopicos = new ListaTopicos();
    }

    // adicionada na iteração 2

    /**
     *
     * @param listaTopicos
     */
        public void setListaTopicos(List<Topico> listaTopicos) {
        getM_listaTopicos().addAll(listaTopicos);
    }

    /**
     *
     * @return
     */
    public boolean valida() {
        if (this.m_utilizador==null){
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    public String getNome() {
        return m_strNome;
    }

    /**
     *
     * @return
     */
    public Utilizador getUtilizador() {
        return m_utilizador;
    }

    @Override
    public String toString() {
        String strRevisor = m_utilizador.toString() + ": ";

        String strTopicos = "";
        for (Topico t : this.getM_listaTopicos().getListaTopicos()) {
            strTopicos += t.toString();
        }

        strRevisor += strTopicos;

        return strRevisor;
    }

    /**
     *
     * @param t
     */
    public void addTopicoPericia(Topico t) {
        this.getM_listaTopicos().addTopico(t);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Revisor other = (Revisor) obj;
        if (!Objects.equals(this.m_strNome, other.m_strNome)) {
            return false;
        }

        if (!Objects.equals(this.m_utilizador, other.m_utilizador)) {
           
            return false;

        }
        if (!Objects.equals(this.m_utilizador.getEmail(), other.m_utilizador.getEmail())) {
           
            return false;

        }
        
        if (!Objects.equals(this.m_listaTopicos, other.m_listaTopicos)) {
            
            return false;
        }
       
        return true;
    }

    /**
     *
     * @return
     */
    public ListaTopicos getM_listaTopicos() {
        return m_listaTopicos;
    }

}
