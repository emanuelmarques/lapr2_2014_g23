/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Autor implements Serializable {

    private Utilizador m_Utilizador;
    private String m_strNome;
    private String m_strAfiliacao;
    private String m_strEMail;

    /**
     *
     */
    public Autor() {

    }

    /**
     *
     * @param strNome
     */
    public void setNome(String strNome) {
        
        this.m_strNome=strNome;

    }

    /**
     *
     * @param strAfiliacao
     */
    public void setAfiliacao(String strAfiliacao) {
        this.m_strAfiliacao = strAfiliacao;
    }

    /**
     *
     * @param strEMail
     */
    public void setEMail(String strEMail) {
        this.m_strEMail = strEMail;
    }

    /**
     *
     * @param utilizador
     */
    public void setUtilizador(Utilizador utilizador) {
        this.m_Utilizador = utilizador;
    }

    /**
     *
     * @return
     */
    public Utilizador getUtilizador() {
        return m_Utilizador;
    }

    /**
     *
     * @return
     */
    public String getNome() {
        return m_strNome;
    }

    /**
     *
     * @return
     */
    public String getAfiliacao() {
        return m_strAfiliacao;
    }

    /**
     *
     * @return
     */
    public String getEMail() {
        return m_strEMail;
    }

    /**
     *
     * @return
     */
    public boolean valida() {
        if(this.m_Utilizador==null||this.m_strAfiliacao==null||this.m_strEMail==null||this.m_strNome==null){
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    public boolean podeSerCorrespondente() {
        return (m_Utilizador != null);
    }

    @Override
    public String toString() {
        return this.m_strNome + " - " + this.m_strAfiliacao + " - " + this.m_strEMail;
    }

    /**
     *
     * @param obj
     * @return
     *
     * Um Autor é identificado pelo seu endereço de e-mail que deve ser único no
     * artigo, mas não tem que corresponder a um Utilizador.
     */
    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        } else if (obj instanceof Autor) {
            Autor aux = (Autor) obj;
            return this.m_strEMail.equals(aux.m_strEMail);
        } else if(this==null && obj==null) {
            return true;
        }else{
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.m_Utilizador != null ? this.m_Utilizador.hashCode() : 0);
        hash = 41 * hash + (this.m_strEMail != null ? this.m_strEMail.hashCode() : 0);
        return hash;
    }
}
