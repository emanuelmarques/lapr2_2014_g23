/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author Nuno Silva
 */
public class Organizador implements Serializable {

    private final String m_strNome;
    private Utilizador m_utilizador;

    /**
     *
     * @param u
     */
    public Organizador(Utilizador u) {
        m_strNome = u.getNome();
        this.setUtilizador(u);
    }

    private void setUtilizador(Utilizador u) {
        m_utilizador = u;
    }

    /**
     *
     * @return
     */
    public boolean valida() {
        if (this.m_utilizador==null){
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    public String getNome() {
        return m_strNome;
    }

    /**
     *
     * @return
     */
    public Utilizador getUtilizador() {
        return m_utilizador;
    }

    @Override
    public String toString() {
        return m_utilizador.toString();
    }
}
