/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vitor_000
 */
public class ListaMecanismosDistribuicao implements Serializable{
    
    private List<MecanismoDistribuicao> listaMecanismos;

    /**
     *
     */
    public ListaMecanismosDistribuicao() {
        this.listaMecanismos = new ArrayList<>();
    }

    /**
     *
     * @return
     */
    public List<MecanismoDistribuicao> getListaMecanismos() {
        return this.listaMecanismos;
    }

    /**
     *
     * @param listaMecanismos
     */
    public void setListaMecanismos(List<MecanismoDistribuicao> listaMecanismos) {
        this.listaMecanismos = listaMecanismos;
    }
    
    /**
     *
     * @param mec
     */
    public void add(MecanismoDistribuicao mec) {
        this.listaMecanismos.add(mec);
    }
    
    
}
