/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;


/**
 *
 * @author vitor_000
 */
public interface MetodosDePagamento {
    
    /**
     * @return the numCC
     */
    public abstract String getNumCC();

    /**
     * @return the dtValidadeCC
     */
    public abstract String getDtValidadeCC();

    /**
     * @return the dtLimiteAutorizacao
     */
    public abstract String getDtLimiteAutorizacao();

    /**
     * @return the valorAutorizacao
     */
    public abstract float getValorAutorizacao();

    /**
     * @param numCC the numCC to set
     */
    public abstract void setNumCC(String numCC);
    

    /**
     * @param dtValidadeCC the dtValidadeCC to set
     */
    public abstract void setDtValidadeCC(String dtValidadeCC);

    /**
     * @param dtLimiteAutorizacao the dtLimiteAutorizacao to set
     */
    public abstract void setDtLimiteAutorizacao(String dtLimiteAutorizacao);

    /**
     * @param valorAutorizacao the valorAutorizacao to set
     */
    public abstract void setValorAutorizacao(float valorAutorizacao);
}