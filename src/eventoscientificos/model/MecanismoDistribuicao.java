package eventoscientificos.model;

import java.util.List;
import eventoscientificos.exceptions.ImpossivelDistribuirException;

/**
 *
 * @author Paulo Silva
 */
public interface MecanismoDistribuicao {

    /**
     * distribui artigos pelos revisores
     *
     * @param pd processo de distribuiçao a utilizar
     * @return lista de distribuiçao
     * @throws ImpossivelDistribuirException
     */
    public List<Distribuicao> distribui(ProcessoDistribuicao pd)
            throws ImpossivelDistribuirException;
}
