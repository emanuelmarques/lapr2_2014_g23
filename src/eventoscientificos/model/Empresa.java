package eventoscientificos.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Nuno Silva
 */
public class Empresa implements Serializable {

    private RegistaEventos re;

    private RegistaUtilizadores ru;
    private Submissao la;
    private ListaMecanismosDistribuicao listaMecanismosDistribuicao;
    private ListaMetodosPagamento listaMetodosPagamento;
    private ListaTopicos listaTopicosACM;

    /**
     *
     */
    public Empresa() {

        this.re = new RegistaEventos(this);
        this.ru = new RegistaUtilizadores(this);
        this.listaTopicosACM = carregarTopicosFicheiro();
        this.listaMetodosPagamento=new ListaMetodosPagamento();

    }

    /**
     *
     * @return
     */
    public RegistaEventos getRegistaEventos() {
        return this.re;
    }

    /**
     *
     * @return
     */
    public RegistaUtilizadores getRegistaUtilizadores() {
        return this.ru;
    }

    /**
     *
     * @param strId
     * @return
     */
    public List<Evento> getEventosOrganizador(String strId) {
        return getRegistaEventos().getListaEventosOrganizador(strId);

    }

   

    /**
     *
     * @param ru
     */
    public void setRu(RegistaUtilizadores ru) {
        this.ru = ru;
    }

   
    /**
     *
     * @return
     */
    public ListaMecanismosDistribuicao getListaMecanismosDistribuicao() {
        this.listaMecanismosDistribuicao = new ListaMecanismosDistribuicao();
        MecanismoDistribuicao1 mec1 = new MecanismoDistribuicao1();
        MecanismoDistribuicao2 mec2 = new MecanismoDistribuicao2();
        this.listaMecanismosDistribuicao.add(mec1);
        this.listaMecanismosDistribuicao.add(mec2);

        return this.listaMecanismosDistribuicao;
    }

    /**
     *
     * @param listaMecanismosDistribuicao
     */
    public void setListaMecanismosDistribuicao(ListaMecanismosDistribuicao listaMecanismosDistribuicao) {
        this.listaMecanismosDistribuicao = listaMecanismosDistribuicao;
    }

    /**
     * Metodo para carregar topicos ACM a partir de um ficheiro presente no
     * diretorio do projeto.
     *
     * @return uma instancia lista de topicos com todos os topicos lidos.
     */
    private ListaTopicos carregarTopicosFicheiro() {

        try {
            Scanner input = new Scanner(new File("codigosacm.txt"));
            ListaTopicos lt = new ListaTopicos();
            while (input.hasNext()) {
                String[] split = input.nextLine().split(" - ");
                lt.getListaTopicos().add(new Topico(split[1], split[0]));
            }

            input.close();
            return lt;
        } catch (FileNotFoundException ex) {
            // Logger.getLogger(Empresa.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Impossivel carregar topicos" + ex, "Erro ao carregar topicos", JOptionPane.ERROR_MESSAGE);
            return new ListaTopicos();
        }
    }

    /**
     * retorna a lista de topicos da empresa
     *
     * @return a listaTopicosACM da empresa
     */
    public ListaTopicos getListaTopicosACM() {
        return this.listaTopicosACM;
    }

    /**
     * Altera a lista de topicos da empresa para uma nova passada por parametro
     *
     * @param listaTopicosACM a nova listaTopicos para se alterar
     */
    public void setListaTopicosACM(ListaTopicos listaTopicosACM) {
        this.listaTopicosACM = listaTopicosACM;
    }

    public List<Revisao> getRevisoesDoRevisor(Revisor revisor) {
           
        List<Revisao> revisoes = new ArrayList<>();
        for (Evento sub : this.getRegistaEventos().getListaEventosRevisorEmail(revisor.getUtilizador().getEmail())) {
        
            for (Submissao submissao : sub.getSubmissoes().getListaSubmissoes()) {
           
                for (Revisao revisao : submissao.getListaRevisoes().getListaRevisoes()) {
             
                    if (revisao.getRevisor()==revisor) {
                        revisoes.add(revisao);

                    }
                     

                }
        

            }

        }
        return revisoes;

    }

    /**
     * @return the listaMetodosPagamento
     */
    public ListaMetodosPagamento getListaMetodosPagamento() {

        

        return listaMetodosPagamento;
    }

    /**
     * @param listaMetodosPagamento the listaMetodosPagamento to set
     */
    public void setListaMetodosPagamento(ListaMetodosPagamento listaMetodosPagamento) {
        this.listaMetodosPagamento = listaMetodosPagamento;

    }
}
