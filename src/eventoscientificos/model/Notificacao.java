

package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author Paulo Silva
 */
public class Notificacao implements Serializable{
    
    private String m_notificacao;
    
    /**
     *
     */
    public Notificacao(){
        m_notificacao="criada";
    }
    
    
    /**
     *
     * @return
     */
    public String getM_notificacao() {
        return m_notificacao;
    }

    /**
     *
     * @param m_notificacao
     */
    public void setM_notificacao(String m_notificacao) {
        this.m_notificacao = m_notificacao;
    }
    
}
