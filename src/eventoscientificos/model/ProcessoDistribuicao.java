package eventoscientificos.model;

import java.io.Serializable;
import java.util.List;
import eventoscientificos.exceptions.ImpossivelDistribuirException;

/**
 *
 * @author Paulo Silva
 */
public class ProcessoDistribuicao implements Serializable {

    /**
     * mecanismo de distribuiçao escolhido
     */
    private MecanismoDistribuicao mecanismo;
    /**
     * lista de distribuiçao do processo
     */
    private ListaDistribuicoes ld;
    /**
     * evento do processo de distribuiçao
     */
    private Evento evento;

    /**
     * cria um processo de distribuiçao recebendo por parametro o evento e
     * mecanismo escolhidos
     *
     * @param e evento escolhido
     * @param m mecanismo escolhido
     */
    public ProcessoDistribuicao(Evento e, MecanismoDistribuicao m) {
        this.evento = e;
        this.mecanismo = m;
        this.ld = new ListaDistribuicoes();
    }

    /**
     * cria um processo de distribuiçao por defeito
     *
     */
    public ProcessoDistribuicao() {
        this.ld = new ListaDistribuicoes();
    }

    /**
     * retorna o evento do processo de distribuiçao
     *
     * @return evento do PD
     */
    public Evento getEvento() {
        return this.evento;
    }

    /**
     * retorna a lista de distribuiçoes
     *
     * @return lista de distribuiçoes
     */
    public ListaDistribuicoes getListaDistribuicoes() {
        return this.ld;
    }

    /**
     * atribui um mecanismo de distribuiçao
     *
     * @param m mecanismo a atribuir
     */
    public void setMecanismoDistribuicao(MecanismoDistribuicao m) {
        this.mecanismo = m;
    }

    /**
     * atribui uma lista de distribuiçao
     *
     * @param ld lista de distribuiçao a atribuir
     */
    public void setListaDistribuicoes(ListaDistribuicoes ld) {
        this.ld = ld;
    }

    /**
     * distribui os artigos pelos revisores
     *
     * @return lista de distribuiçao
     * @throws ImpossivelDistribuirException
     */
    public List<Distribuicao> distribui() throws ImpossivelDistribuirException {

        this.ld.setListaDitr(this.mecanismo.distribui(this));
        return this.ld.getListaDitr();
    }

    /**
     * retorna a lista de revisores do evento
     *
     * @return lista de revisores do evento
     */
    public List<Revisor> getListaRevisores() {
        return this.evento.getCP().getListaRevisores().getListaRevisores();
    }

    /**
     * retorna a lista de submissoes do evento
     *
     * @return lista de submissoes do evento
     */
    public List<Submissao> getListaSubmissoes() {
        return this.evento.getSubmissoes().getListaSubmissoes();
    }

    /**
     * cria e retorna uma nova distribuiçao
     *
     * @return distribuiçao criada
     */
    public Distribuicao novaDistribuicao() {
        Distribuicao d = new Distribuicao();
        return d;
    }

}
