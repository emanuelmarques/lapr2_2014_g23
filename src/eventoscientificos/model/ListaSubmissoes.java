/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author vitor_000
 */
public class ListaSubmissoes implements Serializable{
    
    private List<Submissao> listaSubmissoes;
    private HashMap<String, Submissao> mapaDeSubmissoes;

    /**
     *
     */
    public ListaSubmissoes() {
        this.listaSubmissoes = new ArrayList<>();
        this.mapaDeSubmissoes=new HashMap<>();
    }
    
    /**
     *
     * @return
     */
    public Submissao novaSubmissao(Evento evento) {
        return new Submissao( evento);
    }
    
    /**
     *
     * @param s
     * @return
     */
    public boolean addSubmissao(Submissao s) {
        return this.listaSubmissoes.add(s);
    }
    
    /**
     *
     * @return
     */
    public List<Submissao> getListaSubmissoes() {
        

        return this.listaSubmissoes;
    }

    /**
     *
     * @return
     */
    public HashMap<String, Submissao> getMapaDeSubmissoes() {
        return mapaDeSubmissoes;
    }
    
}
