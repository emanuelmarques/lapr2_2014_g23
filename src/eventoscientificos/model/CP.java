package eventoscientificos.model;

import eventoscientificos.eventstates.*;
import java.io.Serializable;
import java.util.*;

/**
 *
 * @author Paulo Silva
 */
public class CP implements Serializable {

    private CPDefinidaState estadoCP;

    ListaRevisores m_listaRevisores;

    /**
     *
     */
    public CP() {
        m_listaRevisores = new ListaRevisores();
    }

    /**
     *
     * @param strId
     * @param u
     * @return
     */
    public Revisor addMembroCP(String strId, Utilizador u) {
        Revisor r = new Revisor(u);

        if (r.valida() && validaMembroCP(r)) {
            return r;
        } else {
            return null;
        }
    }

    private boolean validaMembroCP(Revisor r) {
        return true;
    }

    /**
     *
     * @param r
     * @return
     */
    public boolean registaMembroCP(Revisor r) {
        return m_listaRevisores.addMembroCP(r);
    }

    @Override
    public String toString() {
        String strCP = "Membros de CP: ";
        for (ListIterator<Revisor> it = m_listaRevisores.getListaRevisores().listIterator(); it.hasNext();) {
            strCP += it.next().toString();
            if (it.hasNext()) {
                strCP += ", ";
            }
        }
        return strCP;
    }

    /**
     *
     * @return
     */
    public String cpString() {
        return "CP:"
                + "\nRevisores/Membros da CP: " + this.m_listaRevisores;
    }

    /**
     *
     * @return
     */
    public ListaRevisores getListaRevisores() {
        return this.m_listaRevisores;
    }

}
