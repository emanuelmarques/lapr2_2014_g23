/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author iazevedo
 */
public class Topico implements Serializable {

    private String m_strDescricao;

    private String m_strCodigoACM;

    /**
     *
     */
    public Topico() {
    }

    public Topico(String m_strDescricao, String m_strCodigoACM) {
        this.m_strDescricao = m_strDescricao;
        this.m_strCodigoACM = m_strCodigoACM;
    }
    
    
    
    /**
     *
     * @param strDescricao
     */
    public void setDescricao(String strDescricao) {
        this.m_strDescricao = strDescricao;
    }

    /**
     *
     * @param codigoACM
     */
    public void setCodigoACM(String codigoACM) {
        this.m_strCodigoACM = codigoACM;
    }

    /**
     *
     * @return
     */
    public boolean valida() {
        if (!this.m_strCodigoACM.equals("") && !this.m_strDescricao.equals("")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @return
     */
    public String getDescricao() {
        return m_strDescricao;
    }

    /**
     *
     * @return
     */
    public String getCodigoACM() {
        return m_strCodigoACM;
    }
    

    @Override
    public String toString() {
        return "Código: " + this.m_strCodigoACM + "\nDescrição: " + this.m_strDescricao;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } 
        if (this == obj) {
            return true;
        }
        Topico other = (Topico) obj;
        return other.m_strCodigoACM.equalsIgnoreCase(this.m_strCodigoACM) && other.m_strDescricao.equalsIgnoreCase(this.m_strDescricao);
    }
    
    
}
