/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author vitor_000
 */
public class FormulaDePagamento1 implements FormulasDePagamento, Serializable {

    private Evento evento;
    private float precoFullpaper;
    private float precoShortpaper;
    private float precoPosterpaper;
    private int qntFullPapers;
    private int qntShortPapers;
    private int qntPosterPapers;

    /**
     *
     * @param e
     */
    public FormulaDePagamento1(Evento e) {
        this.evento = e;
        this.precoFullpaper = this.evento.getPrecoFullPaper();
        this.precoShortpaper = this.evento.getPrecoShortPaper();
        this.precoPosterpaper = this.evento.getPrecoPosterPaper();
    }

    /**
     *
     * @return
     */
    @Override
    public float resultado() {
        return this.precoFullpaper * this.getQntFullPapers()
                + this.precoShortpaper * this.getQntShortPapers()
                + this.precoPosterpaper * this.getQntPosterPapers();
    }

    /**
     * @return the qntFullPapers
     */
    @Override
    public int getQntFullPapers() {
        return qntFullPapers;
    }

    /**
     * @return the qntShortPapers
     */
    @Override
    public int getQntShortPapers() {
        return qntShortPapers;
    }

    /**
     * @return the qntPosterPapers
     */
    @Override
    public int getQntPosterPapers() {
        return qntPosterPapers;
    }

    /**
     * @param qntFullPapers the qntFullPapers to set
     */
    @Override
    public void setQntFullPapers(int qntFullPapers) {
        this.qntFullPapers = qntFullPapers;
    }

    /**
     * @param qntShortPapers the qntShortPapers to set
     */
    @Override
    public void setQntShortPapers(int qntShortPapers) {
        this.qntShortPapers = qntShortPapers;
    }

    /**
     * @param qntPosterPapers the qntPosterPapers to set
     */
    @Override
    public void setQntPosterPapers(int qntPosterPapers) {
        this.qntPosterPapers = qntPosterPapers;
    }

}
