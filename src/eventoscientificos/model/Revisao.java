/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import eventoscientificos.utils.Estatistica;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author vitor_000
 */
public class Revisao implements Serializable{

    private int confianca;
    private int adequacao;
    private int originalidade;
    private int qualidade;
    private String recomendacao;
    private String textoJustificativo;
    private Revisor revisor;
    
    private int mediClassificaçoes;

    /**
     *
     * @param confianca
     * @param adequacao
     * @param originalidade
     * @param qualidade
     * @param recomendacao
     * @param revisor
     * @param textoJustificativo
     */
    public Revisao(int confianca, int adequacao, int originalidade, int qualidade, String recomendacao, Revisor revisor, String textoJustificativo) {
        this.confianca = confianca;
        this.adequacao = adequacao;
        this.originalidade = originalidade;
        this.qualidade = qualidade;
        this.recomendacao = recomendacao;
        this.revisor = revisor;
        this.textoJustificativo = textoJustificativo;
        this.mediClassificaçoes=Estatistica.calcMediaClassificacoes(confianca, adequacao,originalidade, qualidade);
        
    }

    /**
     *
     */
    public Revisao() {
        this.confianca = -1;
        this.adequacao = -1;
        this.originalidade = -1;
        this.qualidade = -1;
        this.recomendacao = "";
        this.textoJustificativo = "";
        this.revisor=new Revisor(new Utilizador());
        
    }

    /**
     *
     * @return
     */
    public int getConfianca() {
        return confianca;
    }

    /**
     *
     * @return
     */
    public int getAdequacao() {
        return adequacao;
    }

    /**
     *
     * @return
     */
    public int getOriginalidade() {
        return originalidade;
    }

    /**
     *
     * @return
     */
    public int getQualidade() {
        return qualidade;
    }

    /**
     *
     * @return
     */
    public String getRecomendacao() {
        return recomendacao;
    }

    /**
     *
     * @param confianca
     */
    public void setConfianca(int confianca) {
        this.confianca = confianca;
    }

    /**
     *
     * @param adequacao
     */
    public void setAdequacao(int adequacao) {
        this.adequacao = adequacao;
    }

    /**
     *
     * @param originalidade
     */
    public void setOriginalidade(int originalidade) {
        this.originalidade = originalidade;
    }

    /**
     *
     * @param qualidade
     */
    public void setQualidade(int qualidade) {
        this.qualidade = qualidade;
    }

    /**
     *
     * @param recomendacao
     */
    public void setRecomendacao(String recomendacao) {
        this.recomendacao = recomendacao;
    }

    /**
     *
     * @param revisor
     */
    public void setRevisor(Revisor revisor) {
        this.revisor = revisor;
    }
    

    @Override
    public String toString() {
        return "Confiança: " + this.confianca
                + "\nAdequação: " + this.adequacao
                + "\nOriginalidade: " + this.originalidade
                + "\nQualidade: " + this.qualidade
                + "\nRecomendação: " + this.recomendacao
                + "\nTexto justificativo: " + this.textoJustificativo;
    }

    /**
     *
     * @return
     */
    public boolean validaRevisao() {
        return this.confianca != -1
                && this.adequacao != -1
                && this.originalidade != -1
                && this.qualidade != -1
                && !this.recomendacao.equals("")
                && !this.textoJustificativo.equals("");
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Revisao other = (Revisao) obj;
        if (this.confianca != other.confianca) {
            return false;
        }
        if (this.adequacao != other.adequacao) {
            return false;
        }
        if (this.originalidade != other.originalidade) {
            return false;
        }
        if (this.qualidade != other.qualidade) {
            return false;
        }
        if (!Objects.equals(this.recomendacao, other.recomendacao)) {
            return false;
        }
        if (!Objects.equals(this.revisor, other.revisor)) {
            return false;
        }
        return true;
    }

    public Revisor getRevisor() {
        return revisor;
    }

    public int getMediClassificaçoes() {
        this.mediClassificaçoes=Estatistica.calcMediaClassificacoes(confianca, adequacao,originalidade, qualidade);
        return mediClassificaçoes;
    }

    /**
     * @return the textoJustificativo
     */
    public String getTextoJustificativo() {
        return textoJustificativo;
    }

    /**
     * @param textoJustificativo the textoJustificativo to set
     */
    public void setTextoJustificativo(String textoJustificativo) {
        this.textoJustificativo = textoJustificativo;
    }
    
    

}
