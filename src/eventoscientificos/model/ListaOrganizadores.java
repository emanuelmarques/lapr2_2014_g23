/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author vitor_000
 */
public class ListaOrganizadores implements Serializable{
    
    private List<Organizador> listaOrganizadores;

    /**
     *
     */
    public ListaOrganizadores() {
        this.listaOrganizadores = new ArrayList<>();
    }
    
    /**
     *
     * @return
     */
    public List<Organizador> getListaOrganizadores() {
        List<Organizador> lOrg = new ArrayList<Organizador>();

        for (ListIterator<Organizador> it = listaOrganizadores.listIterator(); it.hasNext();) {
            lOrg.add(it.next());
        }

        return lOrg;
    }
    
    /**
     *
     * @param o
     * @return
     */
    public boolean addOrganizador(Organizador o) {
        return this.listaOrganizadores.add(o);
    }

    /**
     *
     * @param u
     * @return
     */
    public boolean addOrganizador(Utilizador u) {
        Organizador o = new Organizador(u);

        o.valida();

        return addOrganizador(o);
    }
}
