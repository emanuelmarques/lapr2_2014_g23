package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Silva
 */
public class Distribuicao implements Serializable {

    /**
     * artigo distribuido
     */
    private Artigo artigo;
    /**
     * revisores atribuidos
     */
    private List<Revisor> listaRevisores = new ArrayList<>();

    /**
     * cria uma distribuiçao
     */
    public Distribuicao() {
    }

    /**
     * retorna o artigo da distribuiçao
     *
     * @return artigo da distribuiçao
     */
    public Artigo getArtigo() {
        return this.artigo;
    }

    /**
     * retorna os revisores atribuidos
     *
     * @return revisores atribuidos
     */
    public List<Revisor> getRevisores() {
        return this.listaRevisores;
    }

    /**
     * atribui um artigo a distribuiçao
     *
     * @param a artigo a atribuir
     */
    public void setArtigo(Artigo a) {
        this.artigo = a;
    }

    /**
     * atribui uma lista de revisores a distribuiçao
     *
     * @param lr lista de revisores a atribuir
     */
    public void setRevisor(List<Revisor> lr) {
        this.listaRevisores = lr;
    }

    /**
     * adiciona um revisor a lista de revisores
     *
     * @param r revisor a adicionar
     */
    public void addRevisor(Revisor r) {
        this.listaRevisores.add(r);
    }

    /**
     * apresenta os atributos da distribuiçao
     *
     * @return artigo e revisores da distribuiçao
     */
    @Override
    public String toString() {
        String revs = "";
        for (Revisor revisor : listaRevisores) {
            revs += revisor.getNome();
        }
        return "Artigo: " + this.artigo + " Revisores: " + revs;
    }

}
